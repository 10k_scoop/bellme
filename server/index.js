import express from "express";
import cors from "cors";
import { initializeApp } from "firebase/app";
import {
  getFirestore,
  doc,
  collection,
  getDocs,
  getDoc,
} from "firebase/firestore";
import {
  getAuth,
  GoogleAuthProvider,
  signInWithCredential,
} from "firebase/auth";
const app = express();
const firebaseConfig = {
  apiKey: "AIzaSyDvmmpaxdSVbmBEqZJALGFMeR7TcUiwrkI",
  authDomain: "bellme-2bee1.firebaseapp.com",
  projectId: "bellme-2bee1",
  storageBucket: "bellme-2bee1.appspot.com",
  messagingSenderId: "184120454139",
  appId: "1:184120454139:web:62ebeb080c9b054941b95e",
  measurementId: "G-92RL12SYHY",
};

const firebaseApp = initializeApp(firebaseConfig);
const db = getFirestore(firebaseApp);
app.use(express.json());
app.use(cors());
const auth = getAuth();
const port = process.env.PORT || 3030;
app.listen(port);

app.post("/googleLogin", async (req, res) => {
  try {
   var token=req?.body?.token
   const auth = getAuth();
   const provider = new GoogleAuthProvider();
   const credential = provider.credential(token);
   signInWithCredential(auth, credential);
  } catch (e) {
    console.log("Error Occured");
    res.json({ error: e.message });
  }
});

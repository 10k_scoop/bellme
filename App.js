import React, { useEffect, useState } from "react";
import { ActivityIndicator, StyleSheet, Text, View } from "react-native";
import { useFonts } from "expo-font";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { createMaterialBottomTabNavigator } from "@react-navigation/material-bottom-tabs";
import * as firebase from "firebase";
import Launch1 from "./src/Screens/Launch1";
import ClientLogin from "./src/Screens/Client/ClientLogin";
import ClientRegister from "./src/Screens/Client/ClientRegister";
import ClientRegister1 from "./src/Screens/Client/ClientRegister1";
import HandymanLogin from "./src/Screens/Handyman/HandymanLogin";
import HandymanRegister from "./src/Screens/Handyman/HandymanRegister";
import HandymanRegister1 from "./src/Screens/Handyman/HandymanRegister1";
import {
  SettingsIcon,
  HomeActiveIcon,
  SettingsActiveIcon,
  ProfileActiveIcon,
  ProfileIcon,
  HomeIcon,
  ChatActiveIcon,
  ChatIcon,
  NotficationActive,
  NotficationIcon,
} from "./src/Components/SvgComponets";
import Home from "./src/Screens/Client/Home";
import PostProject from "./src/Screens/Client/PostProject";
import PostProject1 from "./src/Screens/Client/PostProject1";
import PostProject2 from "./src/Screens/Client/PostProject2";
import QuoteRecieved from "./src/Screens/Client/QuoteRecieved";
import LiveLocation from "./src/Screens/Client/LiveLocation";
import LiveLocation1 from "./src/Screens/Client/LiveLocation1";
import Feedback from "./src/Screens/Client/Feedback";
import SPProfile from "./src/Screens/Client/SPProfile";
import Chat from "./src/Screens/Client/Chat";
import Chat1 from "./src/Screens/Client/Chat1";
import Notification from "./src/Screens/Client/Notification";
import Profile from "./src/Screens/Client/Profile";
import Settings from "./src/Screens/Client/Settings";
import Projects1 from "./src/Screens/Client/Projects1";
import BlockList from "./src/Screens/Client/BlockList";
import PostProject3 from "./src/Screens/Client/PostProject3";
import PostProject4 from "./src/Screens/Client/PostProject4";
import HHome from "./src/Screens/Handyman/HHome";
import SubmitQuote from "./src/Screens/Handyman/SubmitQuote";
import HChat1 from "./src/Screens/Handyman/HChat1";
import HChat2 from "./src/Screens/Handyman/HChat2";
import HNotification from "./src/Screens/Handyman/HNotification";
import HLiveLocation from "./src/Screens/Handyman/HLiveLocation";
import HFeedback from "./src/Screens/Handyman/HFeedBack";
import HProfile from "./src/Screens/Handyman/HProfile";
import HSettings from "./src/Screens/Handyman/HSettings";
import SubCategories from "./src/Screens/Client/SubCategories";
import GoPremium from "./src/Screens/Client/GoPremium";
import SuggestAService from "./src/Screens/Client/SuggestAService";
import { Provider } from "react-redux";
import store from "./src/state-management/store";
import Payment from "./src/Screens/Client/Payment";
import HGoPremium from "./src/Screens/Handyman/HGoPremium";
import JobStatus from "./src/Screens/Client/JobStatus";
import HJobStatus from "./src/Screens/Handyman/HJobStatus";
import ChangePassword from "./src/Screens/ChangePassword";
import TermsAndCondition from "./src/Screens/TermsAndCondition";
import ForgotPassword from "./src/Screens/ForgotPassword";
const Stack = createStackNavigator();
const BottomTabs = createMaterialBottomTabNavigator();
import { LogBox } from "react-native";
import CheckLocation from "./src/Screens/CheckLocation";
import Dashboard from "./src/Screens/Admin/Dashboard";
import ProviderDetail from "./src/Screens/Admin/ProviderDetail";

LogBox.ignoreAllLogs();
// Client stacks starts here
function ClientAuthStack() {
  return (
    <Stack.Navigator screenOptions={{ headerShown: false }}>
      <Stack.Screen name="ClientLogin" component={ClientLogin} />
      <Stack.Screen name="ClientRegister" component={ClientRegister} />
      <Stack.Screen name="ForgotPassword" component={ForgotPassword} />
      <Stack.Screen name="ClientRegister1" component={ClientRegister1} />
    </Stack.Navigator>
  );
}

function HomeStack() {
  return (
    <Stack.Navigator screenOptions={{ headerShown: false }}>
      {/* <Stack.Screen name="Home" component={Home} /> */}
      <Stack.Screen name="PostProject" component={PostProject} />
      <Stack.Screen name="GoPremium" component={GoPremium} />
      <Stack.Screen name="ChangePassword" component={ChangePassword} />
      <Stack.Screen name="TermsAndCondition" component={TermsAndCondition} />
      <Stack.Screen name="CheckLocation" component={CheckLocation} />
      {/* <Stack.Screen name="PostProject1" component={PostProject1} />
      <Stack.Screen name="PostProject2" component={PostProject2} /> */}
      <Stack.Screen name="QuoteRecieved" component={QuoteRecieved} />
      <Stack.Screen name="JobStatus" component={JobStatus} />
      <Stack.Screen name="LiveLocation" component={LiveLocation} />
      <Stack.Screen name="LiveLocation1" component={LiveLocation1} />
      <Stack.Screen name="Feedback" component={Feedback} />
      <Stack.Screen name="SPProfile" component={SPProfile} />
      <Stack.Screen name="SubCategories" component={SubCategories} />
      <Stack.Screen name="SuggestAService" component={SuggestAService} />
      <Stack.Screen name="Chat1" component={Chat1} />
    </Stack.Navigator>
  );
}

function ChatStack() {
  return (
    <Stack.Navigator screenOptions={{ headerShown: false }}>
      {/* <Stack.Screen name="Chat" component={Chat} /> */}
      <Stack.Screen name="Chat1" component={Chat1} />
    </Stack.Navigator>
  );
}

function ProfileStack() {
  return (
    <Stack.Navigator screenOptions={{ headerShown: false }}>
      <Stack.Screen name="Projects1" component={Projects1} />
    </Stack.Navigator>
  );
}

function SettingsStack() {
  return (
    <Stack.Navigator screenOptions={{ headerShown: false }}>
      <Stack.Screen name="BlockList" component={BlockList} />
      <Stack.Screen name="PostProject3" component={PostProject3} />
      <Stack.Screen name="PostProject4" component={PostProject4} />
    </Stack.Navigator>
  );
}
function ClientBottomTabNavigator() {
  return (
    <BottomTabs.Navigator
      // inactiveColor="#FFD6D6"
      // activeColor="#FFFFFF"
      style={{ alignContent: "flex-end" }}
      shifting={false}
      barStyle={{
        backgroundColor: "#FFFFFF",
        borderRadius: 20,
        position: "absolute",
        overflow: "hidden",
      }}
      initialRouteName="Home"
    >
      <BottomTabs.Screen
        options={{
          tabBarLabel: false,
          tabBarIcon: ({ color, size, focused }) =>
            focused ? (
              <SettingsActiveIcon />
            ) : (
              <SettingsIcon style={{ marginTop: 10 }} color={color} />
            ),
        }}
        name="Settings"
        component={Settings}
      />

      <BottomTabs.Screen
        options={{
          tabBarLabel: false,
          tabBarIcon: ({ color, size, focused }) =>
            focused ? (
              <ProfileActiveIcon />
            ) : (
              <ProfileIcon style={{ marginTop: 10 }} color={color} />
            ),
        }}
        name="Profile"
        component={Profile}
      />

      <BottomTabs.Screen
        options={{
          tabBarLabel: false,
          tabBarIcon: ({ color, size, focused }) =>
            focused ? (
              <HomeActiveIcon />
            ) : (
              <HomeIcon style={{ marginTop: 10 }} color={color} />
            ),
        }}
        name="Home"
        component={Home}
      />

      <BottomTabs.Screen
        options={{
          tabBarLabel: false,
          tabBarIcon: ({ color, size, focused }) =>
            focused ? (
              <ChatActiveIcon />
            ) : (
              <ChatIcon style={{ marginTop: 10 }} color={color} />
            ),
        }}
        name="Chat"
        component={Chat}
      />

      <BottomTabs.Screen
        options={{
          tabBarLabel: false,
          tabBarIcon: ({ color, size, focused }) =>
            focused ? (
              <NotficationActive />
            ) : (
              <NotficationIcon style={{ marginTop: 10 }} color={color} />
            ),
        }}
        name="Notification"
        component={Notification}
      />
    </BottomTabs.Navigator>
  );
}
// Client stack ends here

// Handyman stacks starts here

function HHomeStack() {
  return (
    <Stack.Navigator screenOptions={{ headerShown: false }}>
      <Stack.Screen name="HHome" component={HHome} />
      <Stack.Screen name="SubmitQuote" component={SubmitQuote} />
      <Stack.Screen name="HNotification" component={HNotification} />
      <Stack.Screen name="ChangePassword" component={ChangePassword} />
      <Stack.Screen name="TermsAndCondition" component={TermsAndCondition} />
      <Stack.Screen name="CheckLocation" component={CheckLocation} />
      <Stack.Screen name="ForgotPassword" component={ForgotPassword} />
      <Stack.Screen name="HChat1" component={HChat1} />
      <Stack.Screen name="HChat2" component={HChat2} />
      <Stack.Screen name="HLiveLocation" component={HLiveLocation} />
      <Stack.Screen name="HFeedback" component={HFeedback} />
      <Stack.Screen name="HJobStatus" component={HJobStatus} />
      {/* <Stack.Screen name="HProfile" component={HProfile} /> */}
    </Stack.Navigator>
  );
}
function HandyManAuth() {
  return (
    <Stack.Navigator screenOptions={{ headerShown: false }}>
      <Stack.Screen name="HandymanLogin" component={HandymanLogin} />
      <Stack.Screen name="HandymanRegister" component={HandymanRegister} />
      <Stack.Screen name="ForgotPassword" component={ForgotPassword} />
      <Stack.Screen name="HandymanRegister1" component={HandymanRegister1} />
    </Stack.Navigator>
  );
}

function HMBottomTabNavigator() {
  return (
    <BottomTabs.Navigator
      // inactiveColor="#FFD6D6"
      // activeColor="#FFFFFF"
      style={{ alignContent: "flex-end" }}
      shifting={false}
      barStyle={{
        backgroundColor: "#FFFFFF",
        borderRadius: 20,
        position: "absolute",
        overflow: "hidden",
      }}
      initialRouteName="HHome"
    >
      <BottomTabs.Screen
        options={{
          tabBarLabel: false,
          tabBarIcon: ({ color, size, focused }) =>
            focused ? (
              <SettingsActiveIcon />
            ) : (
              <SettingsIcon style={{ marginTop: 10 }} color={color} />
            ),
        }}
        name="Settings"
        component={HSettings}
      />

      <BottomTabs.Screen
        options={{
          tabBarLabel: false,
          tabBarIcon: ({ color, size, focused }) =>
            focused ? (
              <ProfileActiveIcon />
            ) : (
              <ProfileIcon style={{ marginTop: 10 }} color={color} />
            ),
        }}
        name="HProfile"
        component={HProfile}
      />

      <BottomTabs.Screen
        options={{
          tabBarLabel: false,
          tabBarIcon: ({ color, size, focused }) =>
            focused ? (
              <HomeActiveIcon />
            ) : (
              <HomeIcon style={{ marginTop: 10 }} color={color} />
            ),
        }}
        name="HHome"
        component={HHome}
      />

      <BottomTabs.Screen
        options={{
          tabBarLabel: false,
          tabBarIcon: ({ color, size, focused }) =>
            focused ? (
              <ChatActiveIcon />
            ) : (
              <ChatIcon style={{ marginTop: 10 }} color={color} />
            ),
        }}
        name="Chat"
        component={HChat1}
      />

      <BottomTabs.Screen
        options={{
          tabBarLabel: false,
          tabBarIcon: ({ color, size, focused }) =>
            focused ? (
              <NotficationActive />
            ) : (
              <NotficationIcon style={{ marginTop: 10 }} color={color} />
            ),
        }}
        name="HNotification"
        component={HNotification}
      />
    </BottomTabs.Navigator>
  );
}
function Auth() {
  return (
    <NavigationContainer>
      <Stack.Navigator screenOptions={{ headerShown: false }}>
        {/* */}
        <Stack.Screen name="Launch1" component={Launch1} />
        {/* Client Stacks */}
        <Stack.Screen name="ClientAuthStack" component={ClientAuthStack} />
        {/* Handyman Stacks */}
        <Stack.Screen name="HandyManAuth" component={HandyManAuth} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
function Admin() {
  return (
    <NavigationContainer>
      <Stack.Navigator screenOptions={{ headerShown: false }}>
        {/* */}
        <Stack.Screen name="Dashboard" component={Dashboard} />
        <Stack.Screen name="ProviderDetail" component={ProviderDetail} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

function HandyManHome() {
  return (
    <NavigationContainer>
      <Stack.Navigator screenOptions={{ headerShown: false }}>
        {/* Handyman Stacks */}
        <Stack.Screen
          name="HMBottomTabNavigator"
          component={HMBottomTabNavigator}
        />
        <Stack.Screen name="HHomeStack" component={HHomeStack} />
        <Stack.Screen name="HGoPremium" component={HGoPremium} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

function ClientHome() {
  return (
    <NavigationContainer>
      <Stack.Navigator screenOptions={{ headerShown: false }}>
        <Stack.Screen
          name="ClientBottomTabNavigator"
          component={ClientBottomTabNavigator}
        />
        <Stack.Screen name="HomeStack" component={HomeStack} />
        <Stack.Screen name="ChatStack" component={ChatStack} />
        <Stack.Screen name="ProfileStack" component={ProfileStack} />
        <Stack.Screen name="SettingsStack" component={SettingsStack} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default function App() {
  const [role, setRole] = useState(false);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    // Initialize Firebase
    var firebaseConfig = {
      apiKey: "AIzaSyA29wEJqI9qwxdnHASZXsynZuY_QgZASS0",
      authDomain: "bellmeapp-c9c34.firebaseapp.com",
      projectId: "bellmeapp-c9c34",
      storageBucket: "bellmeapp-c9c34.appspot.com",
      messagingSenderId: "894704988725",
      appId: "1:894704988725:web:0f5e9db21e77b695e8b77c",
      measurementId: "G-SSZSEL8W72",
    };
    // Initialize Firebase
    try {
      if (firebase.apps.length === 0) {
        firebase.initializeApp(firebaseConfig);
      }
    } catch (error) {
      alert("Check you internet connection!!");
    }

    firebase.auth().onAuthStateChanged(function(user) {
      if (user) {
        var db = firebase.firestore();
        setLoading(true);
        db.collection("users")
          .where("email", "==", user.providerData[0].email)
          .get()
          .then((querySnapshot) => {
            querySnapshot.forEach((doc) => {
              setLoading(false);
              setRole(doc.data().role);
              console.log(doc.data().role);
            });
          })
          .catch((error) => {
            console.log("Error getting documents: ", error);
            setLoading(false);
          });
      } else {
        setRole("LoggedOut");
        setLoading(false);
      }
      setLoading(false);
    });
  }, []);

  const [loaded] = useFonts({
    RBl: require("./assets/fonts/Roboto/Roboto-Black.ttf"),
    RBIt: require("./assets/fonts/Roboto/Roboto-BlackItalic.ttf"),
    RBo: require("./assets/fonts/Roboto/Roboto-Bold.ttf"),
    RBIt: require("./assets/fonts/Roboto/Roboto-BoldItalic.ttf"),
    RIt: require("./assets/fonts/Roboto/Roboto-Italic.ttf"),
    RLi: require("./assets/fonts/Roboto/Roboto-Light.ttf"),
    RLIt: require("./assets/fonts/Roboto/Roboto-LightItalic.ttf"),
    RMe: require("./assets/fonts/Roboto/Roboto-Medium.ttf"),
    RMIt: require("./assets/fonts/Roboto/Roboto-MediumItalic.ttf"),
    RRe: require("./assets/fonts/Roboto/Roboto-Regular.ttf"),
    RT: require("./assets/fonts/Roboto/Roboto-Thin.ttf"),
    RTIt: require("./assets/fonts/Roboto/Roboto-ThinItalic.ttf"),

    PBo: require("./assets/fonts/Poppins/Poppins-Bold.ttf"),
    PRe: require("./assets/fonts/Poppins/Poppins-Regular.ttf"),
    PMe: require("./assets/fonts/Poppins/Poppins-Medium.ttf"),
    PSBo: require("./assets/fonts/Poppins/Poppins-SemiBold.ttf"),
  });

  if (!loaded) {
    return null;
  }
  if (loading) {
    return (
      <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
        <ActivityIndicator size="large" color="#222" />
      </View>
    );
  }

  return (
    <Provider store={store}>
      {role == "client" ? (
        <ClientHome />
      ) : role == "provider" ? (
        <HandyManHome />
      ) : role == "admin" ? (
        <Admin />
      ) : (
        <Auth />
      )}
    </Provider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});

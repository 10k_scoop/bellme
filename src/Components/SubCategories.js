export const subCategories = [
    {
        items: ['Architect', 'Iron Bender', 'Mixer / Moulder', 'Construction worker', 'Labourer', 'Painter']
    },
    {
        items: ['Wood Worker', 'Upholstery Builder', 'Lumber Jack', 'Roofing / Repairing']
    },
    {
        items: ['Cleaner', 'Waste Management']
    },
    {
        items: ['Water works', 'Piping installation', 'Sewage / Drainage repair']
    },
    {
        items: ['Personal trainer', 'Music trainer', 'Home teacher (edu)']
    },
    {
        items: ['Laundry', 'Phone repair', 'Barber', 'Hairstylist', 'Tailor', 'Driver', 'Private Security', 'Mani / Pedicure']
    },
    {
        items:['Vulcanizer','Engine servicing','Auto repairs','Truck repairs','Tractor / Mechanis','Rewirer','Ac/ Heater repair']
    },
    {
        items:['Light Installation','Light repairs','CCtv installation','Solar Panel','Generator repair','Alarm installation','Electronics repair']
    },
    {
        items:['Chef','Gardener','Interior decorater','Au pair /  babysitter','Exterior designer','Ac / Heater repair','Security guard','Frame artist']
    },
    {
        items:['Venue lease','Carteres','Event rentals','Photographer','Video Coverage','Event planner','Music Band','Funeral mgt']
    }
]
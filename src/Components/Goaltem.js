import React from 'react';
import {View , Text, StyleSheet ,TouchableOpacity} from 'react-native'
import { RFValue  as rf} from "react-native-responsive-fontsize";
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

const GoalItem = props =>{
    return(
        <>
        <View style = {styles.Main}>
         
                
        <View key = {props.index1} style = {styles.listItem}>
        <Text style = {{color : '#fff'}}>{props.title}</Text>
        </View>
        
        </View>
       
    </>
    )
}

const styles = StyleSheet.create({
   
Main : {
    width : wp('100%'),
    flexDirection : 'row',
    paddingHorizontal : wp('7%')
    
},
InnerMain : {
    flexDirection : 'row',
},
listItem : 
{
  paddingVertical : hp('1%'),
  paddingHorizontal : wp('2%'),
  backgroundColor : '#EF4136',
  borderColor : '#EF4136',
  borderWidth : 1 ,
  borderRadius : 10,
  marginBottom : 10
  
},

})
export default GoalItem;
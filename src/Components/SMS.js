import { useState } from "react";
import React from 'react';
import { View, Text, } from 'react-native'

export const SendSms = (props) => {

    const [height, setHeight] = useState(10)
    return (
        <View
            onLayout={(event) => {
                var { x, y, width, height } = event.nativeEvent.layout;
                setHeight(height)
            }}

            style={[{
                paddingHorizontal: 10,
                minHeight:25,
                backgroundColor: 'white',
                borderTopLeftRadius: height / 2,
                borderBottomLeftRadius: height / 2,
                borderTopRightRadius: height / 2,
                borderBottomRightRadius: height / 6,
                paddingVertical: 5,
                paddingLeft: 12,
                alignSelf: 'flex-end',
                marginTop: 13
            },props.style]}
            
        >
            <Text style={{ color: '#FBB040', fontSize: 12, fontFamily: 'RRe' }}>{props.msg}</Text>
        </View>
    )
}

export const RecieveSms = (props) => {
    const [height, setHeight] = useState(0)
    return (<View
        onLayout={(event) => {
            var { x, y, width, height } = event.nativeEvent.layout;
            setHeight(height)
        }}
        style={[{
            paddingHorizontal: 10,
            marginTop: 13,
            minHeight:25,
            backgroundColor: '#FBB040',
            borderTopLeftRadius: height / 2,
            borderBottomLeftRadius: height / 6,
            borderTopRightRadius: height / 2,
            borderBottomRightRadius: height / 2,
            paddingVertical: 5,
            paddingLeft: 12,
            alignSelf: 'flex-start',
            marginTop: 10,
        },props.style]}
        
    >
        <Text style={{ color: 'white', fontSize: 12, fontFamily: 'RRe' }}>{props.msg}</Text>
    </View>
    )
}
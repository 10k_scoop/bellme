import React from 'react';
import { Text, View, StyleSheet, TouchableWithoutFeedback, TouchableOpacity, FlatList, Dimensions, ScrollView } from 'react-native';



const { width, height } = Dimensions.get('window');
export default class DropDown extends React.Component {

    render() {

        if (this.props.show) {
            const { y: top, x: left } = this.props.position;

            console.log(this.props.renderItem)
            var RenderItem = this.props.renderItem;
            return (
                <TouchableWithoutFeedback onPress={() => this.props.hide('background pressed')}>
                    <View style={styles.container}>
                        <ScrollView
                            showsVerticalScrollIndicator={false}
                            contentContainerStyle={{ paddingBottom: 850 }}
                            style={{ width: "100%", }}
                        >
                            <View style={[styles.menu, { top: top, width: width - 40, borderRadius:15}]}>

                                <TouchableOpacity
                                    activeOpacity={1}
                                    style={{ width: "100%",  }}>
                                    <RenderItem />
                                </TouchableOpacity>

                            </View>
                        </ScrollView>
                    </View>
                </TouchableWithoutFeedback>
            );
        } else {
            return null
        }
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,

        backgroundColor: 'rgba(0,0,0,0.5)',
        alignItems: 'center',
        flexGrow: 1,
        flexShrink: 1


    },
    menu: {
        flex: 1,
        position: 'absolute',
        backgroundColor: 'white',
        alignItems: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 5,
        },
        shadowOpacity: 0.36,
        shadowRadius: 6.68,
        elevation: 11,
        alignSelf: 'center',
        flexGrow: 1,
        flexShrink: 1


    }
});


import { LinearGradient } from "expo-linear-gradient";
import { StatusBar } from "expo-status-bar";
import React, { useState } from "react";
import {
  SafeAreaView,
  View,
  Text,
  TextInput,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
  ActivityIndicator,
} from "react-native";
import AppColors from "../Components/AppColors";
import {
  ArrowLeft,
  LoginSvg,
  LogoSvg,
  MsgIcon,
  ORSvg,
  PasswordIcon,
  PersonIcon,
  RegisterSvg,
} from "../Components/SvgComponets";
import * as firebase from "firebase";
const ForgotPassword = (props) => {
  const [loading, setLoading] = useState(false);
  const [email, setEmail] = useState("");
  const user = firebase.auth().currentUser;

  const onNext = () => {
    if (email != "") {
      setLoading(true);
      firebase
        .auth()
        .sendPasswordResetEmail(email)
        .then(() => {
          setLoading(false);
          alert("Verification email sent to your email");
          props?.navigation.goBack();
        })
        .catch((error) => {
          var errorCode = error.code;
          var errorMessage = error.message;
          alert(errorMessage);
        });
    } else {
      alert("Enter your email !");
    }
  };

  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: "#F5F5F5" }}>
      <StatusBar hidden={true} />
      <View
        style={{
          marginTop: Platform.OS == "android" ? "15%" : "10%",
          width: "90%",
          alignItems: "center",
        }}
      >
        <View
          style={{
            flexDirection: "row",
            alignItems: "flex-start",
            width: "100%",
            paddingHorizontal: "10%",
          }}
        >
          <TouchableOpacity
            onPress={() => {
              props.navigation.goBack();
            }}
            style={{ marginRight: 10 }}
          >
            <ArrowLeft />
          </TouchableOpacity>
        </View>
      </View>
      <Text
        style={{
          fontFamily: "RRe",
          fontSize: 14,
          color: "#707070",
          marginTop: 20,
          alignSelf: "center",
        }}
      >
        Recover password
      </Text>
      <ScrollView contentContainerStyle={{ paddingBottom: 80 }}>
        <View
          style={{
            width: "85%",
            alignSelf: "center",
            alignItems: "center",
            marginTop: 20,
          }}
        >
          <View style={styles.textInputContainer}>
            <View style={{ position: "absolute", left: 15 }}>
              <MsgIcon />
            </View>
            <TextInput
              placeholder="Email Address"
              placeholderTextColor="#707070"
              style={styles.textInput}
              onChangeText={(val) => setEmail(val)}
              keyboardType="email-address"
            />
          </View>
          <TouchableOpacity
            onPress={onNext}
            style={{ marginTop: 40, width: "100%", height: 40 }}
          >
            <LinearGradient
              style={{
                flex: 1,
                justifyContent: "center",
                alignItems: "center",
                borderRadius: 13,
              }}
              colors={[AppColors.linear1, AppColors.linear2]}
            >
              <Text
                style={{ color: "#FFFFFF", fontSize: 14, fontFamily: "RBo" }}
              >
                {loading ? (
                  <ActivityIndicator size="small" color="#fff" />
                ) : (
                  "S U B M I T"
                )}
              </Text>
            </LinearGradient>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  textInputContainer: {
    flexDirection: "row",
    backgroundColor: "#E4E4E4",
    borderRadius: 13,
    height: 40,
    width: "100%",
    paddingLeft: 15,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 25,
  },
  textInput: {
    fontFamily: "RRe",
    fontSize: 14,
    color: "#707070",
    width: "80%",
    textAlign: "center",
  },
});

export default ForgotPassword;

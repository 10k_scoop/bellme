import { LinearGradient } from "expo-linear-gradient";
import { StatusBar } from "expo-status-bar";
import React, { useState } from "react";
import {
  SafeAreaView,
  View,
  Text,
  TextInput,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
  ActivityIndicator,
} from "react-native";
import { RFValue } from "react-native-responsive-fontsize";
import AppColors from "../Components/AppColors";
import { ArrowLeft } from "../Components/SvgComponets";
import MapView, { Marker, PROVIDER_GOOGLE } from "react-native-maps";
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from "react-native-responsive-screen";

const CheckLocation = (props) => {
  let location = JSON.parse(
    props?.route?.params?.data?.Hmandetails?.handyManLocations
  );
  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: "#F5F5F5" }}>
      <View
        style={{
          marginTop: Platform.OS == "android" ? "15%" : "5%",
          width: "90%",
          alignItems: "center",
          zIndex: 999,
        }}
      >
        <View
          style={{
            flexDirection: "row",
            alignItems: "flex-start",
            width: "100%",
            paddingHorizontal: "10%",
            zIndex: 9999,
          }}
        >
          <TouchableOpacity
            onPress={() => {
              props.navigation.goBack();
            }}
            style={{ marginRight: 10, zIndex: 99 }}
          >
            <ArrowLeft />
          </TouchableOpacity>
        </View>
      </View>

      <View
        style={{
          width: "85%",
          alignSelf: "center",
          alignItems: "center",
          position: "absolute",
        }}
      >
        <MapView
          provider={PROVIDER_GOOGLE}
          initialRegion={{
            latitude: location?.latitude,
            longitude: location?.longitude,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
          }}
          style={styles.map}
        >
          <Marker
            coordinate={{
              latitude: location?.latitude || 0,
              longitude: location?.longitude || 0,
            }}
            title={""}
            description={""}
          ></Marker>
        </MapView>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  textInputContainer: {
    flexDirection: "row",
    backgroundColor: "#E4E4E4",
    borderRadius: 13,
    height: 40,
    width: "100%",
    paddingLeft: 15,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 25,
  },
  textInput: {
    fontFamily: "RRe",
    fontSize: 14,
    color: "#707070",
    width: "80%",
    textAlign: "center",
  },
  map: {
    width: widthPercentageToDP("100%"),
    height: heightPercentageToDP("100%"),
  },
});

export default CheckLocation;

import React from "react";
import {
  SafeAreaView,
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  Image,
} from "react-native";
import { ArrowLeft, CirclesIcon } from "../../Components/SvgComponets";
import { LinearGradient } from "expo-linear-gradient";
import AppColors from "../../Components/AppColors";

const GoPremium = (props) => {
  const PackageView = ({ price, period }) => (
    <TouchableOpacity
      style={[styles.premiumBox, { overflow: "hidden" }]}
    >
      <Text style={styles.text}>{period}</Text>
      <Image
        source={require("../../assets/circle2.png")}
        style={{
          right: -60,
          position: "absolute",
          top: -15,
          width: 200,
          height: 120,
          resizeMode: "contain",
        }}
      />
      <Text
        style={[
          {
            fontFamily: "PBo",
            fontSize: 14,
            color: "#EF4136",
            position: "absolute",
            top: 115,
            right: 10,
          },
        ]}
      >
        {price}
      </Text>
      <Image
        source={require("../../assets/circle1.png")}
        style={{ left: -30, position: "absolute", bottom: -30 }}
      />
    </TouchableOpacity>
  );

  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: "#F5F5F5" }}>
      <View
        style={{
          height: "100%",
          width: "80%",
          alignSelf: "center",
          marginTop: Platform.OS == "android" ? "10%" : "5%",
        }}
      >
        <View
          style={{
            width: "100%",
            flexDirection: "row",
            justifyContent: "space-between",
          }}
        >
          <TouchableOpacity
            onPress={() => {
              props.navigation.goBack();
            }}
          >
            <ArrowLeft />
          </TouchableOpacity>
          <Text style={{ color: "#EF4136", fontFamily: "RBo", fontSize: 20 }}>
            Go Premium
          </Text>
          <Text></Text>
        </View>

        <View style={{ flex: 1, marginTop: 50 }}>
          <PackageView period="Trial Version" price="Free   (Activated)" dark />
          <PackageView period="3 Months" price="$29.99" />
          <PackageView period="6 Months" price="$49.99" />
          <PackageView period="12 Months" price="$99.99" />
        </View>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  premiumBox: {
    width: "100%",
    height: 100,
    borderRadius: 10,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "white",
    marginTop: 20,
    height: 150,
  },
  text: {
    fontSize: 16,
    fontFamily: "RBo",
    alignSelf: "center",
    color: "black",
    textAlign: "center",
    lineHeight: 20,
  },
});

export default GoPremium;

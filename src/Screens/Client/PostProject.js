import React, { useEffect, useState } from "react";
import {
  TouchableOpacity,
  View,
  Text,
  Platform,
  StyleSheet,
  TextInput,
  FlatList,
  Alert,
  Modal,
  Pressable,
  Image,
  ScrollView,
  ActivityIndicator,
} from "react-native";
import { SafeAreaView } from "react-native-safe-area-context";
import {
  ArrowDown,
  ArrowLeft,
  CameraGradientIcon,
} from "../../Components/SvgComponets";
import * as ImagePicker from "expo-image-picker";
import { LinearGradient } from "expo-linear-gradient";
import AppColors from "../../Components/AppColors";
import DropDown from "../../Components/DropDown";
import Photo from "../../assets/cele.jpg";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import { connect } from "react-redux";
import { subCategories } from "../../Components/SubCategories";
import {
  postJob,
  getUserData,
} from "../../state-management/actions/features/Features";
import * as firebase from "firebase";
import * as Location from "expo-location";

const PostProject = (props) => {
  const [modalVisible, setModalVisible] = useState(false);
  const [showSubCategory, setShowSubCategory] = useState(false);
  const [subCategory, setSubCategory] = useState("Skill Sub-category");
  const [urgency, setUrgency] = useState("Flexible");
  const [budget, setBudget] = useState("00.0");
  const [desc, setDesc] = useState("");
  const [showUrgency, setShowUrgency] = useState(false);
  let urgencyItems = ["Flexible", "Urgent", "Very Urgent"];
  const [image, setImage] = useState(null);
  const [loading, setLoading] = useState(true);
  const user = firebase.auth().currentUser;
  const [days, setDays] = useState([1, 2, 3, 4, 5, 6, 7, 8, 9, 10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30]);
  const [selectedDay, setSelectedDay] = useState(1);
  const [showDays, setShowDays] = useState(false);
  const [location, setLocation] = useState(null);
  const [errorMsg, setErrorMsg] = useState(null);

  useEffect(() => {
    (async () => {
      let { status } = await Location.requestForegroundPermissionsAsync();
      if (status !== "granted") {
        setErrorMsg("Permission to access location was denied");
        return;
      }

      let location = await Location.getCurrentPositionAsync({});
      setLocation(location?.coords);
    })();
  }, []);

  useEffect(() => {
    props.getUserData(user.uid, setLoading);
  }, []);


  const pickImage = async () => {
    // No permissions request is necessary for launching the image library
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.IMAGE,
      allowsEditing: true,
      aspect: [4, 3],
      quality: 1,
    });

    if (!result.cancelled) {
      setImage(result.uri);
    }
  };

  if (loading) {
    return (
      <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
        <ActivityIndicator color="black" size="large" />
      </View>
    );
  }

  const onPost = () => {
    setLoading(true);
    let data = {
      category: props.route.params?.selectedCategory,
      subCategory: subCategory,
      urgency: urgency,
      desc: desc,
      budget: budget,
      image: image,
      author: props.get_user_details?.name,
      authorEmail: props.get_user_details?.email,
      authorProfile: props.get_user_details?.profileImg,
      authorId: user.uid,
      date: new Date(),
      location: "",
      duration: selectedDay,
      status: "New",
      applicants: [],
      clientsLocations:JSON.stringify(location)
    };
    props.postJob(data, setLoading, setModalVisible);
  };

  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: "#F5F5F5" }}>
      <ScrollView>
        <View
          style={{
            height: hp("100%"),
            width: "80%",
            alignSelf: "center",
            marginTop: Platform.OS == "android" ? "10%" : hp("3%"),
          }}
        >
          <View
            style={{
              width: "100%",
              flexDirection: "row",
              justifyContent: "space-between",
            }}
          >
            <TouchableOpacity
              onPress={() => {
                props.navigation.goBack();
              }}
            >
              <ArrowLeft />
            </TouchableOpacity>
            <Text style={{ color: "#EF4136", fontFamily: "RBo", fontSize: 20 }}>
              Post Projesct
            </Text>
            <Text></Text>
          </View>

          <TouchableOpacity style={[styles.picker, { marginTop: 40 }]}>
            <Text
              style={{
                position: "absolute",
                left: 15,
                fontSize: 14,
                color: "#707070",
                fontFamily: "RRe",
              }}
            >
              Category
            </Text>
            <Text
              style={{
                fontSize: 14,
                fontFamily: "RRe",
                color: "#EF4136",
                marginRight: 15,
              }}
            >
              {props.route.params?.selectedCategory}
            </Text>
            <ArrowDown />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              setShowSubCategory(!showSubCategory);
            }}
            style={[styles.picker]}
          >
            <Text
              style={{
                position: "absolute",
                left: 15,
                fontSize: 14,
                color: "#707070",
                fontFamily: "RRe",
              }}
            >
              {subCategory}
            </Text>
            <Text
              style={{
                fontSize: 14,
                fontFamily: "RRe",
                color: "#EF4136",
                marginRight: 15,
              }}
            >
              Select
            </Text>
            <ArrowDown />
          </TouchableOpacity>
          <TouchableOpacity
            style={[styles.picker]}
            onPress={() => {
              setShowUrgency(true);
            }}
          >
            <Text
              style={{
                position: "absolute",
                left: 15,
                fontSize: 14,
                color: "#707070",
                fontFamily: "RRe",
              }}
            >
              Urgency
            </Text>
            <Text
              style={{
                fontSize: 14,
                fontFamily: "RRe",
                color: "#EF4136",
                marginRight: 15,
              }}
            >
              {urgency}
            </Text>
            <ArrowDown />
          </TouchableOpacity>
          <TouchableOpacity
            style={[styles.picker]}
            onPress={() => {
              setShowDays(true);
            }}
          >
            <Text
              style={{
                position: "absolute",
                left: 15,
                fontSize: 14,
                color: "#707070",
                fontFamily: "RRe",
              }}
            >
              Duration
            </Text>
            <Text
              style={{
                fontSize: 14,
                fontFamily: "RRe",
                color: "#EF4136",
                marginRight: 15,
              }}
            >
              {selectedDay}
            </Text>
            <ArrowDown />
          </TouchableOpacity>
          <TouchableOpacity style={[styles.picker]}>
            <Text
              style={{
                position: "absolute",
                left: 15,
                fontSize: 14,
                color: "#707070",
                fontFamily: "RRe",
              }}
            >
              Budget
            </Text>
            <Text
              style={{
                fontSize: 14,
                fontFamily: "RRe",
                color: "#EF4136",
              }}
            >
              $
            </Text>
            <TextInput
              placeholder="00.00"
              placeholderTextColor="#EF4136"
              style={{
                fontSize: 14,
                fontFamily: "RRe",
                color: "#EF4136",
                marginRight: 15,
              }}
              onChangeText={(val) => setBudget(val)}
            />
          </TouchableOpacity>

          <TextInput
            style={{
              backgroundColor: "#E4E4E4",
              height: 80,
              fontFamily: "RRe",
              fontSize: 14,
              color: "#707070",
              paddingTop: 10,
              paddingLeft: 15,
              borderRadius: 13,
              marginTop: 20,
            }}
            placeholder="Description"
            placeholderTextColor="#707070"
            textAlignVertical="top"
            onChangeText={(val) => setDesc(val)}
          />
          <TouchableOpacity
            style={{
              backgroundColor: "#E4E4E4",
              height: 80,
              fontSize: 14,
              borderRadius: 13,
              marginTop: 20,
              justifyContent: "center",
              alignItems: "center",
              overflow: "hidden",
            }}
            onPress={pickImage}
          >
            {image != null && (
              <Image
                source={{ uri: image }}
                style={{ width: "100%", height: "100%", position: "absolute" }}
              />
            )}
            <CameraGradientIcon />
            <Text
              style={{
                fontFamily: "RRe",
                color: "#707070",
                fontSize: 12,
                marginTop: 10,
              }}
            >
              Upload Document/Photos
            </Text>
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => {
              if (subCategory != "" && budget != "" && desc != "") {
                onPost();
              } else {
                alert("Fill all details !");
              }
            }}
            style={{
              width: wp("80%"),
              alignSelf: "center",
              height: hp("5%"),
              marginVertical: hp("10%"),
            }}
          >
            <LinearGradient
              style={{
                flex: 1,
                borderRadius: 13,
                alignItems: "center",
                justifyContent: "center",
              }}
              colors={[AppColors.linear1, AppColors.linear2]}
            >
              <Text style={{ color: "white", fontFamily: "RBo", fontSize: 14 }}>
                S U B M I T
              </Text>
            </LinearGradient>
          </TouchableOpacity>
        </View>
      </ScrollView>
      <Modal
        transparent={true}
        isVisible={true}
        animationType="fade"
        visible={modalVisible}
        onRequestClose={() => {
          Alert.alert("Modal has been closed.");
          setModalVisible(!modalVisible);
        }}
      >
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <Image style={styles.ProfilePicStyle} source={Photo} />
            <Text style={styles.modalText}>Congratulations</Text>
            <Text style={styles.modalText1}>
              your job has been posted Successfullly
            </Text>
            <Pressable
              style={[styles.button, styles.buttonClose]}
              colors={[AppColors.linear1, AppColors.linear2]}
              onPress={() => {
                setModalVisible(!modalVisible);
                props.navigation.navigate("HomeStack", {
                  screen: "JobStatus",
                  params: {jobId:props?.post_job?.id,route:'noti'},
                });
              }}
            >
              <Text style={styles.textStyle}>OKAY</Text>
            </Pressable>
          </View>
        </View>
      </Modal>

      {showSubCategory && (
        <View style={styles.pickerPopUpWrapper}>
          <View style={styles.pickerPopUp}>
            <ScrollView>
              {subCategories[
                props.route.params?.selectedCategoryIndex
              ]?.items?.map((item, index) => {
                return (
                  <TouchableOpacity
                    onPress={() => {
                      setSubCategory(item);
                      setShowSubCategory(false);
                    }}
                    style={styles.pickerPopUpItem}
                    key={index}
                  >
                    <Text>{item}</Text>
                  </TouchableOpacity>
                );
              })}
            </ScrollView>
          </View>
        </View>
      )}

      {showDays && (
        <View style={styles.pickerPopUpWrapper}>
          <View style={styles.pickerPopUp}>
            <ScrollView>
              {days?.map((item, index) => {
                return (
                  <TouchableOpacity
                    onPress={() => {
                      setSelectedDay(item+" days");
                      setShowDays(false);
                    }}
                    style={styles.pickerPopUpItem}
                    key={index}
                  >
                    <Text>{item+" days"}</Text>
                  </TouchableOpacity>
                );
              })}
            </ScrollView>
          </View>
        </View>
      )}

      {showUrgency && (
        <View style={styles.pickerPopUpWrapper}>
          <View style={styles.pickerPopUp}>
            <ScrollView>
              {urgencyItems?.map((item, index) => {
                return (
                  <TouchableOpacity
                    onPress={() => {
                      setUrgency(item);
                      setShowUrgency(false);
                    }}
                    style={styles.pickerPopUpItem}
                    key={index}
                  >
                    <Text>{item}</Text>
                  </TouchableOpacity>
                );
              })}
            </ScrollView>
          </View>
        </View>
      )}
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  picker: {
    flexDirection: "row",
    width: "100%",
    height: 40,
    marginTop: 20,
    backgroundColor: "#FFFFFF",
    borderRadius: 13,
    justifyContent: "flex-end",
    paddingRight: 10,
    alignItems: "center",
    shadowColor: "#000000",
    shadowOpacity: 0.2,
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowRadius: 2,
    // elevation: 1
  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(0,0,0,0.5)",
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  button: {
    borderRadius: 20,
    padding: 10,
    elevation: 2,
    backgroundColor: "#f47d3c",
  },
  buttonOpen: {
    backgroundColor: "#f47d3c",
  },
  buttonClose: {
    backgroundColor: "#f47d3c",
    width: wp("30%"),
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center",
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center",
    color: "#EF4136",
    fontSize: 20,
    fontWeight: "700",
  },
  modalText1: {
    marginBottom: 15,
    textAlign: "center",
    fontSize: 18,
  },
  ProfilePicStyle: {
    width: wp("20%"),
    height: hp("10%"),
  },
  pickerPopUp: {
    width: "90%",
    backgroundColor: "#fff",
    borderRadius: 10,
    position: "absolute",
    paddingVertical: hp("2%"),
    maxHeight: hp("50%"),
    overflow: "scroll",
  },
  pickerPopUpWrapper: {
    width: wp("100%"),
    height: hp("100%"),
    position: "absolute",
    alignItems: "center",
    justifyContent: "center",
  },
  pickerPopUpItem: {
    width: "100%",
    height: hp("5%"),
    borderBottomWidth: 1,
    justifyContent: "center",
    paddingHorizontal: "5%",
    borderColor: "orange",
  },
});

const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  get_user_details: state.main.get_user_details,
  post_job:state.main.post_job
});
export default connect(mapStateToProps, { postJob, getUserData })(PostProject);

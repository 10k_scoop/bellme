import { LinearGradient } from 'expo-linear-gradient';
import React from 'react'
import { Dimensions, Image, SafeAreaView, ScrollView, Text, TouchableOpacity, View } from 'react-native';
import AppColors from '../../Components/AppColors';
import { ArrowLeft, StarIcon } from '../../Components/SvgComponets';

const LiveLocation1 = (props) => {
    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: '#F5F5F5', }}>

            <View style={{flex:1, width: "80%", alignSelf: 'center', marginTop: Platform.OS == "android" ? "10%" : 0 }}>
                <View style={{ width: "100%", flexDirection: 'row', justifyContent: 'space-between' }}>
                    <TouchableOpacity
                        onPress={() => {
                            props.navigation.goBack();
                        }}
                    >
                        <ArrowLeft />
                    </TouchableOpacity>
                    <Text style={{ color: '#EF4136', fontFamily: 'RBo', fontSize: 20 }}>Work In Progress</Text>
                    <Text></Text>
                </View>

                <Image
                    style={{ alignSelf: 'center', marginTop: 30, width: 100, height: 100, borderRadius: 50, }}
                    source={require('../../assets/profileIcon2.png')}
                />
                <View style={{ alignSelf: 'center', paddingHorizontal: 15, paddingVertical: 5, backgroundColor: '#FBB040', borderRadius: 6, marginTop: 2 }}>
                    <Text style={{ color: "#FFFFFF", fontFamily: 'RRe', fontSize: 11 }}>CERTIFIED</Text>
                </View>
                <Text style={{ alignSelf: 'center', marginTop: 5, fontSize: 20, fontFamily: 'RBo' }}>Selena Adems</Text>
                <View style={{ flexDirection: 'row', marginTop: 5, alignSelf: 'center' }}>
                    <StarIcon />
                    <StarIcon />
                    <StarIcon />
                    <StarIcon />
                    <StarIcon />
                </View>
                <ScrollView 
                    contentContainerStyle={{flex:1}}
                    // style={{backgroundColor:'red',}}
                    >
                    <View style={{ width: "100%", height: 40, backgroundColor: 'white', borderRadius: 13, flexDirection: 'row', marginTop: 15, alignItems: 'center', justifyContent: 'space-between', paddingHorizontal: 20 }}>
                        <Text style={{ fontSize: 14, fontFamily: 'RRe', color: "#707070" }}>Company</Text>
                        <Text style={{ fontSize: 14, fontFamily: 'RRe', color: '#FBB040' }}>ABC.org</Text>
                    </View>

                    <TouchableOpacity
                        onPress={() => {
                            props.navigation.navigate('SPProfile')
                        }}
                        style={{ width: "100%", height: 40, marginTop: 20 }}>
                        <LinearGradient
                            style={{ flex: 1, borderRadius: 13, alignItems: 'center', justifyContent: 'center', }}
                            colors={[AppColors.linear1, AppColors.linear2]}
                        >
                            <Text style={{ color: 'white', fontFamily: 'RBo', fontSize: 14 }}>View Full Profile</Text>
                        </LinearGradient>
                    </TouchableOpacity>

                    <Text style={{ fontSize: 14, fontFamily: 'RBo', color: '#112D4E', marginTop: 25 }}>Quote</Text>
                    <View style={{width:"100%",paddingTop:10,paddingBottom:60,backgroundColor:'#FFFFFF',marginTop:10,borderRadius:13,paddingHorizontal:20}}>
                        <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                            <Text style={{color:"#707070",fontSize:14,fontFamily:"RRe"}}>Estimate Cost</Text>
                            <Text style={{color:'#EF4136',fontFamily:'RBo',fontSize:14}}>$200</Text>
                        </View>
                        <View style={{flexDirection:'row',justifyContent:'space-between',marginTop:15}}>
                            <Text style={{color:"#707070",fontSize:14,fontFamily:"RRe"}}>Time to Complete</Text>
                            <Text style={{color:'#EF4136',fontFamily:'RBo',fontSize:14}}>3 days</Text>
                        </View>
                        <Text style={{color:"#707070",fontSize:14,fontFamily:"RBo",marginTop:15}}>Remarks</Text>
                        <Text style={{color:"#707070",fontSize:10,fontFamily:"RRe",marginTop:7,lineHeight:13}}>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et</Text>
                    </View>
                    <View style={{position: 'absolute',bottom:10, flexDirection:'row',justifyContent:'space-between',}}>
                        <TouchableOpacity style={{width:"49%",backgroundColor:'#112D4E',height:40,borderRadius:13,justifyContent:'center',alignItems:'center'}}>
                            <Text style={{fontSize:14,fontFamily:'RBo',color:'#FFFFFF'}}>REPORT PROBLEM</Text>
                        </TouchableOpacity>
                        <TouchableOpacity 
                            onPress={()=>{
                                props.navigation.navigate('Feedback');
                            }}
                            style={{width:"48%",height:40}}>
                            <LinearGradient
                                style={{flex:1,borderRadius:13,justifyContent:'center',alignItems:'center'}}
                                colors={[AppColors.linear1,AppColors.linear2]}
                            >
                                <Text style={{fontSize:14,fontFamily:'RBo',color:'#FFFFFF'}}>COMPLETED</Text>
                            </LinearGradient>
                        </TouchableOpacity>

                    </View>
                </ScrollView>
                
                
            </View>
        </SafeAreaView>
    )
}

export default LiveLocation1

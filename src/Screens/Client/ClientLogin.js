import { LinearGradient } from "expo-linear-gradient";
import { StatusBar } from "expo-status-bar";
import React, { useEffect, useState } from "react";
import {
  SafeAreaView,
  View,
  Text,
  TextInput,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
  ActivityIndicator,
} from "react-native";
import { connect } from "react-redux";
import AppColors from "../../Components/AppColors";
import {
  ArrowLeft,
  FbIcon,
  ForgetPasswordSvg,
  GoogleIcon,
  LogoSvg,
  MsgIcon,
  ORSvg,
  PasswordIcon,
  RegisterSvg,
} from "../../Components/SvgComponets";
import * as firebase from "firebase";
import {
  login,
  facebookLogin,
  loginWithGoogle,
} from "../../state-management/actions/auth/AuthActions";
import * as GoogleSignIn from 'expo-google-sign-in';
const ClientLogin = (props) => {
  const [email, setEmail] = useState("");
  const [pass, setPass] = useState("");
  const [loading, setLoading] = useState(false);
  const [facebookLoading, setFacebookLoading] = useState(false);

  const onLogin = () => {
    setLoading(true);
    if (email != "" && pass != "") {
      let data = {
        email: email,
        pass: pass,
      };
      props.login(data, setLoading);
    } else {
      setLoading(false);
      alert("Fill all details");
    }
  };
  // "firebase": "^8.2.3",

  useEffect(() => {
   (async()=>{
    await GoogleSignIn.initAsync({
      // You may ommit the clientId when the firebase `googleServicesFile` is configured
      clientId: '184120454139-qg1k6k2m7j0of9tlb7mtpc3k1lq68atq.apps.googleusercontent.com',
    });
   })()
  }, [props]);

  const signInAsync = async () => {
    try {
      await GoogleSignIn.askForPlayServicesAsync();
      const { type, user } = await GoogleSignIn.signInAsync();
      if (type === 'success') {
        const user = await GoogleSignIn.signInSilentlyAsync();
        alert(JSON.stringify({ user }))
      }
    } catch ({ message }) {
      alert('login: Error:' + message);
    }
  };

  if (facebookLoading) {
    return (
      <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
        <ActivityIndicator size="large" color="orange" />
      </View>
    );
  }
  // const [request, response, promptAsync] = Google.useIdTokenAuthRequest({
  //   clientId:
  //     "184120454139-r25ifnofhb73lljk3oso3975vbe2jh3n.apps.googleusercontent.com",
  // });


  const sendGoogleLoginToApi = async (id_token) => {
    try {
    props?.loginWithGoogle()
    } catch (e) {
      console.log(e.message);
    }
  };
  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: "#F5F5F5" }}>
      <StatusBar hidden={true} />
      <View
        style={{
          marginTop: Platform.OS == "android" ? "15%" : "10%",
          width: "90%",
          alignItems: "center",
        }}
      >
        <View style={{ flexDirection: "row" }}>
          <TouchableOpacity style={{ marginRight: 10 }}>
            <ArrowLeft />
          </TouchableOpacity>
          <LogoSvg />
        </View>
      </View>
      <Text
        style={{
          alignSelf: "center",
          fontFamily: "RRe",
          fontSize: 14,
          color: "#707070",
          marginTop: 20,
        }}
      >
        Sign in to your CLIENT account
      </Text>
      <ScrollView contentContainerStyle={{ paddingBottom: 80 }}>
        <View
          style={{
            width: "85%",
            alignSelf: "center",
            alignItems: "center",
            marginTop: 20,
          }}
        >
          <View style={styles.textInputContainer}>
            <View style={{ position: "absolute", left: 15 }}>
              <MsgIcon />
            </View>
            <TextInput
              placeholder="Email Address"
              placeholderTextColor="#707070"
              style={styles.textInput}
              onChangeText={(val) => setEmail(val)}
            />
          </View>
          <View style={styles.textInputContainer}>
            <View style={{ position: "absolute", left: 15 }}>
              <PasswordIcon />
            </View>
            <TextInput
              placeholder="Password"
              placeholderTextColor="#707070"
              style={styles.textInput}
              onChangeText={(val) => setPass(val)}
              secureTextEntry={true}
            />
          </View>

          <TouchableOpacity
            onPress={() =>
              props.navigation.navigate("ClientAuthStack", {
                screen: "ForgotPassword",
              })
            }
            style={{ alignSelf: "center", marginTop: 40 }}
          >
            <ForgetPasswordSvg />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => (loading ? undefined : onLogin())}
            style={{ marginTop: 40, width: "100%", height: 40 }}
          >
            <LinearGradient
              style={{
                flex: 1,
                justifyContent: "center",
                alignItems: "center",
                borderRadius: 13,
              }}
              colors={[AppColors.linear1, AppColors.linear2]}
            >
              {loading ? (
                <ActivityIndicator size="small" color="white" />
              ) : (
                <Text
                  style={{ color: "#FFFFFF", fontSize: 14, fontFamily: "RBo" }}
                >
                  L O G I N
                </Text>
              )}
            </LinearGradient>
          </TouchableOpacity>
          <View style={{ marginTop: 25 }}>
            <ORSvg />
          </View>
          <TouchableOpacity
            style={{
              width: "100%",
              backgroundColor: "#005CAC",
              borderRadius: 13,
              height: 40,
              marginTop: 25,
              justifyContent: "center",
              alignItems: "center",
            }}
            onPress={() => {
              setFacebookLoading(true);
              props.facebookLogin(setFacebookLoading);
            }}
          >
            <View style={{ position: "absolute", left: 15 }}>
              <FbIcon />
            </View>
            <Text style={{ color: "#FFFFFF", fontSize: 14, fontFamily: "RRe" }}>
              Login with Facebook
            </Text>
          </TouchableOpacity>
          {/* <TouchableOpacity
            style={{
              width: "100%",
              backgroundColor: "#FF4600",
              borderRadius: 13,
              height: 40,
              marginTop: 15,
              justifyContent: "center",
              alignItems: "center",
            }}
            onPress={() => sendGoogleLoginToApi()}
          >
            <View style={{ position: "absolute", left: 15 }}>
              <GoogleIcon />
            </View>
            <Text style={{ color: "#FFFFFF", fontSize: 14, fontFamily: "RRe" }}>
              Login with Google
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              width: "100%",
              backgroundColor: "#FF4600",
              borderRadius: 13,
              height: 40,
              marginTop: 15,
              justifyContent: "center",
              alignItems: "center",
            }}
            onPress={() => signInAsync()}
          >
            <View style={{ position: "absolute", left: 15 }}>
              <GoogleIcon />
            </View>
            <Text style={{ color: "#FFFFFF", fontSize: 14, fontFamily: "RRe" }}>
              Login with Google New api
            </Text>
          </TouchableOpacity> */}
          <Text
            style={{
              fontFamily: "RRe",
              fontSize: 14,
              color: "#707070",
              marginTop: 30,
            }}
          >
            Don't have an account yet?
          </Text>
          <TouchableOpacity
            onPress={() => props.navigation.navigate("ClientRegister")}
            style={{ marginTop: 15 }}
          >
            <RegisterSvg />
          </TouchableOpacity>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  textInputContainer: {
    flexDirection: "row",
    backgroundColor: "#E4E4E4",
    borderRadius: 13,
    height: 40,
    width: "100%",
    paddingLeft: 15,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 20,
  },
  textInput: {
    fontFamily: "RRe",
    fontSize: 14,
    color: "#707070",
    width: "80%",
    textAlign: "center",
  },
});

const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  login_details: state.main.login_details,
  facebook_login_details: state.main.facebook_login_details,
});
export default connect(mapStateToProps, {
  login,
  facebookLogin,
  loginWithGoogle,
})(ClientLogin);

import { LinearGradient } from "expo-linear-gradient";
import React, { useState, useEffect } from "react";
import {
  SafeAreaView,
  View,
  Text,
  TouchableOpacity,
  Image,
  StyleSheet,
  Share,
  ActivityIndicator,
  TextInput,
} from "react-native";
import AppColors from "../../Components/AppColors";
import { StarIcon } from "../../Components/SvgComponets";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { connect } from "react-redux";
import {
  getUserData,
  myJobs,
  profileUpdate,
} from "../../state-management/actions/features/Features";
import * as firebase from "firebase";

const Profile = (props) => {
  const [loading, setLoading] = useState(true);
  const [data, setData] = useState([]);
  const user = firebase.auth().currentUser;
  const [jobs, setJobs] = useState([]);
  const [editProfile, setEditProfile] = useState(false);

  const [address, setAddress] = useState("");
  const [bio, setBio] = useState("");
  const [number, setNumber] = useState("");
  const [username, setUsername] = useState("");

  useEffect(() => {
    props.getUserData(user?.uid, setLoading);
    props.myJobs(user?.email, setLoading);
  }, []);

  useEffect(() => {
    if (props.get_user_details != null) {
      setData(props.get_user_details);
    }
    if (props.my_jobs != null) {
      setJobs(props.my_jobs);
    }
  }, [props]);

  if (loading) {
    return (
      <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
        <ActivityIndicator color="black" size="large" />
      </View>
    );
  }

  const onProfileUpdate = async () => {
    setLoading(true);
    let data = {
      address: address,
      phone: number,
      bio: bio,
      username: username,
    };
    await props?.profileUpdate(data, user?.uid, setLoading, props?.navigation,"Profile");
  };

  const onShare = async () => {
    try {
      const result = await Share.share({
        message:
          `Hey! Looking for service providers.${'\n'}${'\n'}Download me now!`,
      });
      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // shared with activity type of result.activityType
        } else {
          // shared
        }
      } else if (result.action === Share.dismissedAction) {
        // dismissed
      }
    } catch (error) {
      alert(error.message);
    }
  };
  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: "#F5F5F5" }}>
      <Text
        style={{
          fontSize: 20,
          fontFamily: "RBo",
          color: "#EF4136",
          alignSelf: "center",
          marginTop: Platform.OS == "android" ? "10%" : 0,
        }}
      >
        My Profile
      </Text>
      <Image
        style={{
          alignSelf: "center",
          marginTop: 20,
          width: 100,
          height: 100,
          borderRadius: 100,
        }}
        source={
          data?.profileImg?.length >= 1
            ? { uri: data?.profileImg }
            : require("../../assets/profileIcon.png")
        }
      />
      <Text style={[styles.text, { textAlign: "center", marginTop: 10 }]}>
        {data?.name}
      </Text>

      <View style={{ marginTop: 30, width: "80%", alignSelf: "center" }}>
        {editProfile && (
          <View style={styles.infoFormWrapper}>
            <View style={styles.inputWrapper}>
              <TextInput
                placeholder="Address"
                style={styles.inputField}
                placeholderTextColor="#222"
                onChangeText={(val) => setAddress(val)}
                value={address}
              />
            </View>
            <View style={styles.inputWrapper}>
              <TextInput
                placeholder="Phone Number"
                style={styles.inputField}
                placeholderTextColor="#222"
                onChangeText={(val) => setNumber(val)}
                keyboardType="number-pad"
                value={number}
              />
            </View>
            <View style={styles.inputWrapper}>
              <TextInput
                placeholder="Username"
                style={styles.inputField}
                placeholderTextColor="#222"
                onChangeText={(val) => setUsername(val)}
                value={username}
              />
            </View>
            <View
              style={[styles.inputWrapper, { minHeight: hp("6%"), padding: 5 }]}
            >
              <TextInput
                placeholder="Bio"
                style={styles.inputField}
                multiline
                placeholderTextColor="#222"
                value={bio}
                onChangeText={(val) => setBio(val)}
              />
            </View>
          </View>
        )}
        {!editProfile && (
          <>
            <View style={styles.infoBox}>
              <Text style={styles.text}>Address</Text>
              <Text style={styles.colorText}>{data?.address}</Text>
            </View>
            <View style={styles.infoBox}>
              <Text style={styles.text}>Phone Number</Text>
              <Text style={styles.colorText}>{data?.phone}</Text>
            </View>
            <View style={styles.infoBox}>
              <Text style={styles.text}>username</Text>
              <Text style={styles.colorText}>{data?.username}</Text>
            </View>
          </>
        )}
        {editProfile && (
          <View
            style={{ flexDirection: "row", justifyContent: "space-around" }}
          >
            <TouchableOpacity
              onPress={() => {
                setEditProfile(false);
              }}
              style={{ width: "45%", height: 40, marginTop: 30 }}
            >
              <LinearGradient
                style={{
                  flex: 1,
                  borderRadius: 13,
                  alignItems: "center",
                  justifyContent: "center",
                }}
                colors={[AppColors.linear1, AppColors.linear2]}
              >
                <Text
                  style={{ color: "white", fontFamily: "RBo", fontSize: 14 }}
                >
                  C A N C E L
                </Text>
              </LinearGradient>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                editProfile ? onProfileUpdate() : setEditProfile(!editProfile);
              }}
              style={{ width: "45%", height: 40, marginTop: 30 }}
            >
              <LinearGradient
                style={{
                  flex: 1,
                  borderRadius: 13,
                  alignItems: "center",
                  justifyContent: "center",
                }}
                colors={[AppColors.linear1, AppColors.linear2]}
              >
                <Text
                  style={{ color: "white", fontFamily: "RBo", fontSize: 14 }}
                >
                  U P D A T E
                </Text>
              </LinearGradient>
            </TouchableOpacity>
          </View>
        )}
        {!editProfile && (
          <TouchableOpacity
            onPress={() => {
              editProfile ? onProfileUpdate() : setEditProfile(!editProfile);
            }}
            style={{ width: "100%", height: 40, marginTop: 30 }}
          >
            <LinearGradient
              style={{
                flex: 1,
                borderRadius: 13,
                alignItems: "center",
                justifyContent: "center",
              }}
              colors={[AppColors.linear1, AppColors.linear2]}
            >
              <Text style={{ color: "white", fontFamily: "RBo", fontSize: 14 }}>
                {editProfile ? "U P D A T E" : "E D I T P R O F I L E"}
              </Text>
            </LinearGradient>
          </TouchableOpacity>
        )}
        <TouchableOpacity
          onPress={() => {
            props.navigation.navigate("ProfileStack", {
              screen: "Project1",
              params: { data: "jobs" },
            });
          }}
          style={[styles.infoBox, { marginTop: 30 }]}
        >
          <Text style={styles.text}>{jobs?.length} Projects Posted</Text>
          <Text style={styles.colorText}>View Projects</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.Referal}>
        <TouchableOpacity
          onPress={onShare}
          style={{ width: "100%", height: 40, marginTop: 35 }}
        >
          <LinearGradient
            style={{
              flex: 1,
              borderRadius: 13,
              alignItems: "center",
              justifyContent: "center",
            }}
            colors={[AppColors.linear1, AppColors.linear2]}
          >
            <Text style={{ color: "white", fontFamily: "RBo", fontSize: 14 }}>
              COPY R E F E R A L
            </Text>
          </LinearGradient>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  infoBox: {
    width: "100%",
    height: 40,
    alignItems: "center",
    backgroundColor: "#FFFFFF",
    borderRadius: 13,
    flexDirection: "row",
    paddingHorizontal: 12,
    justifyContent: "space-between",
    marginTop: 10,
  },
  text: {
    color: "#707070",
    fontFamily: "RRe",
    fontSize: 14,
  },
  colorText: {
    color: "#FBB040",
    fontFamily: "RRe",
    fontSize: 14,
  },
  Referal: {
    width: wp("90%"),
    height: hp("20%"),
    paddingHorizontal: wp("4%"),
    justifyContent: "center",
    alignItems: "center",
    justifyContent: "flex-end",
    marginHorizontal:wp('5%'),
    bottom:hp('5%')
  },
  infoFormWrapper: {
    marginTop: 1,
    padding: 10,
  },
  inputWrapper: {
    backgroundColor: "#fff",
    width: "100%",
    height: hp("4%"),
    borderRadius: 10,
    paddingHorizontal: 10,
    marginVertical: 10,
    justifyContent: "center",
  },
  inputField: {
    width: "100%",
    height: "100%",
    fontSize: rf(10),
    color: "#222",
  },
});

const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  get_user_details: state.main.get_user_details,
  my_jobs: state.main.my_jobs,
});
export default connect(mapStateToProps, { getUserData, myJobs, profileUpdate })(
  Profile
);

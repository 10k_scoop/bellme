import React, { useCallback } from 'react'
import { TouchableOpacity, View, Text, Platform, StyleSheet, TextInput, FlatList } from 'react-native'
import { SafeAreaView } from 'react-native-safe-area-context'
import { ArrowDown, ArrowLeft, CameraGradientIcon, SwipeRedRight, CleaningIcon, PlumbingIcon, WasteIcon } from '../../Components/SvgComponets'
import { LinearGradient } from 'expo-linear-gradient';
import AppColors from '../../Components/AppColors';


const SubCategories = (props) => {

    const categoriesArray = [
        {
            icon: CleaningIcon,
            name: "Construction",
        },
        {
            icon: PlumbingIcon,
            name: "Carpentry",
        },
        {
            icon: WasteIcon,
            name: "Waste Mgt",
        },
        {
            icon: CleaningIcon,
            name: "Plumbing",
        },
        {
            icon: PlumbingIcon,
            name: "Tutor",
        },
        {
            icon: WasteIcon,
            name: "Services",
        },
        {
            icon: CleaningIcon,
            name: "Mechanics",
        },
        {
            icon: PlumbingIcon,
            name: "Electrical",
        },
        {
            icon: WasteIcon,
            name: "House Maintenance",
        },
        {
            icon: WasteIcon,
            name: "Events",
        },
    ]

    const keyExtractor = useCallback((item, index) => index.toString(), []);
    const renderCategories = useCallback(({ item, index }) => {
        var Icon = item.icon
        return (

            <TouchableOpacity
                onPress={() => {
                    props.navigation.navigate('PostProject',{selectedCategory:item.name,selectedCategoryIndex:index});
                }}
                style={{ width: "100%", flexDirection: "row", marginTop: 15, alignItems: 'center', justifyContent: 'space-between', borderBottomWidth: 0.5, borderColor: '#112D4E', paddingBottom: 15 }}>
                <Text style={{ color: '#707070', fontFamily: 'RRe', fontSize: 18, letterSpacing: 0.5 }}>{item.name}</Text>
                <SwipeRedRight />
            </TouchableOpacity>


        )
    }, [])


    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: '#F5F5F5', }}>
            <View style={{ height: "100%", width: "87%", alignSelf: 'center', marginTop: Platform.OS == "android" ? "10%" : 0 }}>
                <View style={{ width: "100%", flexDirection: 'row', justifyContent: 'space-between' }}>
                    <TouchableOpacity
                        onPress={() => {
                            props.navigation.goBack();
                        }}
                    >
                        <ArrowLeft />
                    </TouchableOpacity>
                    <Text style={{ color: '#EF4136', fontFamily: 'RBo', fontSize: 20 }}>Sub Categories</Text>
                    <Text></Text>
                </View>
                {/* <View style={{ marginTop: 10, flexDirection: 'row', width: "100%" }}> */}
                <FlatList
                    data={categoriesArray}
                    showsVerticalScrollIndicator={false}
                    style={{ marginTop: 30, flex: 1, }}
                    contentContainerStyle={{}}
                    // numColumns={3}
                    // columnWrapperStyle={{ marginTop: 20, }}
                    keyExtractor={keyExtractor}
                    renderItem={renderCategories}
                    ListFooterComponentStyle={{ flex: 1, height: 200, width: 340, marginLeft: 20 }}

                />

                {/* </View> */}
            </View>
        </SafeAreaView>
    )
}

export default SubCategories

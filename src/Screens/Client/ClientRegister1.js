import { LinearGradient } from "expo-linear-gradient";
import { StatusBar } from "expo-status-bar";
import React, { useState, useEffect } from "react";
import {
  SafeAreaView,
  View,
  Text,
  TextInput,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
  Dimensions,
  ActivityIndicator,
  Image,
} from "react-native";
import AppColors from "../../Components/AppColors";
import {
  ArrowLeft,
  CameraIcon,
  LoginSvg,
  LogoSvg,
  MsgIcon,
  ORSvg,
  PasswordIcon,
  PersonIcon,
  RegisterSvg,
} from "../../Components/SvgComponets";
import * as ImagePicker from "expo-image-picker";
import { signup } from "../../state-management/actions/auth/AuthActions";
import { connect } from "react-redux";

const ClientRegister1 = (props) => {
  const [loading, setLoading] = useState(false);
  const [username, setUsername] = useState("");
  const [phone, setPhone] = useState("");
  const [address, setAddress] = useState("");
  const [profileImg, setProfileImg] = useState(null);

  const onSubmit = () => {
    var prevData = props.route.params.data;
    if (username != "" && phone != "") {
      let data = {
        name: prevData.name,
        email: prevData.email.toLowerCase(),
        pass: prevData.pass,
        username: username,
        phone: phone,
        address: address,
        profileImg: profileImg,
        role: "client",
        verified: false,
      };

      setLoading(true);
      props.signup(data, setLoading);
    } else {
      alert("Fill all details");
    }
  };

  useEffect(() => {
    (async () => {
      if (Platform.OS !== "web") {
        const {
          status,
        } = await ImagePicker.requestMediaLibraryPermissionsAsync();
        if (status !== "granted") {
          // alert("Sorry, we need camera roll permissions to make this work!");
          await ImagePicker.requestCameraRollPermissionsAsync();
        }
      }
    })();
  }, []);

  const pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [4, 3],
      quality: 0,
    });

    if (!result.cancelled) {
      setProfileImg(result.uri);
    }
  };

  if (loading) {
    return (
      <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
        <ActivityIndicator size="large" color="orange" />
      </View>
    );
  }

  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: "#F5F5F5" }}>
      <StatusBar hidden={true} />
      <View
        style={{
          marginTop: Platform.OS == "android" ? "15%" : "10%",
          width: "90%",
          alignItems: "center",
        }}
      >
        <View style={{ flexDirection: "row" }}>
          <TouchableOpacity
            onPress={() => {
              props.navigation.goBack();
            }}
            style={{ marginRight: 10 }}
          >
            <ArrowLeft />
          </TouchableOpacity>
          <LogoSvg />
        </View>
      </View>
      <Text
        style={{
          fontFamily: "RRe",
          fontSize: 14,
          color: "#707070",
          marginTop: 20,
          alignSelf: "center",
        }}
      >
        Lets complete your client profile
      </Text>
      <ScrollView contentContainerStyle={{ paddingBottom: 80 }}>
        <View
          style={{
            width: "85%",
            alignSelf: "center",
            alignItems: "center",
            marginTop: 20,
          }}
        >
          <TouchableOpacity
            style={[styles.addPhoto]}
            onPress={() => pickImage()}
          >
            {profileImg && (
              <Image
                source={{ uri: profileImg }}
                resizeMode="cover"
                style={{ width: "100%", height: "100%" }}
              />
            )}
            <View style={{ position: "absolute", alignItems: "center" }}>
              <CameraIcon />
              <Text style={[styles.textInput, { marginTop: 10 }]}>
                Add a Photo
              </Text>
            </View>
          </TouchableOpacity>
          <View style={styles.textInputContainer}>
            <TextInput
              placeholder="username"
              placeholderTextColor="#707070"
              style={styles.textInput}
              onChangeText={(val) => setUsername(val)}
            />
          </View>
          <View style={styles.textInputContainer}>
            <TextInput
              placeholder="Phone Number"
              placeholderTextColor="#707070"
              style={styles.textInput}
              onChangeText={(val) => setPhone(val)}
              keyboardType="number-pad"
            />
          </View>
          <View style={styles.textInputContainer}>
            <TextInput
              placeholder="Address"
              placeholderTextColor="#707070"
              style={styles.textInput}
              onChangeText={(val) => setAddress(val)}
            />
          </View>

          <TouchableOpacity
            onPress={() => onSubmit()}
            style={{ marginTop: 60, width: "100%", height: 40 }}
          >
            <LinearGradient
              style={{
                flex: 1,
                justifyContent: "center",
                alignItems: "center",
                borderRadius: 13,
              }}
              colors={[AppColors.linear1, AppColors.linear2]}
            >
              <Text
                style={{ color: "#FFFFFF", fontSize: 14, fontFamily: "RBo" }}
              >
                S A V E M Y P R O F I L E
              </Text>
            </LinearGradient>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  textInputContainer: {
    flexDirection: "row",
    backgroundColor: "#E4E4E4",
    borderRadius: 13,
    height: 40,
    width: "100%",
    paddingLeft: 15,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 25,
  },
  textInput: {
    fontFamily: "RRe",
    fontSize: 14,
    color: "#707070",
  },
  addPhoto: {
    width: 200,
    height: 200,
    borderRadius: 200,
    backgroundColor: "#E4E4E4",
    alignItems: "center",
    justifyContent: "center",
    marginVertical: 10,
    overflow: "hidden",
  },
});
const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  signup_res: state.main.signup_res,
});
export default connect(mapStateToProps, { signup })(ClientRegister1);

import React, { useState } from "react";
import {
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  ScrollView,
  Switch,
} from "react-native";
import {
  NotificationIcon,
  PasswordIcon,
  PaymentMethodIcon,
  PersonIcon,
} from "../../Components/SvgComponets";
// import Switch from 'react-native-customisable-switch';
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { connect } from "react-redux";
import { logout } from "../../state-management/actions/auth/AuthActions";
import * as firebase from "firebase";
const Settings = (props) => {
  const [switchNotification, setSwitchNotification] = useState(false);
  const [isEnabled, setIsEnabled] = useState(false);
  const toggleSwitch = () => setIsEnabled((previousState) => !previousState);
  const user = firebase.auth().currentUser;

  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: "#F5F5F5" }}>
      <Text
        style={{
          fontSize: 20,
          fontFamily: "RBo",
          color: "#EF4136",
          alignSelf: "center",
          marginTop: Platform.OS == "android" ? "10%" : 0,
        }}
      >
        Settings
      </Text>

      <View style={{ width: "80%", alignSelf: "center", marginTop: 30 }}>
        <ScrollView>
          {/* <TouchableOpacity
            onPress={() =>
              props.navigation.navigate("SettingsStack", {
                screen: "PostProject3",
              })
            }
            style={styles.optionsBox}
          >
            <PaymentMethodIcon />
            <Text style={styles.optionsText}>Payment Methods</Text>
          </TouchableOpacity> */}
          {/* <TouchableOpacity
            onPress={() =>
              props.navigation.navigate("ClientAuthStack", {
                screen: "ClientRegister1",
              })
            }
            style={styles.optionsBox}
          >
            <PersonIcon />
            <Text style={styles.optionsText}> Change Username</Text>
          </TouchableOpacity> */}
          <TouchableOpacity
            onPress={() => {
              user?.providerData[0]?.providerId == "password" &&
                props.navigation.navigate("HomeStack", {
                  screen: "ChangePassword",
                });
            }}
            style={styles.optionsBox}
          >
            <PasswordIcon />
            <Text style={styles.optionsText}> Change Password</Text>
          </TouchableOpacity>
          <View style={[styles.optionsBox]}>
            <NotificationIcon />
            <Text style={styles.optionsText}> Allow Notifications</Text>
            <View style={styles.container}>
              <Switch
                trackColor={{ false: "#F2F2F7", true: "#F2F2F7" }}
                thumbColor={isEnabled ? "orange" : "#F2F2F7"}
                ios_backgroundColor="#fff"
                onValueChange={toggleSwitch}
                value={isEnabled}
              />
            </View>
          </View>
          {/* 
          <TouchableOpacity
            onPress={() =>
              props.navigation.navigate("SettingsStack", {
                screen: "BlockList",
              })
            }
            style={[styles.optionsBox, { justifyContent: "center" }]}
          >
            <Text style={{ fontFamily: "RBo", fontSize: 14, color: "#EF4136" }}>
              Block List
            </Text>
          </TouchableOpacity> */}
          <TouchableOpacity
            style={[styles.optionsBox, { justifyContent: "center" }]}
           
          >
            <Text style={{ fontFamily: "RBo", fontSize: 14, color: "#EF4136" }}>
              FAQs
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={[styles.optionsBox, { justifyContent: "center" }]}
            onPress={() => {
              props.navigation.navigate("HomeStack", {
                screen: "TermsAndCondition",
              });
            }}
          >
            <Text style={{ fontFamily: "RBo", fontSize: 14, color: "#EF4136" }}>
              Terms & Conditions
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              props.navigation.navigate("HomeStack", { screen: "GoPremium" });
            }}
            style={[styles.optionsBox, { justifyContent: "center" }]}
          >
            <Text style={{ fontFamily: "RBo", fontSize: 14, color: "#EF4136" }}>
              Go Premium
            </Text>
          </TouchableOpacity>
          {/* <TouchableOpacity
            style={{
              height: 40,
              width: "100%",
              backgroundColor: "#FF0000",
              borderRadius: 13,
              marginTop: 25,
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <Text style={{ color: "#FFFFFF", fontFamily: "RBo", fontSize: 14 }}>
              Delete Account
            </Text>
          </TouchableOpacity> */}
          <TouchableOpacity
            onPress={() => props.logout()}
            style={{
              height: 40,
              width: "100%",
              backgroundColor: "#363636",
              borderRadius: 13,
              marginTop: 20,
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <Text style={{ color: "#FFFFFF", fontFamily: "RBo", fontSize: 14 }}>
              L O G O U T
            </Text>
          </TouchableOpacity>
        </ScrollView>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  optionsBox: {
    width: "100%",
    height: 40,
    backgroundColor: "#FFFFFF",
    borderRadius: 13,
    flexDirection: "row",
    paddingHorizontal: 15,
    alignItems: "center",
    marginTop: 15,
  },
  optionsText: {
    color: "#112D4E",
    fontSize: 14,
    fontFamily: "RRe",
    marginLeft: 12,
  },
  container: {
    position: "absolute",
    right: 10,
    // flex: 1,
    // backgroundColor: 'red',
    // justifyContent: 'center',
    // alignItems: 'center',
  },
});

const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  logout: state.main.logout,
});
export default connect(mapStateToProps, { logout })(Settings);

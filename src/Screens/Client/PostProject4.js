import { LinearGradient } from 'expo-linear-gradient';
import React ,{useState}from 'react'

import { SafeAreaView, TouchableOpacity, View, Text, TextInput } from 'react-native';
import AppColors from '../../Components/AppColors';
import { ArrowLeft, CirclesIcon, ThreeDotsVert, VisaIcon } from '../../Components/SvgComponets';

const PostProject4 = (props) => {

    const [cardNo, setCardNo] = useState('')
    return (

        <SafeAreaView style={{ flex: 1, backgroundColor: '#F5F5F5', }}>

            <View style={{ height: "100%", width: "80%", alignSelf: 'center', marginTop: Platform.OS == "android" ? "10%" : 0 }}>
                <View style={{ width: "100%", flexDirection: 'row', justifyContent: 'space-between' }}>
                    <TouchableOpacity
                        onPress={() => {
                            props.navigation.goBack();
                        }}
                    >
                        <ArrowLeft />
                    </TouchableOpacity>
                    <Text style={{ color: '#EF4136', fontFamily: 'RBo', fontSize: 20 }}>New Card</Text>
                    <Text></Text>
                </View>
                
                <View style={{overflow:'hidden', paddingVertical:20, paddingLeft:20,paddingRight:15,backgroundColor:'white',marginTop:50,borderRadius:20}}>
                    <View style={{position: 'absolute',top:-5,right: -20,}}>
                        <CirclesIcon/>
                    </View>
                    <Text style={{fontFamily:'RBo',fontSize:12,color:"#EF4136"}}>VISA</Text>
                    <TextInput
                        placeholderTextColor="#112D4E"
                        placeholder="Title"
                        style={{width:130,borderBottomWidth:1,borderColor:'#E4E4E4',fontSize:12,fontFamily:'RBo',marginTop:20,paddingLeft:10}}
                    />
                    <TextInput
                        placeholderTextColor="#112D4E"
                        maxLength={25}
                        keyboardType="numeric"
                        onChangeText={(text)=>{
                            // setCardNo(text)
                            setCardNo(text.replace(/\s?/g, '').replace(/(\d{4})/g, '$1   ').trim())
                            // console.log(cardNo)
                        }}
                        
                        placeholder=" - - - -    - - - -    - - - -  1234 "
                        value = {cardNo}

                        // placeholder=" - - - -    - - - -    - - - -    1 2 3 4"
                        style={{width:"90%",borderBottomWidth:1,borderColor:'#E4E4E4',fontSize:12,fontFamily:'RBo',marginTop:20,paddingLeft:10}}
                    />
                    <View style={{flexDirection:'row'}}>
                    <TextInput
                        placeholderTextColor="#112D4E"
                        placeholder="MM"
                        maxLength={2}
                        keyboardType="numeric"
                        style={{width:45,borderBottomWidth:1,borderColor:'#E4E4E4',fontSize:12,fontFamily:'RBo',marginTop:20,paddingLeft:5}}
                    />  
                     <TextInput
                        placeholderTextColor="#112D4E"
                        placeholder="YYYY"
                        maxLength={4}
                        keyboardType="numeric"
                        style={{width:45,marginLeft:20, borderBottomWidth:1,borderColor:'#E4E4E4',fontSize:12,fontFamily:'RBo',marginTop:20,paddingLeft:5}}
                    /> 
                     <TextInput
                        placeholderTextColor="#112D4E"
                        placeholder="CVV"
                        style={{width:42,position: 'absolute',right: 10, borderBottomWidth:1,borderColor:'#E4E4E4',fontSize:12,fontFamily:'RBo',marginTop:20,paddingLeft:5}}
                    /> 

                    </View>
                </View>

            </View>


            <TouchableOpacity
                onPress={() => {
                    props.navigation.navigate('PostProject3')
                }}
                style={{ position: 'absolute', bottom: 30, width: "80%", alignSelf: 'center', height: 40 }}>
                <LinearGradient
                    style={{ flex: 1, borderRadius: 13, alignItems: 'center', justifyContent: 'center', }}
                    colors={[AppColors.linear1, AppColors.linear2]}
                >
                    <Text style={{ color: 'white', fontFamily: 'RBo', fontSize: 14 }}>E N T E R   C A R D</Text>
                </LinearGradient>

            </TouchableOpacity>
        </SafeAreaView>
    )
}

export default PostProject4

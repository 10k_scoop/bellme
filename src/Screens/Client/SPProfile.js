import MaskedView from '@react-native-community/masked-view';
import { LinearGradient } from 'expo-linear-gradient';
import React from 'react'
import { SafeAreaView, View, TouchableOpacity, Text, StyleSheet, Image, ScrollView } from 'react-native';
import ImageSlider from 'react-native-image-slider';
import AppColors from '../../Components/AppColors';
import { ArrowLeft, StarIcon, WhiteThreeDots } from '../../Components/SvgComponets';

const SPProfile = (props) => {

    const images = [
        require('../../assets/img1.png'),
        require('../../assets/img2.png'),
        require('../../assets/img3.png'),
    ];

    const Review = () => (
        <View style={styles.reviewBox}>
            
            <Image
                style={{ width: 40, height: 40, borderRadius: 13, }}
                source={require("../../assets/icon1.png")}
            />
            <View style={{ marginLeft: 10, flex: 1 }}>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', }}>
                    <Text style={{ color: '#EF4136', fontSize: 12, fontFamily: 'RBo' }}>Sara L</Text>
                    <View style={{ flexDirection: 'row' }}>
                        <StarIcon />
                        <StarIcon style={{ marginLeft: 2 }} />
                        <StarIcon style={{ marginLeft: 2 }} />
                        <StarIcon style={{ marginLeft: 2 }} />
                        <StarIcon style={{ marginLeft: 2 }} />
                    </View>
                </View>
                <Text style={{ fontSize: 10, fontFamily: 'RRe', color: "#112D4E", lineHeight: 13 }}>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut</Text>
            </View>

        </View>
    )
    return (

        <View style={{ flex: 1, backgroundColor: '#F5F5F5' }}>
           
            <View style={{ width: "100%", height: 250, backgroundColor: '#F5F5F5' }}>
                <ImageSlider
                    style={{ width: "100%", backgroundColor: '#F5F5F5', borderRadius: 40 }}
                    images={images} />
            </View>
            <View style={{ position: 'absolute',top:40, flexDirection:'row',justifyContent:'space-between',width:"80%",alignSelf:'center'}}>
                <TouchableOpacity 
                    onPress={()=>props.navigation.goBack()}
                >
                    <ArrowLeft/>
                </TouchableOpacity>
                <TouchableOpacity>
                    <WhiteThreeDots/>
                </TouchableOpacity>
            </View>
            <ScrollView contentContainerStyle={{ paddingBottom: 50 }} >
                <View style={{ width: "80%", alignSelf: 'center', marginTop: 20 }}>

                    <View style={{ flexDirection: 'row', width: "100%", justifyContent: 'space-between' }}>
                        <Text style={{ color: '#707070', fontSize: 14, fontFamily: "RRe" }}>32 Projects Completed</Text>
                        <View style={{ flexDirection: 'row' }}>
                            <StarIcon />
                            <StarIcon style={{ marginLeft: 3 }} />
                            <StarIcon style={{ marginLeft: 3 }} />
                            <StarIcon style={{ marginLeft: 3 }} />
                            <StarIcon style={{ marginLeft: 3 }} />
                        </View>
                    </View>
                    <MaskedView
                        style={{ width: "100%", height: 30, marginTop: 25 }}
                        maskElement={<Text style={{ fontSize: 25, fontFamily: 'RBo', }}>Salena Adems</Text>}
                    >
                        <LinearGradient
                            colors={[AppColors.linear1, AppColors.linear2]}
                            start={{ x: 1, y: 0 }}
                            end={{ x: 1, y: 1 }}
                            style={{ flex: 1 }}
                        />
                    </MaskedView>
                    <Text style={{ marginTop: 5, fontFamily: 'RRe', fontSize: 12, color: "#707070", lineHeight: 16 }}>Hi, I am Selena, your Architect. I have specialized in House design and interior decoration. Swipe my profile to see my past project and feel free to reach out to me if you have any questions.  Thanks :)</Text>
                    <Text style={{ marginTop: 50, fontFamily: 'RBo', fontSize: 12 }}>Reviews</Text>

                    <Review />
                    <Review />
                    <Review />

                    <TouchableOpacity
                        onPress={() => {
                            props.navigation.navigate('ClientBottomTabNavigator', { screen: "Home" })
                        }}
                        style={{ width: "100%", height: 40, marginTop: 20 }}>
                        <LinearGradient
                            style={{ flex: 1, borderRadius: 13, alignItems: 'center', justifyContent: 'center', }}
                            colors={[AppColors.linear1, AppColors.linear2]}
                        >
                            <Text style={{ color: 'white', fontFamily: 'RBo', fontSize: 14 }}>G E T   A   Q U O T E</Text>
                        </LinearGradient>
                    </TouchableOpacity>


                </View>
            </ScrollView>
        </View>
    )
}
const styles = StyleSheet.create({
    reviewBox: {
        width: "100%",
        paddingVertical: 15,
        paddingHorizontal: 10,
        flexDirection: 'row',
        backgroundColor: '#fff',
        borderRadius: 20,
        marginTop: 12
    }
})
export default SPProfile

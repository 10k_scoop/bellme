import { LinearGradient } from "expo-linear-gradient";
import React, { useEffect, useState } from "react";
import {
  Dimensions,
  Image,
  SafeAreaView,
  ScrollView,
  Text,
  TouchableOpacity,
  ActivityIndicator,
  View,
  Platform,
  KeyboardAvoidingView,
  StyleSheet,
} from "react-native";
import { connect } from "react-redux";
import AppColors from "../../Components/AppColors";
import { ArrowLeft, StarIcon } from "../../Components/SvgComponets";
import {
  viewJob,
  getSingleQuoteDetails,
} from "../../state-management/actions/features/Features";
import * as firebase from "firebase";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import {
  completeJobByClient,
  deleteAJob,
  getAnyUserData,
} from "../../state-management/actions/features/Features";
import { FontAwesome5, AntDesign } from "@expo/vector-icons";
import { RFValue as rf } from "react-native-responsive-fontsize";
import { TextInput } from "react-native-gesture-handler";
const JobStatus = (props) => {
  const [loading, setLoading] = useState(true);
  const [starRating, setStarRating] = useState(1);
  const [ratingBox, setRatingBox] = useState(false);
  const [feedback, setFeedback] = useState();
  const [aData, setAData] = useState();
  const user = firebase.auth().currentUser;
  const [data, setData] = useState(true);
  let jobData =
    props.route.params?.route == "noti"
      ? props.route.params?.jobId
      : props.route.params?.data;

  useEffect(() => {
    props.getSingleQuoteDetails(
      props.route.params?.route == "noti" ? jobData : jobData?.jobDetails?.id,
      setLoading
    );
    props?.viewJob(
      props.route.params?.route == "noti" ? jobData : jobData?.jobDetails?.id,
      setLoading
    );
  }, []);

  const refresh = () => {
    props.getSingleQuoteDetails(
      props.route.params?.route == "noti" ? jobData : jobData?.jobDetails?.id,
      setLoading
    );
    props?.viewJob(
      props.route.params?.route == "noti" ? jobData : jobData?.jobDetails?.id,
      setLoading
    );
  };

  useEffect(() => {
    if (props?.view_job != "") {
      setData(props?.view_job);
      props?.view_job?.hiredId
        ? props?.getAnyUserData(props?.view_job?.hiredId, setLoading, setAData)
        : null;
    }
  }, [props]);

  const deleteJob = async () => {
    setLoading(true);
    await props?.deleteAJob(
      props.route.params?.route == "noti" ? jobData : jobData?.jobDetails?.id,
      setLoading,
      props?.navigation
    );
  };

  const completeAJob = async (res) => {
    try {
      let prevRatings = aData?.rating;
      prevRatings.push({
        stars: starRating,
        feedback: feedback,
        author: user?.email,
      });
      let data = {
        rating: prevRatings,
        handyManId: res?.hiredId,
        clientId: res?.authorId,
        jobId:
          props.route.params?.route == "noti"
            ? jobData
            : jobData?.jobDetails?.id,
        status: "applied",
        type: "half-complete",
        message:
          "Congrats You got " +
          starRating +
          " rating from " +
          user?.email +
          " Job is marked as completed, click to completed on your side.",
        to: res?.hiredEmail,
        from: user?.email,
      };
      setLoading(true);
      await props?.completeJobByClient(data, setLoading);
      setRatingBox(false);
      refresh();
    } catch (e) {
      console.log(e.message);
    }
  };

  const onRatingStars = (val) => {
    setStarRating(val);
  };

  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: "#F5F5F5" }}>
      {ratingBox && (
        <View style={styles.ratingBoxWrapper}>
          <KeyboardAvoidingView
            behavior={Platform.OS === "ios" ? "padding" : "height"}
            style={{ flex: 1 }}
          >
            <ScrollView style={{ flex: 1, zIndex: 999999999999 }}>
              <View style={{ flex: 1 }}>
                <View style={styles.ratingBox}>
                  <Text style={styles.ratingText}>
                    Give provider a feedback
                  </Text>
                  <FontAwesome5
                    name="smile-beam"
                    size={rf(45)}
                    color="#ffba00"
                  />
                  <View style={styles.starsWrapper}>
                    <TouchableOpacity onPress={() => onRatingStars(1)}>
                      <AntDesign
                        name="star"
                        size={rf(25)}
                        color={starRating >= 1 ? "#ffd800" : "grey"}
                      />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => onRatingStars(2)}>
                      <AntDesign
                        name="star"
                        size={rf(25)}
                        color={starRating >= 2 ? "#ffd800" : "grey"}
                      />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => onRatingStars(3)}>
                      <AntDesign
                        name="star"
                        size={rf(25)}
                        color={starRating >= 3 ? "#ffd800" : "grey"}
                      />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => onRatingStars(4)}>
                      <AntDesign
                        name="star"
                        size={rf(25)}
                        color={starRating >= 4 ? "#ffd800" : "grey"}
                      />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => onRatingStars(5)}>
                      <AntDesign
                        name="star"
                        size={rf(25)}
                        color={starRating >= 5 ? "#ffd800" : "grey"}
                      />
                    </TouchableOpacity>
                  </View>
                  <View style={styles.ratingInput}>
                    <TextInput
                      style={{ width: "100%" }}
                      placeholder="Type here"
                      multiline
                      onChangeText={(val) => setFeedback(val)}
                    />
                  </View>
                  <TouchableOpacity
                    onPress={() => completeAJob(data)}
                    style={styles.feedbackBtn}
                  >
                    <LinearGradient
                      style={{
                        width: "100%",
                        height: "100%",
                        borderRadius: 13,
                        alignItems: "center",
                        justifyContent: "center",
                      }}
                      colors={[AppColors.linear1, AppColors.linear2]}
                    >
                      <Text style={styles.feedbackBtnTxt}>Submit</Text>
                    </LinearGradient>
                  </TouchableOpacity>
                </View>
              </View>
            </ScrollView>
          </KeyboardAvoidingView>
        </View>
      )}
      <View
        style={{
          flex: 1,
          width: "80%",
          alignSelf: "center",
          marginTop: Platform.OS == "android" ? "10%" : 0,
        }}
      >
        <View
          style={{
            width: "100%",
            flexDirection: "row",
            justifyContent: "space-between",
          }}
        >
          <TouchableOpacity
            onPress={() => {
              props.navigation.navigate("Home");
            }}
          >
            <ArrowLeft />
          </TouchableOpacity>
          <Text style={{ color: "#EF4136", fontFamily: "RBo", fontSize: 20 }}>
            Job Status
          </Text>
          <Text></Text>
        </View>

        {loading ? (
          <View style={{ flex: 1, justifyContent: "center" }}>
            <ActivityIndicator color="orange" size="large" />
          </View>
        ) : (
          <>
            <Image
              style={{
                alignSelf: "center",
                marginTop: 30,
                width: 100,
                height: 100,
                borderRadius: 50,
              }}
              source={{ uri: data?.authorProfile }}
            />
            <Text
              style={{
                alignSelf: "center",
                marginTop: 5,
                fontSize: 20,
                fontFamily: "RBo",
              }}
            >
              {data?.category}
            </Text>
            <ScrollView
              contentContainerStyle={{ flex: 1 }}
              // style={{backgroundColor:'red',}}
            >
              <View
                style={{
                  width: "100%",
                  height: 40,
                  backgroundColor: "white",
                  borderRadius: 13,
                  flexDirection: "row",
                  marginTop: 15,
                  alignItems: "center",
                  justifyContent: "space-between",
                  paddingHorizontal: 20,
                }}
              >
                <Text
                  style={{ fontSize: 14, fontFamily: "RRe", color: "#707070" }}
                >
                  Sub-Category
                </Text>
                <Text
                  style={{ fontSize: 14, fontFamily: "RRe", color: "#FBB040" }}
                >
                  {data?.subCategory}
                </Text>
              </View>

              <View
                style={{
                  width: "100%",
                  height: 40,
                  backgroundColor: "white",
                  borderRadius: 13,
                  flexDirection: "row",
                  marginTop: 15,
                  alignItems: "center",
                  justifyContent: "space-between",
                  paddingHorizontal: 20,
                }}
              >
                <Text
                  style={{ fontSize: 14, fontFamily: "RRe", color: "#707070" }}
                >
                  Status
                </Text>
                <Text
                  style={{ fontSize: 14, fontFamily: "RRe", color: "#FBB040" }}
                >
                  {data?.status == "onGoing"
                    ? "process"
                    : data?.status == "half-completed"
                    ? "Marked as complete by client"
                    : data?.status == "completed"
                    ? "Completed"
                    : "Pending"}
                </Text>
              </View>

              <Text
                style={{
                  fontSize: 14,
                  fontFamily: "RBo",
                  color: "#112D4E",
                  marginTop: 25,
                }}
              >
                Quote
              </Text>
              <View
                style={{
                  width: "100%",
                  paddingTop: 10,
                  paddingBottom: 60,
                  backgroundColor: "#FFFFFF",
                  marginTop: 10,
                  borderRadius: 13,
                  paddingHorizontal: 20,
                }}
              >
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "space-between",
                  }}
                >
                  <Text
                    style={{
                      color: "#707070",
                      fontSize: 14,
                      fontFamily: "RRe",
                    }}
                  >
                    Estimate Budget
                  </Text>
                  <Text
                    style={{
                      color: "#EF4136",
                      fontFamily: "RBo",
                      fontSize: 14,
                    }}
                  >
                    {data?.budget}
                  </Text>
                </View>
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "space-between",
                    marginTop: 15,
                  }}
                >
                  <Text
                    style={{
                      color: "#707070",
                      fontSize: 14,
                      fontFamily: "RRe",
                    }}
                  >
                    Time to Complete
                  </Text>
                  <Text
                    style={{
                      color: "#EF4136",
                      fontFamily: "RBo",
                      fontSize: 14,
                    }}
                  >
                    {data?.duration}
                  </Text>
                </View>
                <Text
                  style={{
                    color: "#707070",
                    fontSize: 14,
                    fontFamily: "RBo",
                    marginTop: 15,
                  }}
                >
                  Remarks
                </Text>
                <Text
                  style={{
                    color: "#707070",
                    fontSize: 10,
                    fontFamily: "RRe",
                    marginTop: 7,
                    lineHeight: 13,
                  }}
                >
                  {data?.desc}
                </Text>
              </View>

              {data?.hiredSomeone && (
                <>
                  <TouchableOpacity
                    style={{ width: "100%", height: 40, marginTop: 20 }}
                    onPress={() =>
                      props.navigation.navigate("Chat1", {
                        data: props?.get_quote_details?.data?.data,
                      })
                    }
                  >
                    <LinearGradient
                      style={{
                        flex: 1,
                        borderRadius: 13,
                        alignItems: "center",
                        justifyContent: "center",
                      }}
                      colors={[AppColors.linear1, AppColors.linear2]}
                    >
                      <Text
                        style={{
                          color: "white",
                          fontFamily: "RBo",
                          fontSize: 14,
                        }}
                      >
                        Chat now
                      </Text>
                    </LinearGradient>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={{ width: "100%", height: 40, marginTop: 20 }}
                    onPress={() =>
                      props.navigation.navigate("CheckLocation", {
                        data: props?.get_quote_details?.data?.data,
                      })
                    }
                  >
                    <LinearGradient
                      style={{
                        flex: 1,
                        borderRadius: 13,
                        alignItems: "center",
                        justifyContent: "center",
                      }}
                      colors={['#112D4E', '#112D4E']}
                    >
                      <Text
                        style={{
                          color: "white",
                          fontFamily: "RBo",
                          fontSize: 14,
                        }}
                      >
                        Check Location
                      </Text>
                    </LinearGradient>
                  </TouchableOpacity>
                </>
              )}

              {data?.status != "completed" && (
                <View
                  style={{
                    position: "absolute",
                    bottom: Platform.OS == "ios" ? 20 : 15,
                    flexDirection: "row",
                    justifyContent: "space-between",
                  }}
                >
                  <TouchableOpacity
                    style={{
                      width: "48%",
                      backgroundColor: "#112D4E",
                      height: 40,
                      borderRadius: 13,
                      justifyContent: "center",
                      alignItems: "center",
                    }}
                  >
                    <Text
                      style={{
                        fontSize: 14,
                        fontFamily: "RBo",
                        color: "#FFFFFF",
                      }}
                    >
                      Pause
                    </Text>
                  </TouchableOpacity>

                  <TouchableOpacity
                    onPress={() => {
                      // props.navigation.navigate("LiveLocation");
                      data?.status == "onGoing"
                        ? setRatingBox(!ratingBox)
                        : data?.status == "half-completed"
                        ? alert("Waiting for review")
                        : deleteJob();
                    }}
                    style={{ width: "48%", height: 40 }}
                  >
                    <LinearGradient
                      style={{
                        flex: 1,
                        borderRadius: 13,
                        justifyContent: "center",
                        alignItems: "center",
                      }}
                      colors={[AppColors.linear1, AppColors.linear2]}
                    >
                      <Text
                        style={{
                          fontSize: 14,
                          fontFamily: "RBo",
                          color: "#FFFFFF",
                        }}
                      >
                        {data?.status == "onGoing"
                          ? "Completed"
                          : data?.status == "half-completed"
                          ? "waiting for handyman"
                          : "Delete"}
                      </Text>
                    </LinearGradient>
                  </TouchableOpacity>
                </View>
              )}
            </ScrollView>
          </>
        )}
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  ratingBoxWrapper: {
    width: wp("100%"),
    height: hp("100%"),
    position: "absolute",
    zIndex: 9999999999999999999,
  },
  ratingBox: {
    width: "85%",
    zIndex: 99999999999999999999,
    minHeight: hp("25%"),
    backgroundColor: "#fff",
    left: wp("7.5%"),
    marginTop: hp("15%"),
    borderRadius: 10,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    alignItems: "center",
    elevation: 5,
    padding: 20,
  },
  starsWrapper: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    marginTop: hp("2%"),
  },
  ratingInput: {
    width: "100%",
    height: hp("13%"),
    backgroundColor: "#e5e5e5",
    borderRadius: 10,
    top: 20,
    padding: 10,
    marginBottom: 20,
  },
  ratingText: {
    fontSize: rf(14),
    fontWeight: "600",
    marginVertical: 10,
  },
  feedbackBtn: {
    width: "90%",
    height: hp("5%"),
    backgroundColor: "#FBB040",
    borderRadius: 10,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 15,
  },
  feedbackBtnTxt: {
    fontSize: rf(14),
    fontWeight: "600",
    color: "#fff",
  },
});

const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  get_user_details: state.main.get_user_details,
  view_job: state.main.view_job,
  get_quote_details: state.main.get_quote_details,
});
export default connect(mapStateToProps, {
  viewJob,
  getSingleQuoteDetails,
  getAnyUserData,
  completeJobByClient,
  deleteAJob,
})(JobStatus);

import { LinearGradient } from 'expo-linear-gradient'
import React from 'react'
import { SafeAreaView, View, Text, TouchableOpacity, StyleSheet, ScrollView, Image, Platform } from 'react-native'
import AppColors from '../../Components/AppColors'
import { ArrowLeft } from '../../Components/SvgComponets'



const BlockList = (props) => {

    const BlockListView = (props) => (
        <View style={styles.container}>
            <Image
                source={props.image}
            />
            <View style={{ marginLeft: 10, }}>
                <Text style={{ fontFamily: 'RBo', fontSize: 14, color: '#FBB040' }}>{props.name}</Text>
                <Text style={{ fontFamily: 'RRe', fontSize: 12, color: '#707070', marginTop: Platform.OS == "ios" ? 3 : 1 }}>Architect</Text>
            </View>
            <TouchableOpacity style={{ position: 'absolute', right: 10, height: 30, }}>
                <LinearGradient
                    style={{ flex: 1, height: "100%", borderRadius: 10, paddingHorizontal: 15, alignItems: 'center', justifyContent: 'center' }}
                    colors={[AppColors.linear1, AppColors.linear2]}
                >
                    <Text style={{ fontFamily: 'RBo', fontSize: 12, color: '#FFFFFF' }}>UNBLOCK</Text>


                </LinearGradient>
            </TouchableOpacity>

        </View>
    )
    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: '#F5F5F5', }}>

            <View style={{ flexDirection: 'row', justifyContent: 'space-between', width: "80%", alignSelf: 'center', marginTop: Platform.OS == "android" ? "10%" : 0, alignItems: 'center' }}>
                <TouchableOpacity
                    onPress={() => {
                        props.navigation.goBack();
                    }}
                >
                    <ArrowLeft />
                </TouchableOpacity>
                <Text style={{ fontSize: 20, fontFamily: "RBo", color: '#EF4136', alignSelf: 'center', }}>Block List</Text>
                <Text></Text>
            </View>

            <View style={{ width: "80%", alignSelf: 'center', marginTop: 35 }}>
                <ScrollView showsVerticalScrollIndicator={false} >
                    <BlockListView image={require("../../assets/icon3.png")} name="Meressa" />
                    <BlockListView image={require("../../assets/icon2.png")} name="Sara" />
                    <BlockListView image={require("../../assets/icon1.png")} name="Jade" />
                    <BlockListView image={require("../../assets/icon3.png")} name="Samuel" />
                </ScrollView>
            </View>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        width: "100%",
        height: 60,
        borderRadius: 20,
        backgroundColor: 'white',
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 10,
        marginTop: 10
    }
})
export default BlockList

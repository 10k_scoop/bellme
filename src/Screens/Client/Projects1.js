import React, { useState, useEffect } from "react";
import {
  SafeAreaView,
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  ScrollView,
  ActivityIndicator,
} from "react-native";
import { connect } from "react-redux";
import { ArrowLeft } from "../../Components/SvgComponets";
import { myJobs } from "../../state-management/actions/features/Features";
import * as firebase from "firebase";

const Projects1 = (props) => {
  const [loading, setLoading] = useState(true);
  const [data, setData] = useState([]);
  const user = firebase.auth().currentUser;

  useEffect(() => {
    props.myJobs(user?.email, setLoading);
  }, []);

  useEffect(() => {
    if (props.my_jobs != null) {
      setData(props.my_jobs);
    }
  }, [props]);

  if (loading) {
    return (
      <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
        <ActivityIndicator color="black" size="large" />
      </View>
    );
  }

  const Projects = (props) => (
    <TouchableOpacity
      style={styles.projectBox}
      onPress={() =>
        props.navigation.navigate("HomeStack", {
          screen: "JobStatus",
          params: { jobId: props?.data?.id, route: "noti" },
        })
      }
    >
      <View
        style={{
          backgroundColor: "#EF4136",
          width: 5,
          height: 46,
          borderRadius: 20,
        }}
      ></View>
      <View style={{ marginLeft: 15 }}>
        <View
          style={{
            flexDirection: "row",
            width: "100%",
          }}
        >
          <Text
            style={{
              width: "50%",
              color: "#EF4136",
              fontFamily: "RBo",
              fontSize: 14,
            }}
          >
            {props?.data?.data?.subCategory}
          </Text>
          <Text
            style={{
              width: "50%",
              color: "#112D4E",
              fontSize: 10,
              fontFamily: "RRe",
              textAlign: "right",
            }}
          >
            {props?.date}
          </Text>
        </View>
        <Text
          style={{
            fontFamily: "RRe",
            fontSize: 11,
            color: "#707070",
            lineHeight: 15,
            marginTop: 3,
          }}
        >
          {props?.data?.data?.desc}
        </Text>
      </View>
    </TouchableOpacity>
  );
  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: "#F5F5F5" }}>
      <View
        style={{
          flexDirection: "row",
          justifyContent: "space-between",
          width: "80%",
          alignSelf: "center",
          marginTop: Platform.OS == "android" ? "10%" : 0,
          alignItems: "center",
        }}
      >
        <TouchableOpacity
          onPress={() => {
            props.navigation.goBack();
          }}
        >
          <ArrowLeft />
        </TouchableOpacity>
        <Text
          style={{
            fontSize: 20,
            fontFamily: "RBo",
            color: "#EF4136",
            alignSelf: "center",
          }}
        >
          All Projects
        </Text>
        <Text></Text>
      </View>

      <View style={{ width: "80%", alignSelf: "center", marginTop: 35 }}>
        <ScrollView>
          {data?.map((item, index) => {
            let date = new Date(item?.data?.date?.seconds * 1000);
            return (
              <Projects
                key={index}
                data={item}
                date={
                  date.getDate() +
                  "-" +
                  date.getMonth() +
                  1 +
                  "-" +
                  date.getFullYear()
                }
                navigation={props?.navigation}
              />
            );
          })}
        </ScrollView>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  projectBox: {
    width: "100%",
    paddingVertical: 15,
    borderRadius: 20,
    backgroundColor: "white",
    flexDirection: "row",
    paddingHorizontal: 10,
    alignItems: "center",
    marginTop: 10,
  },
});

const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  my_jobs: state.main.my_jobs,
});
export default connect(mapStateToProps, { myJobs })(Projects1);

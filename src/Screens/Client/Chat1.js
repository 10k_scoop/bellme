import MaskedView from "@react-native-community/masked-view";
import { LinearGradient } from "expo-linear-gradient";
import React, { useState, useEffect } from "react";
import {
  SafeAreaView,
  View,
  TouchableOpacity,
  Text,
  Platform,
  Dimensions,
  TextInput,
  StyleSheet,
  Alert,
  ScrollView,
  ActivityIndicator,
  KeyboardAvoidingView,
} from "react-native";
import { connect } from "react-redux";
import AppColors from "../../Components/AppColors";
import { RecieveSms, SendSms } from "../../Components/SMS";

import {
  ArrowLeft,
  PhoneIconLarge,
  SENDSvg,
  SwipeRedRight,
} from "../../Components/SvgComponets";
import {
  sendMsg,
  getChat,
  makeChatRelation,
  renewChat,
} from "../../state-management/actions/features/Features";
import * as firebase from "firebase";
import base64 from "react-native-base64";

const Chat1 = (props) => {
  const [height, setHeight] = useState(0);
  let data = props.route.params?.data;
  const [loading, setLoading] = useState(true);
  const [text, setText] = useState("");
  const [chat, setChat] = useState([]);
  const user = firebase.auth().currentUser;

  useEffect(() => {
    (async () => {
      let id1 = base64.encode(
        data?.clientDetails?.email + data?.Hmandetails?.email
      );
      let id2 = base64.encode(
        data?.Hmandetails?.email + data?.clientDetails?.email
      );
      await props?.makeChatRelation([id1, id2], data, user, setLoading);
      props?.getChat([id1, id2], setLoading);
    })();
  }, []);

  useEffect(() => {
    if (props?.get_chat != "") {
      setChat(props?.get_chat);
    }
  }, [props]);

  const onSend = async () => {
    if (text.length == 0) {
      return;
    }
    let msgData = {
      deletedFor: [],
      sender: user?.email,
      reciever:
        user?.email == data?.clientDetails?.email
          ? data?.Hmandetails?.email
          : data?.clientDetails?.email,
      createdAt: new Date(),
      encodeId: base64.encode(
        data?.Hmandetails?.email + data?.clientDetails?.email
      ),
      text: text,
      senderName:
        user?.email == data?.clientDetails?.email
          ? data?.clientDetails?.name
          : data?.Hmandetails?.name,
      recieverName:
        user?.email == data?.clientDetails?.email
          ? data?.Hmandetails?.name
          : data?.clientDetails?.name,
    };
    let id1 = base64.encode(
      data?.clientDetails?.email + data?.Hmandetails?.email
    );
    let id2 = base64.encode(
      data?.Hmandetails?.email + data?.clientDetails?.email
    );
    await props?.sendMsg(msgData, setLoading);
    props?.renewChat([id1,id2], setLoading);
    setText("");
  };

  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: "#F5F5F5" }}>
      <View
        style={{
          width: "90%",

          alignItems: "center",
          flexDirection: "row",
          alignSelf: "center",
          justifyContent: "space-between",
          marginTop: Platform.OS == "android" ? "10%" : 10,
        }}
      >
        <TouchableOpacity onPress={() => props.navigation.goBack()}>
          <ArrowLeft />
        </TouchableOpacity>
        <Text style={{ color: "#EF4136", fontFamily: "RBo", fontSize: 20 }}>
          {data?.clientDetails?.name}
        </Text>
        <TouchableOpacity>
          <PhoneIconLarge />
        </TouchableOpacity>
      </View>
      <KeyboardAvoidingView
        behavior={Platform.OS === "ios" ? "padding" : "height"}
        style={{ flex: 1 }}
      >
        <View
          style={{
            width: "80%",
            alignSelf: "center",
            marginTop: 25,
            flex: 1,
          }}
        >
          <TouchableOpacity
            onPress={() =>
              props.navigation.navigate("HomeStack", {
                screen: "JobStatus",
                params: { data: data },
              })
            }
            style={{
              width: "100%",
              height: 40,
              alignItems: "center",
              flexDirection: "row",
              justifyContent: "space-between",
              borderRadius: 13,
              backgroundColor: "white",
              paddingHorizontal: 15,
            }}
          >
            <Text style={{ color: "#EF4136", fontSize: 14, fontFamily: "RBo" }}>
              Go to Current Job Status
            </Text>
            <SwipeRedRight />
          </TouchableOpacity>

          {loading ? (
            <View
              style={{
                flex: 1,
                position: "relative",
                top: 100,
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <ActivityIndicator color="black" size="large" />
            </View>
          ) : (
            <ScrollView showsVerticalScrollIndicator={false}>
              <View
                style={{
                  marginTop: 10,
                  height: "80%",
                  width: "100%",
                  paddingBottom: 200,
                }}
              >
                {chat?.length >= 1 &&
                  chat?.map((item, index) => {
                    if (item?.deletedFor?.includes(user.email)) {
                      return;
                    }
                    if (item?.sender == user.email) {
                      return <SendSms key={index} msg={item?.text} />;
                    } else {
                      return (
                        <RecieveSms
                          key={index}
                          style={{ marginTop: 20 }}
                          msg={item?.text}
                        />
                      );
                    }
                  })}
              </View>
            </ScrollView>
          )}
        </View>

        <View style={styles.sendBox}>
          <TextInput
            placeholder="Type message"
            placeholderTextColor="#707070"
            style={{
              color: "#707070",
              fontFamily: "RRe",
              fontSize: 14,
              width: "85%",
            }}
            value={text}
            onChangeText={(val) => setText(val)}
          />
          <TouchableOpacity onPress={onSend}>
            <SENDSvg />
          </TouchableOpacity>
        </View>
      </KeyboardAvoidingView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  sendBox: {
    flexDirection: "row",
    bottom: Platform.OS == "ios" ? 20 : 15,
    paddingHorizontal: 15,
    alignItems: "center",
    alignSelf: "center",
    width: "90%",
    height: 50,
    borderRadius: 40,
    backgroundColor: "#E4E4E4",
  },
  sendSmsBox: {
    // paddingHorizontal:10,
    // // height:30,
    // backgroundColor:'white',
    // borderTopLeftRadius: height/2 15,
    // borderBottomLeftRadius:15,
    // borderTopRightRadius:15,
    // borderBottomRightRadius:5,
    // alignSelf:'flex-end',
  },
});

const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  get_user_details: state.main.get_user_details,
  get_chat: state.main.get_chat,
});
export default connect(mapStateToProps, {
  sendMsg,
  getChat,
  renewChat,
  makeChatRelation,
})(Chat1);

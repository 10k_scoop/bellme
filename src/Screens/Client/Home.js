import MaskedView from "@react-native-community/masked-view";
import { LinearGradient } from "expo-linear-gradient";
import React, { useCallback, useState } from "react";
import {
  SafeAreaView,
  StyleSheet,
  Text,
  TextInput,
  View,
  TouchableOpacity,
  FlatList,
  TouchableWithoutFeedback,
  ScrollView,
  Dimensions,
} from "react-native";
import { widthPercentageToDP } from "react-native-responsive-screen";
import AppColors from "../../Components/AppColors";

import {
  ArchitectIcon,
  ArchitectTxt,
  CarpenterIcon,
  CarpenterTxt,
  CleaningIcon,
  LogoSvg,
  MechanicIcon,
  MechanicTxt,
  PlumbingIcon,
  SearchIcon,
  WasteIcon,
} from "../../Components/SvgComponets";

const Home = (props) => {
  const [showAllSC, setShowAllSC] = useState(false);
  var terminate;
  const [searchText, setSearchText] = useState("");
  const [filteredData, setfilteredData] = useState([]);
  const popularArray = [
    {
      icon: ArchitectIcon,
      name: "Plumbing",
      id:3
    },
    {
      icon: MechanicIcon,
      name: "Mechanic",
      id:6
    },
    {
      icon: CarpenterIcon,
      name: "Carpenter",
      id:1
    },
  ];
  const [categoriesArray, setcategoriesArray] = useState([
    {
      icon: CleaningIcon,
      name: "Construction",
    },
    {
      icon: PlumbingIcon,
      name: "Carpentry",
    },
    {
      icon: WasteIcon,
      name: "Waste-Mgt",
    },
    {
      icon: CleaningIcon,
      name: "Plumbing",
    },
    {
      icon: PlumbingIcon,
      name: "Tutor",
    },
    {
      icon: WasteIcon,
      name: "Services",
    },
    {
      icon: CleaningIcon,
      name: "Mechanics",
    },
    {
      icon: PlumbingIcon,
      name: "Electrical",
    },
    {
      icon: WasteIcon,
      name: "House-Maintenance",
    },
    {
      icon: WasteIcon,
      name: "Events",
    },
  ]);

  const onSearch = (val) => {
    let target = val.toLowerCase();
    setSearchText(target);
    let filteredData = categoriesArray.filter(function (item) {
      return item.name.toLowerCase().includes(val);
    });

    setfilteredData(filteredData);
  };

  const keyExtractor = useCallback((item, index) => index.toString(), []);
  const renderPopular = useCallback(({ item, index }) => {
    var Icon = item.icon;
    var Name = item.name;
    return (
      <TouchableOpacity
        style={{
          marginLeft: 20,
          backgroundColor: "#FFFFFF",
          width: 120,
          height: 154,
          borderRadius: 20,
          alignItems: "center",
        }}
        onPress={() => {
          props.navigation.navigate("HomeStack", {
            screen: "PostProject",
            params:{selectedCategory:item.name,selectedCategoryIndex:item.id}
          });
        }}
      >
        <LinearGradient
          style={{
            width: 100,
            height: 100,
            borderRadius: 10,
            marginTop: 10,
            justifyContent: "center",
            alignItems: "center",
          }}
          colors={[AppColors.linear1, AppColors.linear2]}
        >
          <Icon />
        </LinearGradient>
        <MaskedView
          style={{ height: 24, width: 80, marginTop: 10 }}
          maskElement={
            <Text
              style={{ fontSize: 11, fontFamily: "RBo", alignSelf: "center" }}
            >
              {item.name}
            </Text>
          }
        >
          <LinearGradient
            colors={[AppColors.linear1, AppColors.linear2]}
            start={{ x: 0, y: 1 }}
            end={{ x: 1, y: 0 }}
            style={{ flex: 1 }}
          />
        </MaskedView>
      </TouchableOpacity>
    );
  }, []);

  const renderCategories = useCallback(({ item, index }) => {
    var Icon = item.icon;

    return (
      <View style={{}}>
        <TouchableOpacity
          onPress={() => {
            props.navigation.navigate("HomeStack", {
              screen: "PostProject",
              params:{selectedCategory:item.name,selectedCategoryIndex:index}
            });
          }}
          style={{
            marginLeft: 20,
            backgroundColor: "#FFFFFF",
            width: 110,
            height: 120,
            borderRadius: 20,
            alignItems: "center",
          }}
        >
          <LinearGradient
            style={{
              width: 80,
              height: 80,
              borderRadius: 10,
              marginTop: 10,
              justifyContent: "center",
              alignItems: "center",
            }}
            colors={[AppColors.linear1, AppColors.linear2]}
          >
            <Icon />
          </LinearGradient>
          <MaskedView
            style={{ height: 24, width: 80, marginTop: 10 }}
            maskElement={
              <Text
                style={{ fontSize: 11, fontFamily: "RBo", alignSelf: "center" }}
              >
                {item.name}
              </Text>
            }
          >
            <LinearGradient
              colors={[AppColors.linear1, AppColors.linear2]}
              start={{ x: 0, y: 1 }}
              end={{ x: 1, y: 0 }}
              style={{ flex: 1 }}
            />
          </MaskedView>
        </TouchableOpacity>
      </View>
    );
  }, []);

  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: "#F5F5F5" }}>
      <View
        style={{
          flex: 1,
          paddingTop: "10%",
          width: widthPercentageToDP("100%"),
          paddingHorizontal: widthPercentageToDP("3%"),
        }}
      >
        <LogoSvg />
        <View
          style={{ flexDirection: "row", alignItems: "center", marginTop: 20 }}
        >
          <TextInput
            placeholder="Search a service"
            style={[styles.textInputContainer, { width: "80%" }]}
            onChangeText={(val) => onSearch(val)}
          />
          <TouchableOpacity
            style={{
              width: 40,
              height: 40,
              borderRadius: 13,
              backgroundColor: "#FFFFFF",
              marginLeft: 10,
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <SearchIcon />
          </TouchableOpacity>
        </View>

        {/* {!showAllSC ?   */}
        <Text
          style={{
            fontFamily: "RBo",
            fontSize: 14,
            color: "#8A8A8A",
            marginTop: 15,
            paddingHorizontal: 15,
          }}
        >
          Popular
        </Text>
        {/* :null} */}
        <View>
          <FlatList
            // data={ !showAllSC ? popularArray : [] }
            data={searchText == "" ? popularArray : ""}
            style={{ marginLeft: -18, marginTop: 15 }}
            showsHorizontalScrollIndicator={false}
            contentContainerStyle={{ paddingRight: 50 }}
            horizontal={true}
            keyExtractor={keyExtractor}
            renderItem={renderPopular}
          />
        </View>

        <View
          style={{
            flexDirection: "row",
            justifyContent: "space-between",
            paddingHorizontal: 15,
            marginTop: 20,
          }}
        >
          <TouchableOpacity
            onPress={() => {
              props.navigation.navigate("HomeStack", { screen: "PostProject" });
            }}
          >
            <Text style={{ fontFamily: "RBo", fontSize: 14, color: "#8A8A8A" }}>
              More
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              props.navigation.navigate("HomeStack", {
                screen: "SuggestAService",
              });
            }}
          >
            <Text style={{ fontFamily: "RBo", fontSize: 14, color: "#EF4136" }}>
              SUGGEST A SERVICE
            </Text>
          </TouchableOpacity>
        </View>

        <View
          style={{
            width: "100%",
            flex: 1,
            alignItems: "center",
          }}
        >
          <FlatList
            data={
              filteredData && filteredData.length > 0
                ? filteredData
                : categoriesArray.slice(0, 6)
            }
            showsVerticalScrollIndicator={false}
            style={{ marginLeft: -18, marginTop: 15, flex: 1 }}
            contentContainerStyle={{}}
            numColumns={3}
            columnWrapperStyle={{ marginTop: 20 }}
            keyExtractor={keyExtractor}
            renderItem={renderCategories}
            ListFooterComponentStyle={{
              flex: 1,
              height: 150,
              width: "90%",
              marginHorizontal: "7%",
            }}
            ListFooterComponent={() =>
              !showAllSC ? (
                <View
                  styles={{
                    flex: 1,
                    alignItems: "center",
                    backgroundColor: "red",
                  }}
                >
                  <TouchableOpacity
                    onPress={() => {
                      props.navigation.navigate("HomeStack", {
                        screen: "SubCategories",
                      });
                    }}
                    style={{
                      marginTop: 40,
                      width: "100%",
                      height: 40,
                      alignSelf: "center",
                    }}
                  >
                    <LinearGradient
                      style={{
                        flex: 1,
                        justifyContent: "center",
                        alignItems: "center",
                        borderRadius: 13,
                      }}
                      colors={[AppColors.linear1, AppColors.linear2]}
                    >
                      <Text
                        style={{
                          color: "#FFFFFF",
                          fontSize: 14,
                          fontFamily: "RBo",
                        }}
                      >
                        VIEW ALL SUB CATEGORIES
                      </Text>
                    </LinearGradient>
                  </TouchableOpacity>
                </View>
              ) : null
            }
          />
        </View>
      </View>
    </SafeAreaView>
  );
};
const styles = StyleSheet.create({
  textInputContainer: {
    flexDirection: "row",
    backgroundColor: "#E4E4E4",
    borderRadius: 13,
    height: 40,
    width: "100%",
    paddingLeft: 15,
    alignItems: "center",
    justifyContent: "center",
  },
});

export default Home;

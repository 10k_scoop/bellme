import MaskedView from "@react-native-community/masked-view";
import { LinearGradient } from "expo-linear-gradient";
import React, { useCallback, useState } from "react";
import {
  SafeAreaView,
  StyleSheet,
  Text,
  TextInput,
  View,
  Button,
  TouchableOpacity,
  FlatList,
  TouchableWithoutFeedback,
  ScrollView,
  Dimensions,
  Keyboard,
} from "react-native";
import AppColors from "../../Components/AppColors";
import GoalItem from "../../Components/Goaltem";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { Children } from "react";

const DismissKeyboard = ({ children }) => (
  <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
    {children}
  </TouchableWithoutFeedback>
);
export default function SuggestAService({ navigation }) {
  const [goal, setGoal] = useState();
  const [courseGoal, setCourseGoal] = useState([]);
  const changeGoal = (enteredText) => {
    setGoal(enteredText);
  };
  const addGoal = () => {
    setCourseGoal((prevState) => [...prevState, goal]);
  };
  return (
    <DismissKeyboard>
      <SafeAreaView style={{ flex: 1, backgroundColor: "#F5F5F5" }}>
        <View style={{ marginTop: hp("5%"), marginBottom: hp("2%") }}>
          <Text
            style={{
              fontSize: 20,
              fontFamily: "RBo",
              color: "orange",
              alignSelf: "center",
              marginTop: Platform.OS == "android" ? "10%" : 0,
            }}
          >
            Suggest A Service
          </Text>
        </View>
        <View>
          <TextInput
            style={styles.textField}
            placeholder="Enter your name"
            placeholderTextColor="#222"
          />
          <TextInput
            style={styles.textField}
            placeholder="Enter your email"
            placeholderTextColor="#222"
          />
        </View>

        <View style={{ paddingHorizontal: wp("5%") }}>
          <View style={styles.SuggestionBox}>
            <TextInput
              style={styles.TextInput}
              placeholder="Suggest a service"
              placeholderTextColor="black"
              multiline={true}
            />
          </View>
        </View>
        <View style={styles.TouchableOpacity}>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate("Home");
            }}
            style={{ width: "100%", height: 40, marginTop: 35 }}
          >
            <LinearGradient
              style={{
                flex: 1,
                borderRadius: 13,
                alignItems: "center",
                justifyContent: "center",
              }}
              colors={[AppColors.linear1, AppColors.linear2]}
            >
              <Text style={{ color: "white", fontFamily: "RBo", fontSize: 14 }}>
                S U B M I T
              </Text>
            </LinearGradient>
          </TouchableOpacity>
        </View>
      </SafeAreaView>
    </DismissKeyboard>
  );
}

const styles = StyleSheet.create({
  but1: {
    color: "#EF4136",
  },
  view1: {
    flexDirection: "row",
    padding: 30,
    justifyContent: "center",
    justifyContent: "space-between",
    alignItems: "center",
  },

  input: {
    borderWidth: 2,
    borderColor: "#EF4136",
    padding: 10,
    paddingRight: 100,
    width: wp("80%"),
  },
  footerButton: {
    width: wp("100%"),
    height: hp("10%"),
    justifyContent: "center",
  },
  footerTouch: {
    width: wp("80%"),
    height: wp("10%"),
    borderRadius: wp("2%"),
    backgroundColor: "#EF4136",
    justifyContent: "center",
    alignItems: "center",
  },
  btnText: {
    color: "white",
  },
  SuggestionText: {
    fontWeight: "600",
    fontSize: rf(13),
    color: "orange",
    paddingHorizontal: wp("5%"),
    marginVertical: hp("1%"),
  },
  SuggestionBox: {
    height: hp("30%"),
    width: wp("90%"),
    borderWidth: 1,
    borderColor: "orange",
    borderRadius: 10,
    padding: 10,
  },
  TextInput1: {
    fontSize: rf("16"),
    fontWeight: "600",
  },
  TouchableOpacity: {
    width: wp("100%"),
    justifyContent: "flex-end",
    paddingHorizontal: wp("4%"),
    flex: 1,
    marginBottom: hp("2%"),
  },

  textField: {
    width: wp("90%"),
    height: hp("5%"),
    borderRadius: 10,
    borderWidth: 1,
    borderColor: "orange",
    paddingHorizontal: 10,
    marginHorizontal: wp("5%"),
    marginVertical: 10,
  },
});

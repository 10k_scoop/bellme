import { LinearGradient } from "expo-linear-gradient";
import React, { useEffect, useState } from "react";
import {
  Dimensions,
  Image,
  SafeAreaView,
  ScrollView,
  Text,
  TouchableOpacity,
  ActivityIndicator,
  View,
} from "react-native";
import { connect } from "react-redux";
import AppColors from "../../Components/AppColors";
import { ArrowLeft, StarIcon } from "../../Components/SvgComponets";
import {
  getQuoteDetails,
  acceptJob,
} from "../../state-management/actions/features/Features";
import { MaterialIcons } from "@expo/vector-icons";

const QuoteRecieved = (props) => {
  let data = props.route.params?.data?.data;
  const [loading, setLoading] = useState(true);
  const [quote, setQuote] = useState([]);
  useEffect(() => {
    props.getQuoteDetails(data?.applicationId, setLoading);
  }, []);

  useEffect(() => {
    if (props.get_quote_details != null) {
      setQuote(props.get_quote_details);
    }
  }, [props]);

  const onAccept = async () => {
    try {
      setLoading(true);
      let dd = quote?.data?.data;
      dd.jobDetails.hiredSomeone = true;
      dd.jobDetails.status = "accepted";
      dd.Hmandetails.hired = true;
      props.acceptJob(dd, quote?.id, setLoading);
    } catch (e) {
      console.log(e.message);
    }
  };

  if (loading) {
    return (
      <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
        <ActivityIndicator color="black" size="large" />
      </View>
    );
  }
  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: "#F5F5F5" }}>
      <View
        style={{
          flex: 1,
          width: "80%",
          alignSelf: "center",
          marginTop: Platform.OS == "android" ? "10%" : 0,
        }}
      >
        <View
          style={{
            width: "100%",
            flexDirection: "row",
            justifyContent: "space-between",
          }}
        >
          <TouchableOpacity
            onPress={() => {
              props.navigation.goBack();
            }}
          >
            <ArrowLeft />
          </TouchableOpacity>
          <Text style={{ color: "#EF4136", fontFamily: "RBo", fontSize: 20 }}>
            Quote Received
          </Text>
          <Text></Text>
        </View>

        {quote?.data?.data?.Hmandetails?.businessNameImg?.length < 1 ||
        quote?.data?.data?.Hmandetails?.addressImg?.length < 1 ? (
          <Image
            style={{
              alignSelf: "center",
              marginTop: 30,
              width: 100,
              height: 100,
              borderRadius: 50,
            }}
            source={require("../../assets/profileIcon2.png")}
          />
        ) : (
          <Image
            style={{
              alignSelf: "center",
              marginTop: 30,
              width: 100,
              height: 100,
              borderRadius: 50,
            }}
            source={{
              uri:
                quote?.data?.data?.Hmandetails?.businessNameImg ||
                quote?.data?.data?.Hmandetails?.addressImg,
            }}
          />
        )}
        <View
          style={{
            alignSelf: "center",
            paddingHorizontal: 15,
            paddingVertical: 5,
            backgroundColor: "transparent",
            borderRadius: 6,
            marginTop: 2,
          }}
        >
          {quote?.data?.data?.Hmandetails?.verified && (
            <MaterialIcons name="verified-user" size={23} color="#FBB040" />
          )}
        </View>
        <Text
          style={{
            alignSelf: "center",
            marginTop: 5,
            fontSize: 20,
            fontFamily: "RBo",
          }}
        >
          {quote?.data?.data?.Hmandetails?.name}
        </Text>
        <View
          style={{ flexDirection: "row", marginTop: 5, alignSelf: "center" }}
        >
          {quote?.data?.data?.Hmandetails?.rating?.length < 1 && (
            <>
              <StarIcon
                style={{ marginLeft: 3 }}
                active={false}
              />
              <StarIcon
                style={{ marginLeft: 3 }}
                active={false}
              />
              <StarIcon
                style={{ marginLeft: 3 }}
                active={false}
              />
              <StarIcon
                style={{ marginLeft: 3 }}
                active={false}
              />
              <StarIcon
                style={{ marginLeft: 3 }}
                active={false}
              />
            </>
          )}
          {quote?.data?.data?.Hmandetails?.rating?.map((item, index) => {
            if (item?.stars == 5) {
              arr[0] = arr[0] + 1;
            } else if (item?.stars == 4) {
              arr[1] = arr[1] + 1;
            } else if (item?.stars == 3) {
              arr[2] = arr[2] + 1;
            } else if (item?.stars == 2) {
              arr[3] = arr[3] + 1;
            } else if (item?.stars == 1) {
              arr[4] = arr[4] + 1;
            } else {
            }
            let cal =
              (5 * arr[0] + 4 * arr[1] + 3 * arr[2] + 2 * arr[3] + 1 * arr[4]) /
              (arr[0] + arr[1] + arr[2] + arr[3] + arr[4]);
            // console.log(cal)
            if (index > 0) {
              return (
                <View key={index} style={{ flexDirection: "row" }}>
                  <StarIcon active={cal >= 1 ? true : false} />
                  <StarIcon
                    style={{ marginLeft: 3 }}
                    active={cal >= 2 ? true : false}
                  />
                  <StarIcon
                    style={{ marginLeft: 3 }}
                    active={cal >= 3 ? true : false}
                  />
                  <StarIcon
                    style={{ marginLeft: 3 }}
                    active={cal >= 4 ? true : false}
                  />
                  <StarIcon
                    style={{ marginLeft: 3 }}
                    active={cal >= 5 ? true : false}
                  />
                </View>
              );
            }
          })}
        </View>
        <ScrollView
          contentContainerStyle={{ flex: 1 }}
          // style={{backgroundColor:'red',}}
        >
          <View
            style={{
              width: "100%",
              height: 40,
              backgroundColor: "white",
              borderRadius: 13,
              flexDirection: "row",
              marginTop: 15,
              alignItems: "center",
              justifyContent: "space-between",
              paddingHorizontal: 20,
            }}
          >
            <Text style={{ fontSize: 14, fontFamily: "RRe", color: "#707070" }}>
              Company
            </Text>
            <Text style={{ fontSize: 14, fontFamily: "RRe", color: "#FBB040" }}>
              {quote?.data?.data?.Hmandetails?.name}
            </Text>
          </View>

          <TouchableOpacity
            onPress={() => {
              props.navigation.navigate("SPProfile");
            }}
            style={{ width: "100%", height: 40, marginTop: 20 }}
          >
            <LinearGradient
              style={{
                flex: 1,
                borderRadius: 13,
                alignItems: "center",
                justifyContent: "center",
              }}
              colors={[AppColors.linear1, AppColors.linear2]}
            >
              <Text style={{ color: "white", fontFamily: "RBo", fontSize: 14 }}>
                View Full Profile
              </Text>
            </LinearGradient>
          </TouchableOpacity>

          <Text
            style={{
              fontSize: 14,
              fontFamily: "RBo",
              color: "#112D4E",
              marginTop: 25,
            }}
          >
            Quote
          </Text>
          <View
            style={{
              width: "100%",
              paddingTop: 10,
              paddingBottom: 60,
              backgroundColor: "#FFFFFF",
              marginTop: 10,
              borderRadius: 13,
              paddingHorizontal: 20,
            }}
          >
            <View
              style={{
                flexDirection: "row",
                marginBottom: 10,
                justifyContent: "space-between",
              }}
            >
              <Text
                style={{ color: "#707070", fontSize: 14, fontFamily: "RRe" }}
              >
                Job Title
              </Text>
              <Text
                style={{ color: "#EF4136", fontFamily: "RBo", fontSize: 14 }}
              >
                {quote?.data?.data?.jobDetails?.subCategory}
              </Text>
            </View>
            <View
              style={{ flexDirection: "row", justifyContent: "space-between" }}
            >
              <Text
                style={{ color: "#707070", fontSize: 14, fontFamily: "RRe" }}
              >
                Estimate Cost
              </Text>
              <Text
                style={{ color: "#EF4136", fontFamily: "RBo", fontSize: 14 }}
              >
                ${" "}
                {quote?.data?.data?.jobDetails?.hourlyRate ||
                  quote?.data?.data?.jobDetails?.quoteByHman}
              </Text>
            </View>

            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-between",
                marginTop: 15,
              }}
            >
              <Text
                style={{ color: "#707070", fontSize: 14, fontFamily: "RRe" }}
              >
                Time to Complete
              </Text>
              <Text
                style={{ color: "#EF4136", fontFamily: "RBo", fontSize: 14 }}
              >
                {quote?.data?.data?.jobDetails?.durationByHman > 1
                  ? quote?.data?.data?.jobDetails?.durationByHman + " days"
                  : quote?.data?.data?.jobDetails?.durationByHman + " day"}
              </Text>
            </View>
            <Text
              style={{
                color: "#707070",
                fontSize: 14,
                fontFamily: "RBo",
                marginTop: 15,
              }}
            >
              Remarks
            </Text>
            <Text
              style={{
                color: "#707070",
                fontSize: 10,
                fontFamily: "RRe",
                marginTop: 7,
                lineHeight: 13,
              }}
            >
              Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
              nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam
              erat, sed diam voluptua. At vero eos et
            </Text>
          </View>
          {quote?.data?.data?.jobDetails?.hiredSomeone && (
            <TouchableOpacity
              style={{ width: "100%", height: 40, marginTop: 20 }}
              onPress={() =>
                props.navigation.navigate("Chat1", { data: quote?.data?.data })
              }
            >
              <LinearGradient
                style={{
                  flex: 1,
                  borderRadius: 13,
                  alignItems: "center",
                  justifyContent: "center",
                }}
                colors={[AppColors.linear1, AppColors.linear2]}
              >
                <Text
                  style={{
                    color: "white",
                    fontFamily: "RBo",
                    fontSize: 14,
                  }}
                >
                  Chat now
                </Text>
              </LinearGradient>
            </TouchableOpacity>
          )}
          <View
            style={{
              position: "absolute",
              bottom: 10,
              flexDirection: "row",
              justifyContent: "space-between",
            }}
          >
            {quote?.data?.data?.jobDetails?.hiredSomeone == false ? (
              <>
                <TouchableOpacity
                  style={{
                    width: "48%",
                    backgroundColor: "#112D4E",
                    height: 40,
                    borderRadius: 13,
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                >
                  <Text
                    style={{
                      fontSize: 14,
                      fontFamily: "RBo",
                      color: "#FFFFFF",
                    }}
                  >
                    R E J E C T
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => {
                    // props.navigation.navigate("LiveLocation");
                    onAccept();
                  }}
                  style={{ width: "48%", height: 40 }}
                >
                  <LinearGradient
                    style={{
                      flex: 1,
                      borderRadius: 13,
                      justifyContent: "center",
                      alignItems: "center",
                    }}
                    colors={[AppColors.linear1, AppColors.linear2]}
                  >
                    <Text
                      style={{
                        fontSize: 14,
                        fontFamily: "RBo",
                        color: "#FFFFFF",
                      }}
                    >
                      A C C E P T
                    </Text>
                  </LinearGradient>
                </TouchableOpacity>
              </>
            ) : (
              <TouchableOpacity
                style={{ width: "90%", height: 40, marginHorizontal: "5%" }}
              >
                <LinearGradient
                  style={{
                    flex: 1,
                    borderRadius: 13,
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                  colors={[AppColors.linear1, AppColors.linear2]}
                >
                  <Text
                    style={{
                      fontSize: 14,
                      fontFamily: "RBo",
                      color: "#FFFFFF",
                    }}
                  >
                    You already hired someone for this job
                  </Text>
                </LinearGradient>
              </TouchableOpacity>
            )}
          </View>
        </ScrollView>
      </View>
    </SafeAreaView>
  );
};

const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  get_quote_details: state.main.get_quote_details,
});
export default connect(mapStateToProps, { getQuoteDetails, acceptJob })(
  QuoteRecieved
);

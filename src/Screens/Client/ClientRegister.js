import { LinearGradient } from "expo-linear-gradient";
import { StatusBar } from "expo-status-bar";
import React, { useState } from "react";
import {
  SafeAreaView,
  View,
  Text,
  TextInput,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
  Dimensions,
} from "react-native";
import AppColors from "../../Components/AppColors";
import {
  ArrowLeft,
  LoginSvg,
  LogoSvg,
  MsgIcon,
  ORSvg,
  PasswordIcon,
  PersonIcon,
  RegisterSvg,
} from "../../Components/SvgComponets";

const ClientRegister = (props) => {
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [pass, setPass] = useState("");
  const [confirmPass, setConfirmPass] = useState("");

  const onNext = () => {
    if (name != "" && email != "" && pass != "") {
      let data = {
        name: name,
        email: email,
        pass: pass,
      };
      if (pass == confirmPass) {
        props.navigation.navigate("ClientRegister1", { data: data });
      } else {
        alert("Password doesnot match");
      }
    } else {
      alert("Fill all details");
    }
  };

  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: "#F5F5F5" }}>
      <StatusBar hidden={true} />
      <View
        style={{
          marginTop: Platform.OS == "android" ? "15%" : "10%",
          width: "90%",
          alignItems: "center",
        }}
      >
        <View style={{ flexDirection: "row" }}>
          <TouchableOpacity
            onPress={() => {
              props.navigation.goBack();
            }}
            style={{ marginRight: 10 }}
          >
            <ArrowLeft />
          </TouchableOpacity>
          <LogoSvg />
        </View>
      </View>
      <Text
        style={{
          fontFamily: "RRe",
          fontSize: 14,
          color: "#707070",
          marginTop: 20,
          alignSelf: "center",
        }}
      >
        Register for your CLIENT account
      </Text>
      <ScrollView contentContainerStyle={{ paddingBottom: 80 }}>
        <View
          style={{
            width: "85%",
            alignSelf: "center",
            alignItems: "center",
            marginTop: 20,
          }}
        >
          <View style={styles.textInputContainer}>
            <View style={{ position: "absolute", left: 15 }}>
              <PersonIcon />
            </View>
            <TextInput
              placeholder="Name"
              placeholderTextColor="#707070"
              style={styles.textInput}
              onChangeText={(val) => setName(val)}
              keyboardType="default"
            />
          </View>
          <View style={styles.textInputContainer}>
            <View style={{ position: "absolute", left: 15 }}>
              <MsgIcon />
            </View>
            <TextInput
              placeholder="Email Address"
              placeholderTextColor="#707070"
              style={styles.textInput}
              onChangeText={(val) => setEmail(val)}
              keyboardType="email-address"
            />
          </View>
          <View style={styles.textInputContainer}>
            <View style={{ position: "absolute", left: 15 }}>
              <PasswordIcon />
            </View>
            <TextInput
              placeholder="Password"
              placeholderTextColor="#707070"
              style={styles.textInput}
              onChangeText={(val) => setPass(val)}
              secureTextEntry={true}
            />
          </View>
          <View style={styles.textInputContainer}>
            <View style={{ position: "absolute", left: 15 }}>
              <PasswordIcon />
            </View>
            <TextInput
              placeholder="Re - Enter Password"
              placeholderTextColor="#707070"
              style={styles.textInput}
              onChangeText={(val) => setConfirmPass(val)}
              secureTextEntry={true}
            />
          </View>

          <TouchableOpacity
            onPress={onNext}
            style={{ marginTop: 40, width: "100%", height: 40 }}
          >
            <LinearGradient
              style={{
                flex: 1,
                justifyContent: "center",
                alignItems: "center",
                borderRadius: 13,
              }}
              colors={[AppColors.linear1, AppColors.linear2]}
            >
              <Text
                style={{ color: "#FFFFFF", fontSize: 14, fontFamily: "RBo" }}
              >
                R E G I S T E R
              </Text>
            </LinearGradient>
          </TouchableOpacity>
          <View style={{ alignItems: "center", marginTop: 60 }}>
            <Text
              style={{
                fontFamily: "RRe",
                fontSize: 14,
                color: "#707070",
                marginTop: 30,
              }}
            >
              Already have an Account?
            </Text>
            <TouchableOpacity
              onPress={() => {
                props.navigation.navigate("ClientLogin");
              }}
              style={{ marginTop: 15 }}
            >
              <LoginSvg />
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  textInputContainer: {
    flexDirection: "row",
    backgroundColor: "#E4E4E4",
    borderRadius: 13,
    height: 40,
    width: "100%",
    paddingLeft: 15,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 25,
  },
  textInput: {
    fontFamily: "RRe",
    fontSize: 14,
    color: "#707070",
    width: "80%",
    textAlign: "center",
  },
});

export default ClientRegister;

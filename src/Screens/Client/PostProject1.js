import { LinearGradient } from 'expo-linear-gradient';
import React from 'react'
import { SafeAreaView, TouchableOpacity, View, Text } from 'react-native';
import AppColors from '../../Components/AppColors';
import { ArrowLeft, ThreeDotsVert, VisaIcon } from '../../Components/SvgComponets';

const PostProject1 = (props) => {
    return (

        <SafeAreaView style={{ flex: 1, backgroundColor: '#F5F5F5', }}>

            <View style={{ height: "100%", width: "80%", alignSelf: 'center', marginTop: Platform.OS == "android" ? "10%" : 0 }}>
                <View style={{ width: "100%", flexDirection: 'row', justifyContent: 'space-between' }}>
                    <TouchableOpacity
                        onPress={() => {
                            props.navigation.goBack();
                        }}
                    >
                        <ArrowLeft />
                    </TouchableOpacity>
                    <Text style={{ color: '#EF4136', fontFamily: 'RBo', fontSize: 20 }}>Payment</Text>
                    <Text></Text>
                </View>
                <TouchableOpacity style={{ width: '100%', alignSelf: 'center', height: 60, flexDirection: 'row', alignItems: 'center', backgroundColor: 'white', marginTop: 30, borderRadius: 20, paddingHorizontal: 10 }}>
                    <VisaIcon />
                    <View style={{ marginLeft: 15, width: "77%" }}>
                        <Text style={{ color: '#EF4136', fontSize: 12, fontFamily: 'RBo' }}>Sara Jack</Text>
                        <Text style={{ color: "#112D4E", fontSize: 12, fontFamily: 'RBo' }}>----    ----    ----    1234</Text>
                    </View>
                    <ThreeDotsVert />
                </TouchableOpacity>
                <TouchableOpacity style={{ width: '100%', alignSelf: 'center', height: 60, flexDirection: 'row', alignItems: 'center', backgroundColor: 'white', marginTop: 15, borderRadius: 20, paddingHorizontal: 10 }}>
                    <VisaIcon />
                    <View style={{ marginLeft: 15, width: "77%" }}>
                        <Text style={{ color: '#EF4136', fontSize: 12, fontFamily: 'RBo' }}>Sara Jack</Text>
                        <Text style={{ color: "#112D4E", fontSize: 12, fontFamily: 'RBo' }}>----    ----    ----    1234</Text>
                    </View>
                    <ThreeDotsVert />
                </TouchableOpacity>

            </View>


            <TouchableOpacity
                onPress={() => {
                    props.navigation.navigate('PostProject2')
                }}
                style={{ position: 'absolute', bottom: 30, width: "80%", alignSelf: 'center', height: 40 }}>
                <LinearGradient
                    style={{ flex: 1, borderRadius: 13, alignItems: 'center', justifyContent: 'center', }}
                    colors={[AppColors.linear1, AppColors.linear2]}
                >
                    <Text style={{ color: 'white', fontFamily: 'RBo', fontSize: 14 }}>A D D   N E W   C A R D</Text>
                </LinearGradient>

            </TouchableOpacity>
        </SafeAreaView>
    )
}

export default PostProject1

import MaskedView from '@react-native-community/masked-view';
import { LinearGradient } from 'expo-linear-gradient';
import React from 'react'
import { Dimensions, Image, SafeAreaView, ScrollView, Text, TextInput, TouchableOpacity, View } from 'react-native';
import AppColors from '../../Components/AppColors';
import { ArrowLeft, DollarIcon, StarBlack, StarIcon } from '../../Components/SvgComponets';

const Feedback = (props) => {
    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: '#F5F5F5', }}>

            <View style={{ flex: 1, width: "80%", alignSelf: 'center', marginTop: Platform.OS == "android" ? "10%" : 0 }}>
                <View style={{ width: "100%", flexDirection: 'row', justifyContent: 'space-between' }}>
                    <TouchableOpacity
                        onPress={() => {
                            props.navigation.goBack();
                        }}
                    >
                        <ArrowLeft />
                    </TouchableOpacity>
                    <Text style={{ color: '#EF4136', fontFamily: 'RBo', fontSize: 20 }}>Work In Progress</Text>
                    <Text></Text>
                </View>

                <Image
                    style={{ alignSelf: 'center', marginTop: 30, width: 100, height: 100, borderRadius: 50, }}
                    source={require('../../assets/profileIcon2.png')}
                />
                <View style={{ alignSelf: 'center', paddingHorizontal: 15, paddingVertical: 5, backgroundColor: '#FBB040', borderRadius: 6, marginTop: 2 }}>
                    <Text style={{ color: "#FFFFFF", fontFamily: 'RRe', fontSize: 11 }}>CERTIFIED</Text>
                </View>
                <Text style={{ alignSelf: 'center', marginTop: 5, fontSize: 20, fontFamily: 'RBo' }}>Selena Adems</Text>
                <View style={{ flexDirection: 'row', marginTop: 5, alignSelf: 'center' }}>
                    <StarIcon />
                    <StarIcon />
                    <StarIcon />
                    <StarIcon />
                    <StarIcon />
                </View>
                <ScrollView
                    contentContainerStyle={{ flex: 1 }}
                // style={{backgroundColor:'red',}}
                >
                    <View style={{ width: "100%", height: 40, backgroundColor: 'white', borderRadius: 13, flexDirection: 'row', marginTop: 15, alignItems: 'center', justifyContent: 'space-between', paddingHorizontal: 20 }}>
                        <Text style={{ fontSize: 14, fontFamily: 'RRe', color: "#707070" }}>Company</Text>
                        <Text style={{ fontSize: 14, fontFamily: 'RRe', color: '#FBB040' }}>ABC.org</Text>
                    </View>



                    <MaskedView
                        style={{width:"100%",height:30, marginTop:25 }}
                        maskElement={<Text style={{fontSize:20,fontFamily:'RBo',alignSelf:'center'}}>Rate Your Experience</Text>}
                    >
                        <LinearGradient
                            colors={[AppColors.linear1, AppColors.linear2]}
                            start={{ x: 0, y:1  }}
                            end={{ x: 1, y:1 }}
                            style={{ flex: 1 }}
                        />
                    </MaskedView>
                   <View style={{flexDirection:'row',justifyContent:'space-between',marginTop:20}}>
                        <Text style={{fontFamily:'RRe',fontSize:14,color:"#707070"}}>Service</Text>
                        <View style={{flexDirection:'row'}}>
                            <StarBlack/>
                            <StarBlack style={{marginLeft:5}} />
                            <StarBlack style={{marginLeft:5}} />
                            <StarBlack style={{marginLeft:5}} />
                            <StarBlack style={{marginLeft:5}} />
                        </View>
                   </View>
                   <View style={{flexDirection:'row',justifyContent:'space-between',marginTop:20}}>
                        <Text style={{fontFamily:'RRe',fontSize:14,color:"#707070"}}>Behavior</Text>
                        <View style={{flexDirection:'row'}}>
                            <StarBlack/>
                            <StarBlack style={{marginLeft:5}} />
                            <StarBlack style={{marginLeft:5}} />
                            <StarBlack style={{marginLeft:5}} />
                            <StarBlack style={{marginLeft:5}} />
                        </View>
                   </View>
                   <View style={{flexDirection:'row',justifyContent:'space-between',marginTop:20}}>
                        <Text style={{fontFamily:'RRe',fontSize:14,color:"#707070"}}>Communication</Text>
                        <View style={{flexDirection:'row'}}>
                            <StarBlack/>
                            <StarBlack style={{marginLeft:5}} />
                            <StarBlack style={{marginLeft:5}} />
                            <StarBlack style={{marginLeft:5}} />
                            <StarBlack style={{marginLeft:5}} />
                        </View>
                   </View>
                    
                    <TextInput 
                        style={{width:"100%",height:100,paddingHorizontal:10, backgroundColor:'#E4E4E4',borderRadius:10,marginTop:20,color:'#707070',fontFamily:'RRe',fontSize:14}}
                        placeholder="Write a review"
                        placeholderTextColor="#707070"
                        textAlignVertical="top"
                        multiline={true}
                    />         
                        <TouchableOpacity
                            onPress={() => {
                                props.navigation.navigate('ClientBottomTabNavigator', {screen : "Home"})
                            }}
                            style={{position: 'absolute', bottom: 10,  width: "100%", height: 40, marginTop: 20 }}>
                            <LinearGradient
                                style={{ flex: 1, borderRadius: 13, alignItems: 'center', justifyContent: 'center', }}
                                colors={[AppColors.linear1, AppColors.linear2]}
                            >
                                <Text style={{ color: 'white', fontFamily: 'RBo', fontSize: 14 }}>S U B M I T   F E E D B A C K</Text>
                            </LinearGradient>
                        </TouchableOpacity>

                    
                </ScrollView>


            </View>
        </SafeAreaView>
    )
}

export default Feedback

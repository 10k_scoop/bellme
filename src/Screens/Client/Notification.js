import React, { useEffect, useState } from "react";
import {
  SafeAreaView,
  Text,
  View,
  TouchableOpacity,
  StyleSheet,
  ActivityIndicator,
} from "react-native";
import { connect } from "react-redux";
import { NotficationIcon } from "../../Components/SvgComponets";
import {
  myJobs,
  getAllNotifications,
} from "../../state-management/actions/features/Features";
import * as firebase from "firebase";
import { Entypo } from "@expo/vector-icons";
import { RFValue } from "react-native-responsive-fontsize";

const Notification = (props) => {
  const [notificationState, setNotificationState] = useState(true);
  const [projectState, setProjectState] = useState(false);
  const user = firebase.auth().currentUser;
  const [loading, setLoading] = useState(true);
  const [projects, setProjects] = useState([]);
  const [notifications, setNotifications] = useState([]);

  useEffect(() => {
    props.myJobs(user.email, setLoading);
    props.getAllNotifications(setLoading);
  }, []);

  useEffect(() => {
    if (props.get_all_notifications != null) {
      let arr = [];
      props?.get_all_notifications?.map((item, index) => {
        if (item?.data?.data?.to == user.email) {
          arr.push(item);
        }
        setNotifications(arr);
      });
    }
  }, [props]);

  console.log(projects);

  useEffect(() => {
    if (props.my_jobs != null) {
      setProjects(props.my_jobs);
    }
  }, [props]);

  if (loading) {
    return (
      <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
        <ActivityIndicator color="black" size="large" />
      </View>
    );
  }

  const viseVerse = () => {
    setNotificationState(false);
    setProjectState(true);
  };

  const NotificationView = (props) => (
    <TouchableOpacity
      style={styles.notificationBox}
      onPress={() => {
        props?.data?.data?.data?.type == "applied"
          ? props.navigation.navigate("HomeStack", {
              screen: "QuoteRecieved",
              params: { data: props?.data },
            })
          : viseVerse();
      }}
    >
      <View
        style={{
          backgroundColor: "#FBB040",
          width: 5,
          height: 20,
          borderRadius: 20,
        }}
      ></View>
      <View style={{ marginLeft: 10 }}>
        <Text style={{ color: "#FBB040", fontFamily: "RBo", fontSize: 14 }}>
          {props?.data?.data?.data?.type == "applied"
            ? "Congrats! New Quote"
            : props?.data?.data?.data?.type == "completed"
            ? "Congrats! "
            : "Alert"}
        </Text>
        <Text style={{ fontFamily: "RRe", fontSize: 11 }}>
          {props?.data?.data?.data?.message}
        </Text>
      </View>
    </TouchableOpacity>
  );
  const OnGoing = (props) => (
    <TouchableOpacity
      style={styles.notificationBox}
      onPress={() =>
        props.navigation.navigate("HomeStack", {
          screen: "JobStatus",
          params: { jobId: props?.id, route: "noti" },
        })
      }
    >
      <View
        style={{
          backgroundColor: "#EF4136",
          width: 5,
          height: 20,
          borderRadius: 20,
        }}
      ></View>
      <View style={{ marginLeft: 10 }}>
        <Text style={{ color: "#EF4136", fontFamily: "RBo", fontSize: 14 }}>
          {props?.data?.subCategory}
        </Text>
        <Text style={{ fontFamily: "RRe", fontSize: 11 }}>
          <Text style={{ color: "#EF4136" }}></Text>{" "}
          {props?.data?.status == "half-completed"
            ? "Waiting for review"
            : "handyman is on his way to your location"}
        </Text>
      </View>
    </TouchableOpacity>
  );
  const Completed = (props) => (
    <TouchableOpacity style={styles.notificationBox}>
      <View
        style={{
          backgroundColor: "#00A410",
          width: 5,
          height: 20,
          borderRadius: 20,
        }}
      ></View>
      <View style={{ marginLeft: 10, width: "75%" }}>
        <Text style={{ color: "#00A410", fontFamily: "RBo", fontSize: 14 }}>
          Order Feedback - Plumbing
        </Text>
        <Text style={{ fontFamily: "RRe", fontSize: 11 }}>
          {props?.data?.status == "completed"
            ? " Job has been completed"
            : " Your feedback has been submitted"}
        </Text>
      </View>
      <Text
        style={{
          color: "#112D4E",
          fontSize: 10,
          fontFamily: "RRe",
          position: "absolute",
          right: 15,
        }}
      >
        {props?.date?.getDate() +
          "-" +
          eval(props?.date?.getMonth() + 1) +
          "-" +
          props?.date?.getFullYear()}
      </Text>
    </TouchableOpacity>
  );

  const New = (props) => (
    <TouchableOpacity
      style={styles.notificationBox}
      onPress={() =>
        props.navigation.navigate("HomeStack", {
          screen: "JobStatus",
          params: { jobId: props?.id, route: "noti" },
        })
      }
    >
      <View
        style={{
          backgroundColor: "#00A410",
          width: 5,
          height: 20,
          borderRadius: 20,
        }}
      ></View>
      <View style={{ marginLeft: 10, width: "75%" }}>
        <Text style={{ color: "#00A410", fontFamily: "RBo", fontSize: 14 }}>
          Order Details - {props?.data.category}
        </Text>
        <Text
          numberOfLines={1}
          style={{ fontFamily: "RRe", fontSize: 11, width: "85%" }}
        >
          {props?.data?.desc}
        </Text>
      </View>
      <Text
        style={{
          color: "#112D4E",
          fontSize: 10,
          fontFamily: "RRe",
          position: "absolute",
          right: 15,
        }}
      >
        {props?.date?.getDate() +
          "-" +
          eval(props?.date?.getMonth() + 1) +
          "-" +
          props?.date?.getFullYear()}
      </Text>
    </TouchableOpacity>
  );

  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: "#F5F5F5" }}>
      <Text
        style={{
          fontSize: 20,
          fontFamily: "RBo",
          color: "#EF4136",
          alignSelf: "center",
          marginTop: Platform.OS == "android" ? "10%" : 0,
        }}
      >
        Notifications
      </Text>
      <View style={{ marginTop: 30, width: "80%", alignSelf: "center" }}>
        <View
          style={{
            width: "100%",
            paddingHorizontal: 3,
            height: 40,
            flexDirection: "row",
            backgroundColor: "#E4E4E4",
            borderRadius: 13,
          }}
        >
          <TouchableOpacity
            onPress={() => {
              setNotificationState(true);
              setProjectState(false);
            }}
            style={notificationState ? styles.activeTab : styles.inActiveTab}
          >
            <Text
              style={
                notificationState ? styles.activeText : styles.inActiveText
              }
            >
              Notifications
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              setProjectState(true);
              setNotificationState(false);
            }}
            style={projectState ? styles.activeTab : styles.inActiveTab}
          >
            <Text
              style={projectState ? styles.activeText : styles.inActiveText}
            >
              Projects
            </Text>
          </TouchableOpacity>
        </View>

        {notificationState ? (
          <View style={{ marginTop: 20 }}>
            {notifications?.length == 0 && (
              <View style={styles.errorMsg}>
                <Entypo name="emoji-sad" size={RFValue(30)} color="black" />
                <Text
                  style={{
                    marginVertical: 15,
                    fontSize: RFValue(17),
                    fontWeight: "600",
                    textAlign: "center",
                    width: "100%",
                  }}
                >
                  No recent notifications
                </Text>
              </View>
            )}
            {notifications?.map((item, index) => {
              return (
                <NotificationView
                  navigation={props.navigation}
                  key={index}
                  data={item}
                />
              );
            })}
          </View>
        ) : null}
        {projectState ? (
          <View style={{ marginTop: 20 }}>
            {projects?.length == 0 && (
              <View style={styles.errorMsg}>
                <Entypo name="emoji-sad" size={RFValue(30)} color="black" />
                <Text
                  style={{
                    marginVertical: 15,
                    fontSize: RFValue(17),
                    fontWeight: "600",
                    textAlign: "center",
                    width: "100%",
                  }}
                >
                  No recent projects
                </Text>
              </View>
            )}
            {projects?.map((item, index) => {
              let d = new Date(item?.data?.date?.seconds * 1000);
              let currentDate = new Date();
              return (
                <View key={index}>
                  {item?.data?.status == "onGoing" ? (
                    <>
                      <Text
                        style={{
                          marginTop: 20,
                          fontSize: 11,
                          fontFamily: "RBo",
                          color: "#112D4E",
                        }}
                      >
                        On Going Project
                      </Text>
                      <OnGoing
                        id={item?.id}
                        data={item?.data}
                        date={new Date(d)}
                        navigation={props.navigation}
                      />
                    </>
                  ) : item?.data?.status == "half-completed" ? (
                    <>
                      <Text
                        style={{
                          marginTop: 20,
                          fontSize: 11,
                          fontFamily: "RBo",
                          color: "#112D4E",
                        }}
                      >
                        Pending for handyman review
                      </Text>
                      <OnGoing
                        id={item?.id}
                        data={item?.data}
                        date={new Date(d)}
                        navigation={props.navigation}
                      />
                    </>
                  ) : item?.data?.status == "completed" ? (
                    <>
                      <Text
                        style={{
                          marginTop: 20,
                          fontSize: 11,
                          fontFamily: "RBo",
                          color: "#112D4E",
                        }}
                      >
                        Completed
                      </Text>
                      <Completed
                        id={item?.id}
                        data={item?.data}
                        date={new Date(d)}
                        navigation={props.navigation}
                      />
                    </>
                  ) : item?.data?.status == "New" ? (
                    <>
                      <Text
                        style={{
                          marginTop: 20,
                          fontSize: 11,
                          fontFamily: "RBo",
                          color: "#112D4E",
                        }}
                      >
                        New Project
                      </Text>
                      <New
                        id={item?.id}
                        data={item?.data}
                        date={new Date(d)}
                        navigation={props.navigation}
                      />
                    </>
                  ) : null}
                </View>
              );
            })}
          </View>
        ) : null}
      </View>
    </SafeAreaView>
  );
};
const styles = StyleSheet.create({
  activeTab: {
    backgroundColor: "#FFFFFF",
    borderRadius: 11,
    width: "50%",
    height: 36,
    alignSelf: "center",
    justifyContent: "center",
    alignItems: "center",
  },
  inActiveTab: {
    // backgroundColor:'#FFFFFF',
    // borderRadius:11,
    width: "50%",
    // height:36,
    alignSelf: "center",
    justifyContent: "center",
    alignItems: "center",
  },
  activeText: {
    color: "#EF4136",
    fontFamily: "RBo",
    fontSize: 14,
  },
  inActiveText: {
    color: "#707070",
    fontFamily: "RRe",
    fontSize: 14,
  },
  notificationBox: {
    width: "100%",
    paddingVertical: 15,
    borderRadius: 20,
    backgroundColor: "white",
    flexDirection: "row",
    paddingHorizontal: 10,
    alignItems: "center",
    marginTop: 10,
  },
  errorMsg: {
    width: "100%",
    paddingVertical: 15,
    borderRadius: 20,
    paddingHorizontal: 10,
    alignItems: "center",
    marginTop: 10,
  },
});

const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  my_jobs: state.main.my_jobs,
  get_all_notifications: state.main.get_all_notifications,
});
export default connect(mapStateToProps, { myJobs, getAllNotifications })(
  Notification
);

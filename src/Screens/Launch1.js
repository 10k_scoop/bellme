import { StatusBar } from "expo-status-bar";
import React, { useEffect, useState } from "react";
import {
  Platform,
  SafeAreaView,
  View,
  Text,
  Alert,
  Image,
  Dimensions,
} from "react-native";
import { Swipeable } from "react-native-gesture-handler";
import {
  LogoSvg,
  ORSvg,
  SwipeRedRight,
  SwipeYellowRight,
} from "../Components/SvgComponets";
// import { GestureHandler } from 'expo';
// const {Swipeable} = GestureHandler

const Launch1 = (props) => {
  const [refresh, setRefresh] = useState(false);

  // useEffect(() => {
  //     setRefresh(!refresh)
  //     return () => {
  //         setRefresh(!refresh)
  //     }
  // }, [refresh])
  var ClientLeftActions = () => (
    <View
      style={{
        flexDirection: "row",
        justifyContent: "space-between",
        width: "100%",
        height: 40,
        borderRadius: 20,
        alignSelf: "center",
        marginTop: 40,
        backgroundColor: "white",
        borderWidth: 2,
        borderColor: "#FBB040",
        justifyContent: "flex-end",
        alignItems: "center",
        paddingRight: 10,
      }}
    >
      <SwipeYellowRight />
    </View>
  );
  var HandyManLeftActions = () => (
    <View
      style={{
        flexDirection: "row",
        justifyContent: "space-between",
        width: "100%",
        height: 40,
        borderRadius: 20,
        alignSelf: "center",
        marginTop: 40,
        backgroundColor: "white",
        borderWidth: 2,
        borderColor: "#EF4136",
        justifyContent: "flex-end",
        alignItems: "center",
        paddingRight: 10,
      }}
    >
      <SwipeRedRight />
    </View>
  );
  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: "#ffffff" }}>
      <Image
        style={{
          position: "absolute",
          bottom: 0,
          width: "100%",
          resizeMode: "stretch",
        }}
        source={require("../assets/bottomImage.png")}
      />
      <StatusBar hidden={true} />
      <View
        style={{
          marginTop: Platform.OS == "android" ? "20%" : "10%",
          alignSelf: "center",
          alignItems: "center",
        }}
      >
        <LogoSvg />
        <Text
          style={{
            fontFamily: "RRe",
            fontSize: 14,
            color: "#707070",
            marginTop: 20,
          }}
        >
          Sign in or sign up for your relevant account
        </Text>
        <Text style={{ marginTop: "15%", fontSize: 14, fontFamily: "RBo" }}>
          Swipe Right to Begin
        </Text>
      </View>
      <View style={{ width: "80%", alignSelf: "center" }}>
        <Swipeable
          renderLeftActions={ClientLeftActions}
          onSwipeableLeftOpen={() => {
            props.navigation.navigate("ClientAuthStack", {
              screen: "ClientLogin",
            });
          }}
        >
          <View
            style={{
              flexDirection: "row",
              justifyContent: "space-between",
              width: "100%",
              height: 40,
              borderRadius: 20,
              alignSelf: "center",
              marginTop: 40,
              backgroundColor: "#FBB040",
            }}
          >
            <View
              style={{
                width: 36,
                height: 36,
                backgroundColor: "white",
                borderRadius: 18,
                marginLeft: 2,
                justifyContent: "center",
                alignItems: "center",
                alignSelf: "center",
              }}
            >
              <SwipeYellowRight />
            </View>
            <Text
              style={{
                alignSelf: "center",
                color: "#FFFFFF",
                fontFamily: "RBo",
              }}
            >
              I AM A CLIENT
            </Text>
            <Text></Text>
            <Text></Text>
          </View>
        </Swipeable>

        <View style={{ alignSelf: "center", marginTop: 35 }}>
          <ORSvg />
        </View>

        <Swipeable
          renderLeftActions={HandyManLeftActions}
          onSwipeableLeftOpen={() => props.navigation.navigate("HandyManAuth")}
        >
          <View
            style={{
              flexDirection: "row",
              justifyContent: "space-between",
              width: "100%",
              height: 40,
              borderRadius: 20,
              alignSelf: "center",
              marginTop: 40,
              backgroundColor: "#EF4136",
            }}
          >
            <View
              style={{
                width: 36,
                height: 36,
                backgroundColor: "white",
                borderRadius: 18,
                marginLeft: 2,
                justifyContent: "center",
                alignItems: "center",
                alignSelf: "center",
              }}
            >
              <SwipeRedRight />
            </View>
            <Text
              style={{
                alignSelf: "center",
                color: "#FFFFFF",
                fontFamily: "RBo",
              }}
            >
              I AM A SERVICE PROVIDER
            </Text>
            <Text></Text>
            <Text></Text>
          </View>
        </Swipeable>
      </View>
    </SafeAreaView>
  );
};

export default Launch1;

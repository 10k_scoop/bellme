import { LinearGradient } from "expo-linear-gradient";
import { StatusBar } from "expo-status-bar";
import React, { useEffect, useState } from "react";
import {
  SafeAreaView,
  View,
  StyleSheet,
  TouchableOpacity,
  Text,
  Image,
  RefreshControl,
  ActivityIndicator,
} from "react-native";
import { ScrollView } from "react-native-gesture-handler";
import { connect } from "react-redux";
import AppColors from "../../Components/AppColors";
import {
  getProviders,
  getUserData,
} from "../../state-management/actions/features/Features";
import * as firebase from "firebase";
import { RFValue as rf } from "react-native-responsive-fontsize";
import { Entypo } from "@expo/vector-icons";
import { logout } from "../../state-management/actions/auth/AuthActions";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { AntDesign } from "@expo/vector-icons";
const wait = (timeout) => {
  return new Promise((resolve) => setTimeout(resolve, timeout));
};

const Admin = (props) => {
  const [refreshing, setRefreshing] = React.useState(false);
  const [loading, setLoading] = useState(true);
  const [activeTab, setActiveTab] = useState(true);
  const [providers, setProviders] = useState([]);
  const [userdata, setUserData] = useState([]);
  const user = firebase.auth().currentUser;

  const onRefresh = React.useCallback(() => {
    setRefreshing(true);
    wait(2000).then(() => setRefreshing(false));
  }, []);

  useEffect(() => {
    props.getProviders(setLoading);
    props.getUserData(user.uid, setLoading);
  }, []);

  useEffect(() => {
    (async () => {
      if (props.get_user_details != null) {
        setUserData(props?.get_user_details);
      }
      if (props?.get_providers != null) {
        setProviders(props?.get_providers);
      }
    })();
  }, [props]);

  if (loading) {
    return (
      <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
        <ActivityIndicator color="black" size="large" />
      </View>
    );
  }

  return (
    <View style={{ flex: 1, backgroundColor: "#F5F5F5", borderRadius: 40 }}>
      <StatusBar hidden={true} />
      <View
        style={{
          marginTop: "13%",
          paddingBottom: 10,
          alignItems: "center",
          width: "80%",
          alignSelf: "center",
          flexDirection: "row",
          justifyContent: "space-between",
        }}
      >
        <Image style={{}} source={require("../../assets/logo.png")} />
        <TouchableOpacity onPress={() => props?.logout()}>
          <AntDesign name="logout" size={24} color="black" />
        </TouchableOpacity>
      </View>
      <View style={styles.actionBtnWrapper}>
        <TouchableOpacity
          style={[
            styles.actionBtn,
            { backgroundColor: activeTab ? "#FBB040" : "#e5e5e5" },
          ]}
          onPress={() => setActiveTab(true)}
        >
          <Text
            style={[
              styles.actionBtnText,
              { color: activeTab ? "#fff" : "#222" },
            ]}
          >
            Verified
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={[
            styles.actionBtn,
            { backgroundColor: !activeTab ? "#FBB040" : "#e5e5e5" },
          ]}
          onPress={() => setActiveTab(false)}
        >
          <Text
            style={[
              styles.actionBtnText,
              { color: !activeTab ? "#fff" : "#222" },
            ]}
          >
            Unverified
          </Text>
        </TouchableOpacity>
      </View>
      <ScrollView
        refreshControl={
          <RefreshControl
            refreshing={refreshing}
            onRefresh={onRefresh}
            tintColor="#EF4136"
          />
        }
        contentContainerStyle={{ paddingBottom: 100 }}
        showsVerticalScrollIndicator={false}
      >
        <View style={{ width: "80%", alignSelf: "center" }}>
          {providers?.map((item, index) => {
            if (item?.data?.verified == activeTab) {
              return (
                <View style={styles.cardWrapper} key={index}>
                  <View style={styles.profile}>
                    <Image
                      source={{
                        uri:
                          item?.data?.businessNameImg?.length > 1
                            ? item?.data?.businessNameImg
                            : "https://static.vecteezy.com/packs/media/components/global/search-explore-nav/img/vectors/term-bg-1-666de2d941529c25aa511dc18d727160.jpg",
                      }}
                      resizeMode="cover"
                      style={{ width: "100%", height: "100%" }}
                    />
                  </View>
                  <View style={{ flex: 1 }}>
                    <Text style={styles.cardTitle}>{item?.data?.name}</Text>
                    <Text style={[styles.cardTitle, { fontSize: rf(12) }]}>
                      {item?.data?.email}
                    </Text>
                  </View>
                  <TouchableOpacity
                    style={styles.btn}
                    onPress={() =>
                      props?.navigation.navigate("ProviderDetail", {
                        data: item?.data,
                        id: item?.id,
                      })
                    }
                  >
                    <Text style={styles.btnText}>View</Text>
                  </TouchableOpacity>
                </View>
              );
            }
          })}
        </View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  actionBtnWrapper: {
    flexDirection: "row",
    width: wp("100%"),
    paddingHorizontal: wp("5%"),
    alignItems: "center",
    justifyContent: "space-between",
    marginVertical: hp("3%"),
  },
  actionBtn: {
    width: "45%",
    height: hp("6%"),
    borderRadius: 12,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#FBB040",
  },
  actionBtnText: {
    fontSize: rf(14),
    fontFamily: "RMe",
    color: "#fff",
  },
  cardWrapper: {
    width: "100%",
    height: hp("8%"),
    backgroundColor: "#cdcdcd",
    borderRadius: 12,
    flexDirection: "row",
    paddingHorizontal: "3%",
    alignItems: "center",
  },
  profile: {
    width: wp("10%"),
    height: wp("10%"),
    borderRadius: 100,
    backgroundColor: "grey",
    marginRight: 10,
    overflow: "hidden",
  },
  cardTitle: {
    fontSize: rf(14),
    fontFamily: "RMe",
    color: "#222",
  },
  btn: {
    width: wp("25%"),
    height: hp("4%"),
    borderRadius: 6,
    backgroundColor: "#FBB040",
    alignItems: "center",
    justifyContent: "center",
  },
  btnText: {
    fontSize: rf(12),
    fontFamily: "RMe",
    color: "#fff",
  },
});

const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  get_user_details: state.main.get_user_details,
  get_providers: state.main.get_providers,
});
export default connect(mapStateToProps, { getUserData, logout, getProviders })(
  Admin
);

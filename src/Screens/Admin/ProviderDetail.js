import { LinearGradient } from "expo-linear-gradient";
import { StatusBar } from "expo-status-bar";
import React, { useEffect, useState } from "react";
import {
  SafeAreaView,
  View,
  StyleSheet,
  TouchableOpacity,
  Text,
  Image,
  RefreshControl,
  ActivityIndicator,
} from "react-native";
import { ScrollView } from "react-native-gesture-handler";
import { connect } from "react-redux";
import AppColors from "../../Components/AppColors";
import {
  getProviders,
  getUserData,
  updateProviderVerification,
} from "../../state-management/actions/features/Features";
import * as firebase from "firebase";
import { RFValue as rf } from "react-native-responsive-fontsize";
import { Entypo } from "@expo/vector-icons";
import { logout } from "../../state-management/actions/auth/AuthActions";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { AntDesign } from "@expo/vector-icons";

const wait = (timeout) => {
  return new Promise((resolve) => setTimeout(resolve, timeout));
};

const ProviderDetail = (props) => {
  const [refreshing, setRefreshing] = React.useState(false);
  const [loading, setLoading] = useState(false);
  const [imagePreviewPopUp, setImagePreviewPopUp] = useState(false);
  const [imagePreviewUrl, setImagePreviewUrl] = useState(null);
  const user = firebase.auth().currentUser;

  let data = props?.route.params?.data;
  let id = props?.route.params?.id;
  const onRefresh = React.useCallback(() => {
    setRefreshing(true);
    wait(2000).then(() => setRefreshing(false));
  }, []);

  if (loading) {
    return (
      <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
        <ActivityIndicator color="black" size="large" />
      </View>
    );
  }

  const onUpdate = (type) => {
    setLoading(true);
    let data = {
      verified: type,
    };
    props?.updateProviderVerification(
      data,
      id,
      setLoading,
      props?.navigation
    );
  };

  return (
    <View style={{ flex: 1, backgroundColor: "#F5F5F5", borderRadius: 40 }}>
      <StatusBar hidden={true} />
      <View
        style={{
          marginTop: "10%",
          paddingBottom: 10,
          alignItems: "center",
          width: "80%",
          alignSelf: "center",
          flexDirection: "row",
          justifyContent: "space-between",
        }}
      >
        <Image style={{}} source={require("../../assets/logo.png")} />
      </View>
      {imagePreviewPopUp && (
        <View style={styles.previewImageWrapper}>
          <TouchableOpacity
            style={{ marginVertical: 10, top: hp("2%") }}
            onPress={() => {
              setImagePreviewPopUp(false);
              setImagePreviewUrl(null);
            }}
          >
            <AntDesign name="closecircle" size={24} color="white" />
          </TouchableOpacity>
          <Image
            source={{
              uri: imagePreviewUrl,
            }}
            resizeMode="contain"
            style={{ width: "100%", height: "100%" }}
          />
        </View>
      )}
      <ScrollView
        refreshControl={
          <RefreshControl
            refreshing={refreshing}
            onRefresh={onRefresh}
            tintColor="#EF4136"
          />
        }
        contentContainerStyle={{ paddingBottom: 100 }}
        showsVerticalScrollIndicator={false}
      >
        <View
          style={{
            width: "80%",
            alignItems: "center",
            alignSelf: "center",
            marginTop: hp("3%"),
          }}
        >
          <View style={styles.profile}>
            <Image
              source={{
                uri:
                  data?.businessNameImg?.length > 1
                    ? data?.businessNameImg
                    : "https://static.vecteezy.com/packs/media/components/global/search-explore-nav/img/vectors/term-bg-1-666de2d941529c25aa511dc18d727160.jpg",
              }}
              resizeMode="cover"
              style={{ width: "100%", height: "100%" }}
            />
          </View>
          <Text style={styles.smallText}>{data?.name}</Text>
          <View style={styles.detailBox}>
            <View style={styles.detailBoxRow}>
              <Text style={styles.detailBoxText1}>Email :</Text>
              <Text style={styles.detailBoxText}>{data?.email}</Text>
            </View>
            <View style={styles.detailBoxRow}>
              <Text style={styles.detailBoxText1}>Phone :</Text>
              <Text style={styles.detailBoxText}>{data?.phone}</Text>
            </View>
            <View style={styles.detailBoxRow}>
              <Text style={styles.detailBoxText1}>Address :</Text>
              <Text style={styles.detailBoxText}>{data?.address}</Text>
            </View>
            <View style={styles.detailBoxRow}>
              <Text style={styles.detailBoxText1}>Category :</Text>
              <Text style={styles.detailBoxText}>{data?.subCategory}</Text>
            </View>
            <View style={styles.detailBoxRow}>
              <Text style={styles.detailBoxText1}>Status :</Text>
              <Text style={styles.detailBoxText}>
                {data?.verified ? "Verified" : "Not Verified"}
              </Text>
            </View>
            <View style={styles.detailBoxRow}>
              <Text style={styles.detailBoxText1}>Address Image :</Text>
              <TouchableOpacity
                style={styles.documentImage}
                onPress={() => {
                  setImagePreviewPopUp(true);
                  setImagePreviewUrl(data?.addressImg);
                }}
              >
                <Image
                  source={{
                    uri: data?.addressImg,
                  }}
                  resizeMode="cover"
                  style={{ width: "100%", height: "100%" }}
                />
              </TouchableOpacity>
            </View>

            <View style={styles.detailBoxRow}>
              <Text style={styles.detailBoxText1}>Business Image :</Text>
              <TouchableOpacity
                style={styles.documentImage}
                onPress={() => {
                  setImagePreviewPopUp(true);
                  setImagePreviewUrl(data?.businessNameImg);
                }}
              >
                <Image
                  source={{
                    uri: data?.businessNameImg,
                  }}
                  resizeMode="cover"
                  style={{ width: "100%", height: "100%" }}
                />
              </TouchableOpacity>
            </View>

            <View style={styles.detailBoxRow}>
              <Text style={styles.detailBoxText1}>
                Police clearance Image :
              </Text>
              <TouchableOpacity
                style={styles.documentImage}
                onPress={() => {
                  setImagePreviewPopUp(true);
                  setImagePreviewUrl(data?.policeClearanceImg);
                }}
              >
                <Image
                  source={{
                    uri: data?.policeClearanceImg,
                  }}
                  resizeMode="cover"
                  style={{ width: "100%", height: "100%" }}
                />
              </TouchableOpacity>
            </View>

            <View style={styles.detailBoxRow}>
              <Text style={styles.detailBoxText1}>Gurantee Form Image :</Text>
              <TouchableOpacity
                style={styles.documentImage}
                onPress={() => {
                  setImagePreviewPopUp(true);
                  setImagePreviewUrl(data?.guranteeForm);
                }}
              >
                <Image
                  source={{
                    uri: data?.guranteeForm,
                  }}
                  resizeMode="cover"
                  style={{ width: "100%", height: "100%" }}
                />
              </TouchableOpacity>
            </View>
          </View>
          {data?.verified ? (
            <TouchableOpacity
              style={styles.btn}
              onPress={() => onUpdate(false)}
            >
              <Text style={styles.btnText}>Cancel Verification</Text>
            </TouchableOpacity>
          ) : (
            <TouchableOpacity style={styles.btn} onPress={() => onUpdate(true)}>
              <Text style={styles.btnText}>Verify</Text>
            </TouchableOpacity>
          )}
        </View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  smallText: {
    fontSize: rf(15),
    fontFamily: "RBo",
    color: "#222",
    marginTop: 10,
  },
  profile: {
    width: wp("20%"),
    height: wp("20%"),
    borderRadius: 100,
    backgroundColor: "grey",
    marginRight: 10,
    overflow: "hidden",
  },
  detailBox: {
    width: "100%",
    alignItems: "flex-start",
    minHeight: hp("25%"),
    paddingVertical: hp("2%"),
    backgroundColor: "#e5e5e5",
    borderRadius: 12,
    marginVertical: hp("2%"),
    paddingHorizontal: wp("5%"),
  },
  detailBoxRow: {
    width: "100%",
    alignItems: "center",
    flexDirection: "row",
    justifyContent: "space-between",
    marginVertical: 7,
  },
  detailBoxText: {
    fontSize: rf(13),
    fontFamily: "RBo",
    color: "#222",
  },
  detailBoxText1: {
    fontSize: rf(13),
    fontFamily: "RMe",
    color: "#222",
  },
  documentImage: {
    width: wp("10%"),
    height: wp("10%"),
    borderRadius: 10,
    backgroundColor: "grey",
    marginRight: 10,
    overflow: "hidden",
  },
  previewImageWrapper: {
    width: wp("100%"),
    height: hp("100%"),
    backgroundColor: "black",
    padding: 10,
    position: "absolute",
    zIndex: 9999999999999999999,
    alignItems: "center",
    justifyContent: "center",
    paddingVertical: hp("5%"),
  },
  btn: {
    width: wp("45%"),
    height: hp("6%"),
    borderRadius: 6,
    backgroundColor: "#FBB040",
    alignItems: "center",
    justifyContent: "center",
  },
  btnText: {
    fontSize: rf(16),
    fontFamily: "RMe",
    color: "#fff",
  },
});

const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  get_user_details: state.main.get_user_details,
  get_providers: state.main.get_providers,
});
export default connect(mapStateToProps, {
  getUserData,
  logout,
  getProviders,
  updateProviderVerification,
})(ProviderDetail);

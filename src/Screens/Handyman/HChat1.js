import React, { useCallback, useEffect, useState } from "react";
import {
  SafeAreaView,
  View,
  Text,
  TouchableOpacity,
  Platform,
  StyleSheet,
  Image,
  FlatList,
  ActivityIndicator,
  Alert,
} from "react-native";
import { connect } from "react-redux";
import {
  sendMsg,
  getChatRelations,
  deleteChat,
} from "../../state-management/actions/features/Features";
import * as firebase from "firebase";
import { RFValue } from "react-native-responsive-fontsize";
import { AntDesign, Entypo } from "@expo/vector-icons";
import base64 from "react-native-base64";

const HChat1 = (props) => {
  const user = firebase.auth().currentUser;
  const messagesData = [];
  const [loading, setLoading] = useState(true);
  const [chats, setChats] = useState([]);

  useEffect(() => {
    props?.getChatRelations(user.email, setLoading);
  }, []);

  useEffect(() => {
    if (props?.get_chat_relations != "") {
      setChats(props?.get_chat_relations);
    }
  }, [props]);

  console.log(props?.errors);

  const keyExtractor = useCallback((item, index) => index.toString(), []);
  const renderItem = useCallback(({ item, index }) => {
    let details =
      user.email == item?.data?.chatDetails?.Hmandetails?.email
        ? item?.data?.chatDetails?.clientDetails
        : item?.data?.chatDetails?.Hmandetails;
    let id1 = base64.encode(
      item?.data?.chatDetails?.clientDetails?.email +
        item?.data?.chatDetails?.Hmandetails?.email
    );
    let id2 = base64.encode(
      item?.data?.chatDetails?.Hmandetails?.email +
        item?.data?.chatDetails?.clientDetails?.email
    );

    if (item?.data?.deletedFor?.includes(user.email)) {
      return;
    }

    return (
      <TouchableOpacity
        onLongPress={() => {
          Alert.alert("Delete Chat", "Do you confirm to delete this chat?", [
            {
              text: "Cancel",
              onPress: () => console.log("Cancel Pressed"),
              style: "cancel",
            },
            {
              text: "Yes",
              onPress: () => {
                setLoading(true);
                let dArr =
                  item?.data?.deletedFor?.length >= 1
                    ? item?.data?.deletedFor
                    : [];
                dArr.push(user.email);
                props?.deleteChat(
                  item?.id,
                  setLoading,
                  user.email,
                  [id1, id2],
                  dArr
                );
              },
            },
          ]);
        }}
        onPress={() =>
          props.navigation.navigate("HHomeStack", {
            screen: "HChat2",
            params: {
              data: item?.data?.chatDetails,
              relationId: item?.id,
              deletedFor: item?.data?.deletedFor,
            },
          })
        }
        style={styles.msgBox}
      >
        <View style={styles.avatar}>
          <Text style={{ color: "#fff", fontSize: 16, fontFamily: "RBo" }}>
            {details?.name[0]}
          </Text>
        </View>
        <View style={{ marginLeft: 10, flex: 1 }}>
          <Text style={{ color: "#FBB040", fontSize: 14, fontFamily: "RBo" }}>
            {details?.name}
          </Text>
          <Text style={{ fontSize: 12, fontFamily: "RRe", color: "#707070" }}>
            {item?.data?.lastText}
          </Text>
        </View>
        <TouchableOpacity
          onPress={() => {
            Alert.alert("Delete Chat", "Do you confirm to delete this chat?", [
              {
                text: "Cancel",
                onPress: () => console.log("Cancel Pressed"),
                style: "cancel",
              },
              {
                text: "Yes",
                onPress: () => {
                  setLoading(true);
                  let dArr =
                    item?.data?.deletedFor?.length >= 1
                      ? item?.data?.deletedFor
                      : [];
                  dArr.push(user.email);
                  props?.deleteChat(
                    item?.id,
                    setLoading,
                    user.email,
                    [id1, id2],
                    dArr
                  );
                },
              },
            ]);
          }}
        >
          <AntDesign name="delete" size={RFValue(15)} color="red" />
        </TouchableOpacity>
      </TouchableOpacity>
    );
  });

  if (loading) {
    return (
      <View
        style={{
          flex: 1,
          position: "relative",
          top: 100,
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        <ActivityIndicator color="black" size="large" />
      </View>
    );
  }
  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: "#F5F5F5" }}>
      <Text
        style={{
          fontSize: 20,
          fontFamily: "RBo",
          color: "#EF4136",
          alignSelf: "center",
          marginTop: Platform.OS == "android" ? "10%" : 0,
        }}
      >
        Messages
      </Text>
      <View style={{ marginTop: 30, width: "80%", alignSelf: "center" }}>
        {chats?.length >= 1 ? (
          <>
            {loading ? (
              <View
                style={{
                  flex: 1,
                  position: "relative",
                  top: 100,
                  alignItems: "center",
                  justifyContent: "center",
                }}
              >
                <ActivityIndicator color="black" size="large" />
              </View>
            ) : (
              <FlatList
                data={chats?.length >= 1 ? chats : []}
                renderItem={renderItem}
                keyExtractor={keyExtractor}
              />
            )}
          </>
        ) : (
          <View style={styles.errorMsg}>
            <Entypo name="emoji-sad" size={RFValue(30)} color="black" />
            <Text
              style={{
                marginVertical: 15,
                fontSize: RFValue(17),
                fontWeight: "600",
                textAlign: "center",
                width: "100%",
              }}
            >
              No recent chats
            </Text>
          </View>
        )}
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  msgBox: {
    width: "100%",
    height: 60,
    alignSelf: "center",
    backgroundColor: "#FFFFFF",
    borderRadius: 20,
    flexDirection: "row",
    padding: 15,
    marginTop: 12,
  },
  avatar: {
    width: 40,
    height: 40,
    backgroundColor: "#FBB040",
    borderRadius: 100,
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  errorMsg: {
    width: "100%",
    paddingVertical: 15,
    borderRadius: 20,
    paddingHorizontal: 10,
    alignItems: "center",
    marginTop: 10,
  },
});

const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  get_user_details: state.main.get_user_details,
  get_chat_relations: state.main.get_chat_relations,
});
export default connect(mapStateToProps, {
  sendMsg,
  getChatRelations,
  deleteChat,
})(HChat1);

import { LinearGradient } from 'expo-linear-gradient';
import React, { useEffect, useState } from 'react'
import { SafeAreaView, View, Text, TouchableOpacity, Dimensions, Image, StyleSheet, } from 'react-native';
import AppColors from '../../Components/AppColors';
import { ArrowLeft, ChatIconLocal, PhoneIcon, PhoneIconLarge } from '../../Components/SvgComponets';
import MapView from 'react-native-maps';
import * as Location from 'expo-location';
import { ActivityIndicator } from 'react-native-paper';
const HLiveLocation = (props) => {

    const [location, setLocation] = useState(null);
    const [errorMsg, setErrorMsg] = useState(null);

    useEffect(() => {
        (async () => {
            let { status } = await Location.requestForegroundPermissionsAsync();
            if (status !== 'granted') {
                setErrorMsg('Permission to access location was denied');
                return;
            }

            let location = await Location.getCurrentPositionAsync({});
            setLocation(location);
        })();
    }, []);



    return (
        <View style={{ flex: 1, backgroundColor: '#F5F5F5', }}>

            <View style={{ flex: 1, width: "100%", alignSelf: 'center', paddingTop: Platform.OS == "android" ? "10%" : 0 }}>
                {location == null ?
                    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                        <ActivityIndicator size="large" color="black" />
                    </View>
                    :
                    <MapView
                        style={styles.map}
                        region={{ latitude: location.coords.latitude, longitude: location.coords.longitude, latitudeDelta: 0.0922, longitudeDelta: 0.0421 }}
                        showsMyLocationButton
                        showsUserLocation
                    />
                }


                <View style={{ paddingHorizontal: '5%', marginTop: '15%', width: "100%", flexDirection: 'row', justifyContent: 'space-between' }}>
                    <TouchableOpacity
                        onPress={() => {
                            props.navigation.navigate("HHome");
                        }}
                    >
                        <ArrowLeft />
                    </TouchableOpacity>
                    <Text style={{ color: '#EF4136', fontFamily: 'RBo', fontSize: 20 }}>Live Location</Text>
                    <Text></Text>
                </View>
                <TouchableOpacity
                    onPress={() => props.navigation.navigate('HFeedback')}
                    style={{ position: 'absolute', bottom: 120, width: "80%", alignSelf: 'center', height: 40, marginTop: 20 }}>
                    <LinearGradient
                        style={{ flex: 1, borderRadius: 13, alignItems: 'center', justifyContent: 'center', }}
                        colors={[AppColors.linear1, AppColors.linear2]}
                    >
                        <Text style={{ color: 'white', fontFamily: 'RBo', fontSize: 14 }}>Service Provider Has Reached </Text>
                    </LinearGradient>
                </TouchableOpacity>

                <TouchableOpacity
                    // onPress={()=>props.navigation.navigate('LiveLocation1')}
                    style={styles.btmContainer}>
                    <Image
                        source={require("../../assets/profileIconSmall.png")}
                    />
                    <Text style={{ marginLeft: 10, fontSize: 12, fontFamily: 'RRe', color: '#EF4136' }}>Tina<Text style={{ color: "#707070" }}> is on her way</Text></Text>
                    <View style={{ flexDirection: 'row', position: 'absolute', right: 10, }}>
                        <TouchableOpacity
                        // onPress={()=>props.navigation.navigate('ChatStack')}
                        >
                            <PhoneIconLarge />
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={() => props.navigation.navigate('ChatStack')}
                            style={{ marginLeft: 25 }}>
                            <ChatIconLocal />
                        </TouchableOpacity>
                    </View>
                </TouchableOpacity>
            </View>

        </View>
    )
}

const styles = StyleSheet.create({
    btmContainer: {
        position: 'absolute',
        bottom: 20,
        width: "85%",
        alignSelf: 'center',
        height: 60,
        backgroundColor: 'white',
        borderRadius: 20,
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 15
    },
    map: {
        flex: 1,
        position: 'absolute',
        width: '100%',
        height: '130%',
    }
})
export default HLiveLocation

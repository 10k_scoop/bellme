import React, { useState,useEffect } from "react";
import {
  SafeAreaView,
  View,
  TouchableOpacity,
  Text,
  Image,
  TextInput,
  StyleSheet,
  ActivityIndicator,
} from "react-native";
import MaskedView from "@react-native-community/masked-view";
import { LinearGradient } from "expo-linear-gradient";
import { ScrollView } from "react-native-gesture-handler";
import AppColors from "../../Components/AppColors";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import {
  ArrowDown,
  ArrowLeft,
  DollarIcon,
  ORSvg,
} from "../../Components/SvgComponets";
import Modal from "react-native-modal";
import { heightPercentageToDP } from "react-native-responsive-screen";
import { connect } from "react-redux";
import { applyToJob } from "../../state-management/actions/features/Features";
import * as firebase from "firebase";
import * as Location from "expo-location";

const SubmitQuote = (props) => {
  const [modalVisible, setModalVisible] = useState(false);
  const [days, setDays] = useState([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]);
  const [showDays, setShowDays] = useState(false);
  const [selectedDay, setSelectedDay] = useState(1);
  const [projectCost, setProjectCost] = useState("");
  const [hourlyRate, setHourlyRate] = useState("");
  const [totalDuration, setTotalDuration] = useState("");
  const [loading, setLoading] = useState(false);
  const user = firebase.auth().currentUser;
  let data = props.route.params?.data;
  const [location, setLocation] = useState(null);
  const [errorMsg, setErrorMsg] = useState(null);

  useEffect(() => {
    (async () => {
      let { status } = await Location.requestForegroundPermissionsAsync();
      if (status !== "granted") {
        setErrorMsg("Permission to access location was denied");
        return;
      }

      let location = await Location.getCurrentPositionAsync({});
      setLocation(location?.coords);
    })();
  }, []);

  const onSubmit = async () => {
    let applicantArr = data?.applicants;
    applicantArr.push(props?.get_user_details?.email);
    let nData = {
      Hmandetails: {
        name: props?.get_user_details?.name,
        liveLocation: null,
        id: user.uid,
        email: props?.get_user_details?.email,
        phone: props?.get_user_details?.phone,
        subCategory: props?.get_user_details?.subCategory,
        policeClearanceImg: props?.get_user_details?.policeClearanceImg,
        businessNameImg: props?.get_user_details?.businessNameImg,
        addressImg: props?.get_user_details?.addressImg,
        pricePerHours: props?.get_user_details?.hRate,
        verified: props?.get_user_details?.verified,
        rating: props?.get_user_details?.rating,
        hired: props?.get_user_details?.hired || null,
        appliedAt: new Date(),
        message: "Your application has been submitted " + data?.authorEmail,
        handyManLocations:JSON.stringify(location)
      },
      jobDetails: {
        id: data?.id,
        subCategory: data?.subCategory,
        status: "Pending",
        hiredSomeone: false,
        budget: data?.budget,
        quoteByHman: projectCost,
        hourlyRate: hourlyRate,
        tip: null,
        location: data?.location,
        category: data?.category,
        duration: data?.duration,
        date: data?.date,
        durationByHman: selectedDay,
        applicants: applicantArr,
      },
      clientDetails: {
        name: data?.author,
        email: data?.authorEmail,
        id: data?.authorId,
        message: props?.get_user_details?.name + " has applied for this job.",
      },
    };

    if (hourlyRate.length > 0 || projectCost.length > 0) {
      setLoading(true);
      var res = await props.applyToJob(
        { data: nData, createdAt: new Date() },
        setLoading,
        setModalVisible
      );
    } else {
      alert("Fill details");
    }
  };

  if (loading) {
    return (
      <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
        <ActivityIndicator color="black" size="large" />
      </View>
    );
  }

  const Quote = () => (
    <View
      style={{
        backgroundColor: "#fff",
        width: "100%",
        paddingVertical: 10,
        paddingBottom: 20,
        marginTop: 10,
        borderRadius: 20,
        paddingLeft: 15,
        paddingRight: 20,
      }}
    >
      <View style={{ flexDirection: "row", alignItems: "center" }}>
        <Image
          style={{ width: 40, height: 40, borderRadius: 13 }}
          source={{ uri: data?.authorProfile }}
        />
        <Text
          style={{
            marginLeft: 10,
            color: "#6C6C6C",
            fontFamily: "RBo",
            fontSize: 14,
          }}
        >
          {data?.author}
        </Text>
        <Text
          style={{
            position: "absolute",
            right: 0,
            fontFamily: "RRe",
            fontSize: 10,
            color: "#112D4E",
          }}
        >
          Just Now
        </Text>
      </View>

      <View style={{ flexDirection: "row", width: "100%", marginTop: 10 }}>
        <View
          style={{
            width: 5,
            height: "100%",
            borderRadius: 20,
            backgroundColor: "#EF4136",
          }}
        ></View>
        <View style={{ marginLeft: 10,flex:1 }}>
          <Text style={{ color: "#EF4136", fontFamily: "RBo", fontSize: 14 }}>
            {data?.subCategory}
          </Text>
          <Text
            style={{
              color: "#707070",
              fontFamily: "RRe",
              fontSize: 11,
              lineHeight: 15,
            }}
          >
            {data?.desc}
          </Text>
          <View
            style={{
              flexDirection: "row",
              justifyContent: "space-between",
              marginTop: 25,
            }}
          >
            <Text style={{ color: "#6C6C6C", fontFamily: "RBo", fontSize: 12 }}>
              Budget
            </Text>
            <Text style={{ color: "#FBB040", fontFamily: "RBo", fontSize: 12 }}>
              ${data?.budget}
            </Text>
          </View>
          <View
            style={{
              flexDirection: "row",
              justifyContent: "space-between",
              marginTop: 15,
            }}
          >
            <Text style={{ color: "#6C6C6C", fontFamily: "RBo", fontSize: 12 }}>
              Duration
            </Text>
            <Text style={{ color: "#FBB040", fontFamily: "RBo", fontSize: 12 }}>
              {data?.duration}
            </Text>
          </View>
          <View
            style={{
              flexDirection: "row",
              justifyContent: "space-between",
              marginTop: 15,
            }}
          >
            <Text style={{ color: "#6C6C6C", fontFamily: "RBo", fontSize: 12 }}>
              Location
            </Text>
            <Text style={{ color: "#FBB040", fontFamily: "RBo", fontSize: 12 }}>
              {data?.location || "not available"}
            </Text>
          </View>
        </View>
      </View>
    </View>
  );

  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: "#F5F5F5" }}>
      <View
        style={{
          flex: 1,
          height: "100%",
          width: "80%",
          alignSelf: "center",
          marginTop: Platform.OS == "android" ? "10%" : 0,
        }}
      >
        <View
          style={{
            width: "100%",
            flexDirection: "row",
            justifyContent: "space-between",
          }}
        >
          <TouchableOpacity
            onPress={() => {
              props.navigation.goBack();
            }}
          >
            <ArrowLeft />
          </TouchableOpacity>
          <Text style={{ color: "#EF4136", fontFamily: "RBo", fontSize: 20 }}>
            Submit Quote
          </Text>
          <Text></Text>
        </View>
        <ScrollView contentContainerStyle={{ flex: 1 }}>
          <View style={{ marginTop: 10 }}>
            <Quote />
          </View>

          <View
            style={{
              justifyContent: "flex-end",
              marginTop: 20,
              width: "100%",
              height: 40,
              flexDirection: "row",
              backgroundColor: "#E4E4E4",
              borderRadius: 13,
              alignItems: "center",
              paddingHorizontal: 10,
            }}
          >
            <Text
              style={{
                position: "absolute",
                left: 10,
                color: "#707070",
                fontFamily: "RRe",
              }}
            >
              Project Cost
            </Text>
            <TextInput
              placeholderTextColor="#EF4136"
              placeholder="$00.00"
              keyboardType="numeric"
              style={{
                width: "20%",
                height: 40,
                color: "#EF4136",
                fontFamily: "RRe",
                fontSize: 14,
              }}
              onChangeText={(val) => setProjectCost(val)}
            />
            <DollarIcon />
          </View>

          <MaskedView
            style={{
              width: "10%",
              height: 30,
              marginTop: 25,
              alignSelf: "center",
            }}
            maskElement={
              <Text style={{ fontSize: 20, fontFamily: "RBo" }}>OR</Text>
            }
          >
            <LinearGradient
              colors={[AppColors.linear1, AppColors.linear2]}
              style={{ flex: 1 }}
            />
          </MaskedView>

          <View
            style={{
              justifyContent: "flex-end",
              marginTop: 20,
              width: "100%",
              height: 40,
              flexDirection: "row",
              backgroundColor: "#FFFFFF",
              borderRadius: 13,
              alignItems: "center",
              paddingHorizontal: 10,
            }}
          >
            <Text
              style={{
                position: "absolute",
                left: 10,
                color: "#707070",
                fontFamily: "RRe",
              }}
            >
              Hourly Rate
            </Text>
            <TextInput
              placeholderTextColor="#EF4136"
              placeholder="$00.00"
              keyboardType="numeric"
              style={{
                width: "20%",
                height: 40,
                color: "#EF4136",
                fontFamily: "RRe",
                fontSize: 14,
              }}
              onChangeText={(val) => setHourlyRate(val)}
            />
            <DollarIcon />
          </View>

          <TouchableOpacity
            style={{
              justifyContent: "flex-end",
              marginTop: 20,
              width: "100%",
              height: 40,
              flexDirection: "row",
              backgroundColor: "#FFFFFF",
              borderRadius: 13,
              alignItems: "center",
              paddingHorizontal: 10,
            }}
            onPress={() => setShowDays(true)}
          >
            <Text
              style={{
                position: "absolute",
                left: 10,
                color: "#707070",
                fontFamily: "RRe",
              }}
            >
              Total Duration
            </Text>
            <Text
              style={{
                alignSelf: "center",
                width: "20%",
                color: "#EF4136",
                fontFamily: "RRe",
                fontSize: 14,
              }}
            >
              {selectedDay + " Days"}
            </Text>
            <ArrowDown />
          </TouchableOpacity>

          <TouchableOpacity
            onPress={onSubmit}
            style={{
              bottom: 0,
              width: "100%",
              height: 40,
              marginTop: 20,
              bottom: heightPercentageToDP("10%"),
              marginTop: 150,
            }}
          >
            <LinearGradient
              style={{
                flex: 1,
                borderRadius: 13,
                alignItems: "center",
                justifyContent: "center",
                flexDirection: "row",
              }}
              colors={[AppColors.linear1, AppColors.linear2]}
            >
              <Text style={{ color: "white", fontFamily: "RBo", fontSize: 14 }}>
                S U B M I T
              </Text>
              <Text
                style={{
                  left: 10,
                  color: "white",
                  fontFamily: "RBo",
                  fontSize: 14,
                }}
              >
                Q U O T E
              </Text>
            </LinearGradient>
          </TouchableOpacity>
        </ScrollView>
      </View>

      <Modal
        isVisible={modalVisible}
        animationIn="fadeIn"
        animationInTiming={600}
        animationOut="fadeOut"
        // backdropTransitionInTiming={500}
        backdropTransitionOutTiming={0}
      >
        <View
          style={{
            alignSelf: "center",
            width: "90%",
            paddingHorizontal: 15,
            height: 300,
            backgroundColor: "white",
            borderRadius: 20,
            paddingTop: 40,
            alignItems: "center",
          }}
        >
          <Image source={require("../../assets/congratsImage.png")} />
          <MaskedView
            style={{
              width: "50%",
              height: 30,
              marginTop: 20,
              alignSelf: "center",
            }}
            maskElement={
              <Text style={{ fontSize: 20, fontFamily: "RBo" }}>
                Congratulations
              </Text>
            }
          >
            <LinearGradient
              colors={[AppColors.linear1, AppColors.linear2]}
              style={{ flex: 1 }}
            />
          </MaskedView>
          <Text
            style={{
              textAlign: "center",
              marginTop: 5,
              lineHeight: 16,
              fontFamily: "RRe",
              fontSize: 12,
              color: "#707070",
            }}
          >
            Quotation has been sent successfullly.
          </Text>
          <TouchableOpacity
            onPress={() => {
              props.navigation.navigate("HMBottomTabNavigator", {
                screen: "HNotification",
                params: { route: "quote" },
              });
              setModalVisible(false);
            }}
            style={{
              bottom: 20,
              width: "100%",
              height: 40,
              marginTop: 20,
              alignSelf: "center",
              position: "absolute",
            }}
          >
            <LinearGradient
              style={{
                flex: 1,
                borderRadius: 13,
                alignItems: "center",
                justifyContent: "center",
              }}
              colors={[AppColors.linear1, AppColors.linear2]}
            >
              <Text style={{ color: "white", fontFamily: "RBo", fontSize: 14 }}>
                Proceed
              </Text>
            </LinearGradient>
          </TouchableOpacity>
        </View>
      </Modal>

      {showDays && (
        <View style={styles.pickerPopUpWrapper}>
          <View style={styles.pickerPopUp}>
            <ScrollView>
              {days.map((item, index) => {
                return (
                  <TouchableOpacity
                    onPress={() => {
                      setSelectedDay(item);
                      setShowDays(false);
                    }}
                    style={styles.pickerPopUpItem}
                    key={index}
                  >
                    <Text>{item + " Day"}</Text>
                  </TouchableOpacity>
                );
              })}
            </ScrollView>
          </View>
        </View>
      )}
    </SafeAreaView>
  );
};
const styles = StyleSheet.create({
  pickerPopUp: {
    width: "90%",
    backgroundColor: "#fff",
    borderWidth: 0.1,
    borderRadius: 10,
    position: "absolute",
    paddingVertical: hp("2%"),
    maxHeight: "80%",
    overflow: "scroll",
  },
  pickerPopUpWrapper: {
    width: wp("100%"),
    height: hp("100%"),
    position: "absolute",
    alignItems: "center",
    justifyContent: "center",
  },
  pickerPopUpItem: {
    width: "100%",
    height: hp("5%"),
    borderBottomWidth: 0.5,
    justifyContent: "center",
    paddingHorizontal: "5%",
    borderColor: "orange",
  },
});

const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  get_user_details: state.main.get_user_details,
});
export default connect(mapStateToProps, { applyToJob })(SubmitQuote);

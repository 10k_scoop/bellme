import { LinearGradient } from "expo-linear-gradient";
import { StatusBar } from "expo-status-bar";
import React, { useEffect, useState } from "react";
import {
  SafeAreaView,
  View,
  StyleSheet,
  TouchableOpacity,
  Text,
  Image,
  RefreshControl,
  ActivityIndicator,
} from "react-native";
import { ScrollView } from "react-native-gesture-handler";
import { connect } from "react-redux";
import AppColors from "../../Components/AppColors";
import { RefreshIcon } from "../../Components/SvgComponets";
import {
  getJobs,
  getUserData,
} from "../../state-management/actions/features/Features";
import * as firebase from "firebase";
import { RFValue } from "react-native-responsive-fontsize";
import { Entypo } from "@expo/vector-icons";

const wait = (timeout) => {
  return new Promise((resolve) => setTimeout(resolve, timeout));
};

const HHome = (props) => {
  const [refreshing, setRefreshing] = React.useState(false);
  const [loading, setLoading] = useState(true);
  const [jobs, setJobs] = useState([]);
  const [filteredJobs, setFilteredJobs] = useState([]);
  const user = firebase.auth().currentUser;
  const onRefresh = React.useCallback(() => {
    setRefreshing(true);
    wait(2000).then(() => setRefreshing(false));
  }, []);

  useEffect(() => {
    props.getJobs(setLoading);
    props.getUserData(user.uid, setLoading);
  }, []);

  useEffect(() => {
    (async () => {
      if (props.get_jobs != null) {
        await props.get_jobs?.map((item, index) => {
          if (
            item?.status != "completed" &&
            item?.status != "deactivated" &&
            !item?.hiredSomeone
          ) {
            setFilteredJobs(props?.get_jobs);
          }
        });

        // setJobs(props.get_jobs);
      }
    })();
  }, [props]);

  const onRemove = (id) => {
    let arr = filteredJobs;
    let remainingArr = arr.filter(data => data.id != id);
    setFilteredJobs(remainingArr)
  };

  if (loading) {
    return (
      <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
        <ActivityIndicator color="black" size="large" />
      </View>
    );
  }
  const AdView = (props) => (
    <View
      style={{
        backgroundColor: "#fff",
        width: "100%",
        paddingVertical: 10,
        marginTop: 10,
        borderRadius: 20,
        paddingLeft: 15,
        paddingRight: 20,
      }}
    >
      <View style={{ flexDirection: "row", alignItems: "center" }}>
        <Image
          style={{ width: 40, height: 40, borderRadius: 13 }}
          source={
            props?.data.authorProfile != null
              ? { uri: props?.data.authorProfile }
              : require("../../assets/icon1.png")
          }
        />
        <Text
          style={{
            marginLeft: 10,
            color: "#6C6C6C",
            fontFamily: "RBo",
            fontSize: 14,
          }}
        >
          {props?.data?.author}
        </Text>
        <Text
          style={{
            position: "absolute",
            right: 0,
            fontFamily: "RRe",
            fontSize: 10,
            color: "#112D4E",
          }}
        >
          {props?.date == 1
            ? props?.date + " day ago"
            : props?.date == 0
            ? "Today"
            : props?.date + " days ago"}
        </Text>
      </View>

      <View style={{ flexDirection: "row", width: "100%", marginTop: 10 }}>
        <View
          style={{
            width: 5,
            height: "100%",
            borderRadius: 20,
            backgroundColor: "#EF4136",
          }}
        ></View>
        <View style={{ marginLeft: 10 }}>
          <Text style={{ color: "#EF4136", fontFamily: "RBo", fontSize: 14 }}>
            {props?.data?.category}
          </Text>
          <Text
            style={{
              color: "#707070",
              fontFamily: "RRe",
              fontSize: 11,
              lineHeight: 15,
            }}
          >
            {props?.data?.desc}
          </Text>
          <View
            style={{
              flexDirection: "row",
              justifyContent: "space-between",
              marginTop: 25,
            }}
          >
            <Text style={{ color: "#6C6C6C", fontFamily: "RBo", fontSize: 12 }}>
              Budget
            </Text>
            <Text style={{ color: "#FBB040", fontFamily: "RBo", fontSize: 12 }}>
              ${props?.data?.budget}
            </Text>
          </View>
          <View
            style={{
              flexDirection: "row",
              justifyContent: "space-between",
              marginTop: 15,
            }}
          >
            <Text style={{ color: "#6C6C6C", fontFamily: "RBo", fontSize: 12 }}>
              Duration
            </Text>
            <Text style={{ color: "#FBB040", fontFamily: "RBo", fontSize: 12 }}>
              {props?.data?.duration}
            </Text>
          </View>
          <View
            style={{
              flexDirection: "row",
              justifyContent: "space-between",
              marginTop: 15,
            }}
          >
            <Text
              style={{
                color: "#6C6C6C",
                fontFamily: "RBo",
                fontSize: 12,
                textAlign: "left",
                width: "50%",
              }}
            >
              Location
            </Text>
            <Text
              style={{
                color: "#FBB040",
                fontFamily: "RBo",
                fontSize: 12,
                textAlign: "right",
                width: "50%",
              }}
            >
              {props?.data?.location || "none"}
            </Text>
          </View>
        </View>
      </View>

      <View
        style={{
          flexDirection: "row",
          justifyContent: "space-between",
          width: "103%",
          marginTop: 40,
        }}
      >
        <TouchableOpacity
          onPress={() => onRemove(props?.id)}
          style={{
            width: "48%",
            height: 40,
            backgroundColor: "#112D4E",
            borderRadius: 10,
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <Text style={{ fontFamily: "RBo", color: "#FFFFFF", fontSize: 12 }}>
            R E M O V E
          </Text>
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() =>
            props?.data?.hiredId == user.uid && props?.data?.status == "onGoing"
              ? props.navigation.navigate("HHomeStack", {
                  screen: "HNotification",
                  params: { data: { jobDetails: props?.data } },
                })
              : props?.data?.hiredId == user.uid &&
                props?.data?.status == "half-completed"
              ? props.navigation.navigate("HHomeStack", {
                  screen: "HNotification",
                  params: { data: { jobDetails: props?.data } },
                })
              : props?.data?.applicants.includes(user.email)
              ? alert("Already applied to this job")
              : props.navigation.navigate("HHomeStack", {
                  screen: "SubmitQuote",
                  params: { data: props?.data },
                })
          }
          style={{ width: "48%", height: 40 }}
        >
          <LinearGradient
            style={{
              flex: 1,
              borderRadius: 10,
              alignItems: "center",
              justifyContent: "center",
            }}
            colors={[AppColors.linear1, AppColors.linear2]}
          >
            <Text style={{ fontFamily: "RBo", color: "#FFFFFF", fontSize: 12 }}>
              {props?.data?.hiredId == user.uid &&
              props?.data?.status == "onGoing"
                ? "you are hired"
                : props?.data?.hiredId == user.uid &&
                  props?.data?.status == "half-completed"
                ? "Waiting your review"
                : props?.data?.applicants.includes(user.email)
                ? "Alread Applied"
                : props?.data?.hiredSomeone
                ? "Hired somone else"
                : "Q U O T E"}
            </Text>
          </LinearGradient>
        </TouchableOpacity>
      </View>
    </View>
  );
  return (
    <View style={{ flex: 1, backgroundColor: "#F5F5F5", borderRadius: 40 }}>
      <StatusBar hidden={true} />
      <View
        style={{
          marginTop: "10%",
          paddingBottom: 10,
          alignItems: "center",
          width: "80%",
          alignSelf: "center",
          flexDirection: "row",
          justifyContent: "space-between",
        }}
      >
        <Image style={{}} source={require("../../assets/logo.png")} />
      </View>
      <ScrollView
        refreshControl={
          <RefreshControl
            refreshing={refreshing}
            onRefresh={onRefresh}
            tintColor="#EF4136"
          />
        }
        contentContainerStyle={{ paddingBottom: 100 }}
        showsVerticalScrollIndicator={false}
      >
        <View style={{ width: "80%", alignSelf: "center" }}>
          {filteredJobs?.length < 1 ? (
            <View
              style={{
                flex: 1,
                paddingVertical: 15,
                borderRadius: 20,
                paddingHorizontal: 10,
                alignItems: "center",
                marginTop: 10,
              }}
            >
              <Entypo name="emoji-sad" size={RFValue(30)} color="black" />
              <Text
                style={{
                  marginVertical: 15,
                  fontSize: RFValue(17),
                  fontWeight: "600",
                  textAlign: "center",
                  width: "100%",
                }}
              >
                No recent projects
              </Text>
            </View>
          ) : (
            <>
              {filteredJobs?.map((item, index) => {
                var firebaseDate = new Date(item?.date.seconds * 1000);
                var d =
                  eval(firebaseDate.getMonth() + 1) +
                  "/" +
                  firebaseDate.getDate() +
                  "/" +
                  firebaseDate.getFullYear();
                var date1 = new Date(d);
                var date2 = new Date();
                var dif = date2.getTime() - date1.getTime();
                var difInDays = dif / (1000 * 3600 * 24);

                if (
                  item?.status != "completed" &&
                  item?.status != "deactivated" &&
                  !item?.hiredSomeone
                ) {
                  return (
                    <AdView
                      navigation={props.navigation}
                      date={parseInt(difInDays)}
                      data={item}
                      key={index}
                      id={item?.id}
                    />
                  );
                }
              })}
            </>
          )}
        </View>
      </ScrollView>
    </View>
  );
};
const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  get_jobs: state.main.get_jobs,
});
export default connect(mapStateToProps, { getJobs, getUserData })(HHome);

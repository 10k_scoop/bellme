import MaskedView from "@react-native-community/masked-view";
import { LinearGradient } from "expo-linear-gradient";
import React, { useState, useEffect } from "react";
import {
  Share,
  View,
  TouchableOpacity,
  Text,
  StyleSheet,
  Image,
  ScrollView,
  ActivityIndicator,
  TextInput,
  KeyboardAvoidingView,
} from "react-native";
import AppColors from "../../Components/AppColors";
import {
  ArrowLeft,
  EditIcon,
  StarIcon,
  WhiteThreeDots,
} from "../../Components/SvgComponets";
import { connect } from "react-redux";
import {
  getUserData,
  providerJobs,
  profileUpdate,
  uploadPortfolio,
} from "../../state-management/actions/features/Features";
import * as firebase from "firebase";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import { FontAwesome5, AntDesign } from "@expo/vector-icons";
import * as ImagePicker from "expo-image-picker";
import { MaterialIcons } from "@expo/vector-icons";
const HProfile = (props) => {
  const [loading, setLoading] = useState(true);
  const [editProfile, setEditProfile] = useState(false);
  const [data, setData] = useState([]);
  const user = firebase.auth().currentUser;
  const [jobs, setJobs] = useState([]);
  const [address, setAddress] = useState("");
  const [bio, setBio] = useState("");
  const [number, setNumber] = useState("");
  const [portfolioLoading, setPortfolioLoading] = useState(false);
  const [portfolioImageLoading, setPortfolioImageLoading] = useState(true);
  const [username, setUsername] = useState("");
  const [portfolioImage, setPortfolioImage] = useState("");
  let arr = [0, 0, 0, 0, 0];
  useEffect(() => {
    props.getUserData(user?.uid, setLoading);
    props.providerJobs(user?.email, setLoading);
  }, []);

  const images = [
    require("../../assets/img1.png"),
    require("../../assets/img2.png"),
    require("../../assets/img3.png"),
  ];

  const [activeTab, setActiveTab] = useState({
    info: true,
    reviews: false,
    projects: false,
  });

  useEffect(() => {
    if (props.get_user_details != null) {
      setData(props.get_user_details);
      setAddress(data?.address);
      setNumber(data?.phone);
      setUsername(data?.username);
      setBio(data?.bio);
    }
    if (props.my_jobs != null) {
      setJobs(props.my_jobs);
    }
  }, [props]);

  const onProfileUpdate = async () => {
    setLoading(true);
    let data = {
      address: address,
      phone: number,
      bio: bio,
      username: username,
    };
    await props?.profileUpdate(
      data,
      user?.uid,
      setLoading,
      props?.navigation,
      "HProfile"
    );
  };

  if (loading) {
    return (
      <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
        <ActivityIndicator color="black" size="large" />
      </View>
    );
  }

  const pickImage = async () => {
    // No permissions request is necessary for launching the image library
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [4, 3],
      quality: 1,
    });

    if (!result.cancelled) {
      setPortfolioLoading(true);
      setPortfolioImage(result.uri);
      let data = {
        email: user?.email,
        id: user?.uid,
        image: result.uri,
      };
      props?.uploadPortfolio(data, setLoading, setPortfolioLoading);
    }
  };

  const onShare = async () => {
    try {
      const result = await Share.share({
        message: `Hey! Looking for service providers.${"\n"}${"\n"}Download me now!`,
      });
      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // shared with activity type of result.activityType
        } else {
          // shared
        }
      } else if (result.action === Share.dismissedAction) {
        // dismissed
      }
    } catch (error) {
      alert(error.message);
    }
  };
  console.log(data?.verified);

  return (
    <View style={{ flex: 1, backgroundColor: "#F5F5F5" }}>
      <KeyboardAvoidingView
        behavior={Platform.OS === "ios" ? "padding" : "height"}
      >
        <ScrollView>
          <View style={{ flex: 1, paddingBottom: hp("5%") }}>
            <View
              style={{ width: "100%", height: 250, backgroundColor: "#F5F5F5" }}
            >
              <Image
                onLoadEnd={() => setPortfolioImageLoading(false)}
                style={{
                  width: "100%",
                  backgroundColor: "#F5F5F5",
                  borderRadius: 40,
                  height: "100%",
                }}
                source={
                  portfolioImage.length > 0
                    ? { uri: portfolioImage }
                    : data?.portfolio?.length > 0
                    ? { uri: data?.portfolio }
                    : images[0]
                }
              />
            </View>
            <View
              style={{
                position: "absolute",
                top: 40,
                flexDirection: "row",
                justifyContent: "space-between",
                width: "80%",
                alignSelf: "center",
              }}
            >
              <TouchableOpacity onPress={() => props.navigation.goBack()}>
                {/* <ArrowLeft/> */}
              </TouchableOpacity>
              <TouchableOpacity>
                <WhiteThreeDots />
              </TouchableOpacity>
            </View>
            {portfolioLoading || portfolioImageLoading ? (
              <ActivityIndicator
                size="large"
                color="black"
                style={{
                  position: "absolute",
                  top: 100,
                  alignSelf: "center",
                  height: 30,
                  marginTop: 20,
                  backgroundColor: "white",
                  padding: 20,
                  paddingVertical: 30,
                  borderRadius: 10,
                }}
              />
            ) : (
              <TouchableOpacity
                onPress={() => pickImage()}
                style={{
                  position: "absolute",
                  top: 100,
                  alignSelf: "center",
                  height: 30,
                  marginTop: 20,
                }}
              >
                <LinearGradient
                  style={{
                    flex: 1,
                    borderRadius: 10,
                    paddingHorizontal: 20,
                    alignItems: "center",
                    justifyContent: "center",
                  }}
                  colors={["rgba(251, 176, 64,0.6)", "rgba(239,65,54,0.6)"]}
                >
                  <Text
                    style={{ color: "white", fontFamily: "RBo", fontSize: 10 }}
                  >
                    UPLOAD PORTFOLIO
                  </Text>
                </LinearGradient>
              </TouchableOpacity>
            )}

            <View
              style={{
                width: "80%",
                alignSelf: "center",
                marginTop: 20,
                flex: 1,
              }}
            >
              <View
                style={{
                  flexDirection: "row",
                  width: "100%",
                  justifyContent: "space-between",
                }}
              >
                <Text
                  style={{ color: "#707070", fontSize: 14, fontFamily: "RRe" }}
                >
                  {jobs?.length} Projects Completed
                </Text>
                <View style={{ flexDirection: "row" }}>
                  {data?.rating?.map((item, index) => {
                    if (item?.stars == 5) {
                      arr[0] = arr[0] + 1;
                    } else if (item?.stars == 4) {
                      arr[1] = arr[1] + 1;
                    } else if (item?.stars == 3) {
                      arr[2] = arr[2] + 1;
                    } else if (item?.stars == 2) {
                      arr[3] = arr[3] + 1;
                    } else if (item?.stars == 1) {
                      arr[4] = arr[4] + 1;
                    } else {
                    }
                    let cal =
                      (5 * arr[0] +
                        4 * arr[1] +
                        3 * arr[2] +
                        2 * arr[3] +
                        1 * arr[4]) /
                      (arr[0] + arr[1] + arr[2] + arr[3] + arr[4]);
                    // console.log(cal)
                    if (index > 0) {
                      return (
                        <View key={index} style={{ flexDirection: "row" }}>
                          <StarIcon active={cal >= 1 ? true : false} />
                          <StarIcon
                            style={{ marginLeft: 3 }}
                            active={cal >= 2 ? true : false}
                          />
                          <StarIcon
                            style={{ marginLeft: 3 }}
                            active={cal >= 3 ? true : false}
                          />
                          <StarIcon
                            style={{ marginLeft: 3 }}
                            active={cal >= 4 ? true : false}
                          />
                          <StarIcon
                            style={{ marginLeft: 3 }}
                            active={cal >= 5 ? true : false}
                          />
                        </View>
                      );
                    }
                  })}
                </View>
              </View>
              <Text
                style={{
                  color: "#707070",
                  fontSize: 14,
                  marginTop: 10,
                  fontWeight: "bold",
                }}
              >
                Total earning : ${data?.earnings || 0}
              </Text>
              <View
                style={{
                  marginTop: 25,
                  flexDirection: "row",
                  width: "100%",
                  height: 30,
                  justifyContent: "space-between",
                  alignItems: "center",
                }}
              >
                {/* <MaskedView
                  style={{ width: "100%", height: "100%" }}
                  maskElement={
                    <View
                      style={{ flexDirection: "row", alignItems: "center" }}
                    >
                      <Text
                        style={{
                          fontSize: 25,
                          fontFamily: "RBo",
                          marginRight: 6,
                        }}
                      >
                        {data?.name}
                      </Text>
                      {data?.verified && (
                        <MaterialIcons
                          name="verified-user"
                          size={23}
                          color="black"
                        />
                      )}
                    </View>
                  }
                >
                  <LinearGradient
                    colors={[AppColors.linear1, AppColors.linear2]}
                    start={{ x: 1, y: 0 }}
                    end={{ x: 1, y: 1 }}
                    style={{ flex: 1 }}
                  />
                </MaskedView> */}
                <View style={{ flexDirection: "row", alignItems: "center" }}>
                  <Text
                    style={{
                      fontSize: 25,
                      fontFamily: "RBo",
                      marginRight: 6,
                      color: "#FBB040",
                    }}
                  >
                    {data?.name}
                  </Text>
                  {data?.verified == true && (
                    <MaterialIcons
                      name="verified-user"
                      size={23}
                      color="#FBB040"
                    />
                  )}
                </View>
                <TouchableOpacity style={{}}>
                  <EditIcon />
                </TouchableOpacity>
              </View>
              <Text
                style={{
                  fontFamily: "RRe",
                  fontSize: 12,
                  color: "#707070",
                  marginTop: 10,
                }}
              >
                {data?.bio ||
                  " Hi, I am Selena, your Architect. I have specialized in House design and interior decoration. Swipe my profile to see my past project andfeel free to reach out to me if you have any questions. Thanks"}
              </Text>
              <View
                style={{
                  width: "100%",
                  paddingHorizontal: 2,
                  height: 30,
                  flexDirection: "row",
                  backgroundColor: "#E4E4E4",
                  borderRadius: 13,
                  marginTop: 25,
                }}
              >
                <TouchableOpacity
                  onPress={() => {
                    setEditProfile(false);
                    setActiveTab({
                      ...activeTab,
                      info: true,
                      reviews: false,
                      projects: false,
                    });
                  }}
                  style={{ width: "33.33%" }}
                >
                  {activeTab.info ? (
                    <LinearGradient
                      style={{
                        flex: 1,
                        borderRadius: 11,
                        alignItems: "center",
                        justifyContent: "center",
                      }}
                      colors={[AppColors.linear1, AppColors.linear2]}
                    >
                      <Text
                        style={
                          activeTab.info
                            ? styles.activeText
                            : styles.inActiveText
                        }
                      >
                        Info
                      </Text>
                    </LinearGradient>
                  ) : (
                    <View style={styles.inActiveTab}>
                      <Text style={styles.inActiveText}>Info</Text>
                    </View>
                  )}
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => {
                    setEditProfile(false);
                    setActiveTab({
                      ...activeTab,
                      info: false,
                      reviews: true,
                      projects: false,
                    });
                  }}
                  style={{ width: "33.33%" }}
                >
                  {activeTab.reviews ? (
                    <LinearGradient
                      style={{
                        flex: 1,
                        borderRadius: 11,
                        alignItems: "center",
                        justifyContent: "center",
                      }}
                      colors={[AppColors.linear1, AppColors.linear2]}
                    >
                      <Text style={styles.activeText}>Reviews</Text>
                    </LinearGradient>
                  ) : (
                    <View style={styles.inActiveTab}>
                      <Text style={styles.inActiveText}>Reviews</Text>
                    </View>
                  )}
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => {
                    setEditProfile(false);
                    setActiveTab({
                      ...activeTab,
                      info: false,
                      reviews: false,
                      projects: true,
                    });
                  }}
                  style={{ width: "33.33%" }}
                >
                  {activeTab.projects ? (
                    <LinearGradient
                      style={{
                        flex: 1,
                        borderRadius: 11,
                        alignItems: "center",
                        justifyContent: "center",
                      }}
                      colors={[AppColors.linear1, AppColors.linear2]}
                    >
                      <Text style={styles.activeText}>Projects</Text>
                    </LinearGradient>
                  ) : (
                    <View style={styles.inActiveTab}>
                      <Text style={styles.inActiveText}>Projects</Text>
                    </View>
                  )}
                </TouchableOpacity>
              </View>
              {editProfile && (
                <View style={styles.infoFormWrapper}>
                  <View style={styles.inputWrapper}>
                    <TextInput
                      placeholder="Address"
                      style={styles.inputField}
                      placeholderTextColor="#222"
                      onChangeText={(val) => setAddress(val)}
                      value={address}
                    />
                  </View>
                  <View style={styles.inputWrapper}>
                    <TextInput
                      placeholder="Phone Number"
                      style={styles.inputField}
                      placeholderTextColor="#222"
                      onChangeText={(val) => setNumber(val)}
                      keyboardType="number-pad"
                      value={number}
                    />
                  </View>
                  <View style={styles.inputWrapper}>
                    <TextInput
                      placeholder="Username"
                      style={styles.inputField}
                      placeholderTextColor="#222"
                      onChangeText={(val) => setUsername(val)}
                      value={username}
                    />
                  </View>
                  <View
                    style={[
                      styles.inputWrapper,
                      { minHeight: hp("6%"), padding: 5 },
                    ]}
                  >
                    <TextInput
                      placeholder="Bio"
                      style={styles.inputField}
                      multiline
                      placeholderTextColor="#222"
                      value={bio}
                      onChangeText={(val) => setBio(val)}
                    />
                  </View>
                </View>
              )}
              {!editProfile && (
                <>
                  {activeTab.info && (
                    <>
                      <TouchableOpacity
                        style={[
                          styles.textInputContainer,
                          {
                            backgroundColor: "#FFFFFF",
                            justifyContent: "space-between",
                            paddingRight: 15,
                          },
                        ]}
                      >
                        <Text style={styles.text}>Address</Text>
                        <Text style={[styles.text, { color: "#FBB040" }]}>
                          {data?.address}
                        </Text>
                      </TouchableOpacity>
                      <TouchableOpacity
                        style={[
                          styles.textInputContainer,
                          {
                            backgroundColor: "#FFFFFF",
                            justifyContent: "space-between",
                            paddingRight: 15,
                          },
                        ]}
                      >
                        <Text style={styles.text}>Phone Number</Text>
                        <Text style={[styles.text, { color: "#FBB040" }]}>
                          {data?.phone}
                        </Text>
                      </TouchableOpacity>
                      <TouchableOpacity
                        style={[
                          styles.textInputContainer,
                          {
                            backgroundColor: "#FFFFFF",
                            justifyContent: "space-between",
                            paddingRight: 15,
                          },
                        ]}
                      >
                        <Text style={styles.text}>username</Text>
                        <Text style={[styles.text, { color: "#FBB040" }]}>
                          {data?.username}
                        </Text>
                      </TouchableOpacity>
                    </>
                  )}

                  {activeTab.projects && (
                    <View style={styles.ProjectsView}>
                      <Text style={styles.ProjectsViewText}>Completed</Text>
                      <Text
                        style={[
                          styles.ProjectsViewText,
                          { right: 10, fontWeight: "600" },
                        ]}
                      >
                        {" "}
                        {jobs?.length}
                      </Text>
                    </View>
                  )}
                  {activeTab.reviews && (
                    <>
                      {data?.rating?.length < 1 && (
                        <View
                          style={[
                            styles.reviewBox,
                            {
                              justifyContent: "center",
                              backgroundColor: "transparent",
                            },
                          ]}
                        >
                          <Text
                            style={{ textAlign: "center", fontSize: rf(18) }}
                          >
                            0 reviews
                          </Text>
                        </View>
                      )}
                    </>
                  )}
                  {activeTab.reviews &&
                    data?.rating?.map((item, index) => {
                      if (index > 2) return;
                      return (
                        <View style={styles.reviewBox} key={index}>
                          <View style={styles.reviewBoxRow1}>
                            <View
                              style={{
                                flexDirection: "row",
                                alignItems: "center",
                              }}
                            >
                              <View style={styles.reviewBoxProfile}>
                                <Image
                                  source={images[0]}
                                  style={{ width: "100%", height: "100%" }}
                                />
                              </View>
                              <Text style={styles.reviewBoxEmail}>
                                {item?.author.slice(
                                  0,
                                  item?.author.indexOf("@")
                                )}
                              </Text>
                            </View>
                            <View style={styles.reviewStars}>
                              <AntDesign
                                name="star"
                                size={rf(15)}
                                color={item?.stars >= 1 ? "#ffd800" : "grey"}
                              />
                              <AntDesign
                                name="star"
                                size={rf(15)}
                                color={item?.stars >= 2 ? "#ffd800" : "grey"}
                              />
                              <AntDesign
                                name="star"
                                size={rf(15)}
                                color={item?.stars >= 3 ? "#ffd800" : "grey"}
                              />
                              <AntDesign
                                name="star"
                                size={rf(15)}
                                color={item?.stars >= 4 ? "#ffd800" : "grey"}
                              />
                              <AntDesign
                                name="star"
                                size={rf(15)}
                                color={item?.stars >= 5 ? "#ffd800" : "grey"}
                              />
                            </View>
                          </View>
                          <Text style={styles.reviewBoxFeedback}>
                            {item?.feedback}
                          </Text>
                        </View>
                      );
                    })}
                </>
              )}
              <TouchableOpacity
                onPress={() => {
                  editProfile
                    ? onProfileUpdate()
                    : setEditProfile(!editProfile);
                }}
                style={{ width: "100%", height: 40, marginTop: 30 }}
              >
                <LinearGradient
                  style={{
                    flex: 1,
                    borderRadius: 13,
                    alignItems: "center",
                    justifyContent: "center",
                  }}
                  colors={[AppColors.linear1, AppColors.linear2]}
                >
                  <Text
                    style={{ color: "white", fontFamily: "RBo", fontSize: 14 }}
                  >
                    {editProfile ? "U P D A T E" : "E D I T P R O F I L E"}
                  </Text>
                </LinearGradient>
              </TouchableOpacity>
            </View>
            <View style={styles.Referal}>
              <TouchableOpacity
                onPress={onShare}
                style={{ width: "100%", height: 40, marginTop: 35 }}
              >
                <LinearGradient
                  style={{
                    flex: 1,
                    borderRadius: 13,
                    alignItems: "center",
                    justifyContent: "center",
                  }}
                  colors={[AppColors.linear1, AppColors.linear2]}
                >
                  <Text
                    style={{ color: "white", fontFamily: "RBo", fontSize: 14 }}
                  >
                    COPY R E F E R A L
                  </Text>
                </LinearGradient>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
      </KeyboardAvoidingView>
    </View>
  );
};
const styles = StyleSheet.create({
  reviewBox: {
    width: "100%",
    paddingHorizontal: 10,
    backgroundColor: "#fff",
    borderRadius: 10,
    marginTop: 12,
    minHeight: hp("10%"),
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,

    elevation: 2,
    paddingVertical: 10,
  },
  reviewBoxFeedback: {
    fontSize: rf(11),
    fontWeight: "300",
    marginVertical: 10,
    width: "90%",
    paddingHorizontal: "3%",
  },
  reviewBoxRow1: {
    flexDirection: "row",
    alignItems: "center",
    width: "100%",
    justifyContent: "space-between",
  },
  reviewBoxProfile: {
    width: wp("10%"),
    height: wp("10%"),
    borderRadius: 100,
    alignItems: "center",
    justifyContent: "center",
    overflow: "hidden",
  },
  reviewBoxEmail: {
    fontSize: rf(13),
    fontWeight: "500",
    marginLeft: 10,
  },
  reviewStars: {
    flexDirection: "row",
  },
  activeTab: {
    backgroundColor: "#FFFFFF",

    width: "50%",
    height: 36,
    alignSelf: "center",
    // justifyContent: 'center',
    // alignItems: 'center'
  },
  ProjectsView: {
    width: "100%",
    alignItems: "center",
    justifyContent: "space-between",
    height: hp("5%"),
    backgroundColor: "#fff",
    marginTop: 10,
    borderRadius: 10,
    flexDirection: "row",
    padding: 10,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,

    elevation: 2,
  },
  ProjectsViewText: {
    fontSize: rf(13),
    fontWeight: "500",
  },
  inActiveTab: {
    // backgroundColor:'#FFFFFF',
    // borderRadius:11,
    // width: "33%",
    flex: 1,
    // height:36,
    alignSelf: "center",
    justifyContent: "center",
    alignItems: "center",
  },
  activeText: {
    color: "#fff",
    fontFamily: "RBo",
    fontSize: 12,
  },
  inActiveText: {
    color: "#707070",
    fontFamily: "RRe",
    fontSize: 12,
  },
  textInputContainer: {
    flexDirection: "row",
    backgroundColor: "#E4E4E4",
    borderRadius: 13,
    height: 40,
    width: "100%",
    paddingLeft: 15,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 20,
  },
  text: {
    fontFamily: "RRe",
    fontSize: 12,
    color: "#707070",
  },
  infoFormWrapper: {
    marginTop: 1,
    padding: 10,
  },
  inputWrapper: {
    backgroundColor: "#fff",
    width: "100%",
    height: hp("4%"),
    borderRadius: 10,
    paddingHorizontal: 10,
    marginVertical: 10,
    justifyContent: "center",
  },
  inputField: {
    width: "100%",
    height: "100%",
    fontSize: rf(10),
    color: "#222",
  },
  Referal: {
    width: wp("90%"),
    paddingHorizontal: wp("4%"),
    justifyContent: "center",
    alignItems: "center",
    justifyContent: "flex-end",
    marginHorizontal: wp("5%"),
  },
});

const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  my_jobs: state.main.my_jobs,
  get_user_details: state.main.get_user_details,
});
export default connect(mapStateToProps, {
  providerJobs,
  profileUpdate,
  getUserData,
  uploadPortfolio,
})(HProfile);

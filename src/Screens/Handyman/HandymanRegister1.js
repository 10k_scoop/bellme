import { LinearGradient } from "expo-linear-gradient";
import { StatusBar } from "expo-status-bar";
import React, { useEffect, useState } from "react";
import {
  SafeAreaView,
  View,
  TouchableOpacity,
  Text,
  ScrollView,
  StyleSheet,
  TextInput,
  UIManager,
  findNodeHandle,
  Dimensions,
  FlatList,
  ActivityIndicator,
} from "react-native";
import AppColors from "../../Components/AppColors";
import {
  LogoSvg,
  PhoneIcon,
  AddressIcon,
  PaperClipIcon,
} from "../../Components/SvgComponets";
import DropDown from "../../Components/DropDown";
import { connect } from "react-redux";
import { signup } from "../../state-management/actions/auth/AuthActions";
import * as ImagePicker from "expo-image-picker";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import {
  ArrowDown,
  ArrowLeft,
  CameraGradientIcon,
  SwipeRedRight,
  CleaningIcon,
  PlumbingIcon,
  WasteIcon,
} from "../../Components/SvgComponets";
import { subCategories } from "../../Components/SubCategories";
import { AntDesign } from "@expo/vector-icons";

const HandymanRegister1 = (props) => {
  const [showSubCategory, setShowSubCategory] = useState(false);
  const [showCategory, setShowCategory] = useState(false);
  const [verifiedProviderCheck, setVerifiedProviderCheck] = useState(false);
  const [position, setPosition] = useState({});
  const [button, setButton] = useState();
  const [loading, setLoading] = useState(false);
  const [subCategory, setSubCategory] = useState(null);
  const [skillCategory, setSkillCategory] = useState(null);
  const [hRate, setHRate] = useState(null);
  const [phone, setPhone] = useState(null);
  const [address, setAddress] = useState(null);
  const [businessNameImg, setBusinessNameImg] = useState(null);
  const [guranteeForm, setGuranteeForm] = useState(null);
  const [addressImg, setAddressImg] = useState(null);
  const [policeClearanceImg, setPoliceClearanceImg] = useState(null);

  const categoriesArray = [
    {
      icon: CleaningIcon,
      name: "Construction",
    },
    {
      icon: PlumbingIcon,
      name: "Carpentry",
    },
    {
      icon: WasteIcon,
      name: "Waste Mgt",
    },
    {
      icon: CleaningIcon,
      name: "Plumbing",
    },
    {
      icon: PlumbingIcon,
      name: "Tutor",
    },
    {
      icon: WasteIcon,
      name: "Services",
    },
    {
      icon: CleaningIcon,
      name: "Mechanics",
    },
    {
      icon: PlumbingIcon,
      name: "Electrical",
    },
    {
      icon: WasteIcon,
      name: "House Maintenance",
    },
    {
      icon: WasteIcon,
      name: "Events",
    },
  ];

  const onSubmit = () => {
    var prevData = props.route.params.data;
    let data = {
      name: prevData.name,
      email: prevData.email.toLowerCase(),
      pass: prevData.pass,
      skillCategory: skillCategory,
      phone: phone,
      address: address,
      subCategory: subCategory,
      hRate: hRate,
      businessNameImg: businessNameImg,
      guranteeForm: guranteeForm,
      addressImg: addressImg,
      policeClearanceImg: policeClearanceImg,
      role: "provider",
      rating: [],
      earnings: 0,
      verified: false,
      verifiedProviderCheck: verifiedProviderCheck,
    };

    if (
      skillCategory == null ||
      subCategory == null ||
      hRate == null ||
      phone == null
    ) {
      alert("Fill all details");
    } else {
      if (verifiedProviderCheck) {
        if (
          businessNameImg == null ||
          guranteeForm == null ||
          addressImg == null ||
          policeClearanceImg == null
        ) {
          alert("Provide documents");
        } else {
          setLoading(true);
          props.signup(data, setLoading);
        }
      } else {
        setLoading(true);
        props.signup(data, setLoading);
      }
    }
  };

  useEffect(() => {
    (async () => {
      if (Platform.OS !== "web") {
        const {
          status,
        } = await ImagePicker.requestMediaLibraryPermissionsAsync();
        if (status !== "granted") {
          //alert("Sorry, we need camera roll permissions to make this work!");
        }
      }
    })();
  }, []);

  const pickImage = async (type) => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [4, 3],
      quality: 0,
    });
    console.log(type);
    if (!result.cancelled) {
      type == "BNR"
        ? setBusinessNameImg(result?.uri)
        : type == "AGF"
        ? setGuranteeForm(result?.uri)
        : type == "AA"
        ? setAddressImg(result?.uri)
        : type == "PC"
        ? setPoliceClearanceImg(result?.uri)
        : null;
    }
  };

  if (loading) {
    return (
      <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
        <ActivityIndicator size="large" color="orange" />
      </View>
    );
  }

  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: "#F5F5F5" }}>
      <StatusBar hidden={true} />
      <View
        style={{
          marginTop: Platform.OS == "android" ? "15%" : "10%",
          width: "90%",
          alignItems: "center",
        }}
      >
        <View style={{ flexDirection: "row" }}>
          <TouchableOpacity
            onPress={() => {
              props.navigation.goBack();
            }}
            style={{ marginRight: 10 }}
          >
            <ArrowLeft />
          </TouchableOpacity>
          <LogoSvg />
        </View>
      </View>
      <Text
        style={{
          width: "85%",
          lineHeight: 19,
          textAlign: "center",
          alignSelf: "center",
          fontFamily: "RRe",
          fontSize: 14,
          color: "#707070",
          marginTop: 20,
          alignSelf: "center",
        }}
      >
        Tell us a little more about yourself to find jobs based on your skills
      </Text>

      <ScrollView contentContainerStyle={{ paddingBottom: 80 }}>
        <View style={{ width: "85%", alignSelf: "center" }}>
          <TouchableOpacity
            style={[
              styles.textInputContainer,
              {
                backgroundColor: "white",
                justifyContent: "flex-end",
                paddingRight: 10,
              },
            ]}
            onPress={() => setShowCategory(!showCategory)}
          >
            <Text
              style={{
                position: "absolute",
                left: 20,
                fontSize: 14,
                color: "#707070",
              }}
            >
              {categoriesArray[skillCategory]?.name || "Skill Category"}
            </Text>
            <Text
              style={{ color: "#EF4136", fontFamily: "RRe", marginRight: 15 }}
            >
              Skill
            </Text>
            <ArrowDown />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              skillCategory
                ? setShowSubCategory(true)
                : alert("Select category first");
            }}
            style={[
              styles.textInputContainer,
              {
                backgroundColor: "white",
                justifyContent: "flex-end",
                paddingRight: 10,
              },
            ]}
          >
            <Text
              style={{
                position: "absolute",
                left: 20,
                fontSize: 14,
                color: "#707070",
              }}
            >
              {subCategory || "Sub-category"}
            </Text>
            <Text
              style={{ color: "#EF4136", fontFamily: "RRe", marginRight: 15 }}
            >
              sub-category
            </Text>
            <ArrowDown />
          </TouchableOpacity>

          <TouchableOpacity
            style={[
              styles.textInputContainer,
              {
                backgroundColor: "white",
                justifyContent: "flex-end",
                paddingRight: 10,
                marginTop: 20,
              },
            ]}
          >
            <Text
              style={{
                position: "absolute",
                left: 20,
                fontSize: 14,
                color: "#707070",
              }}
            >
              Hourly Rate
            </Text>
            <TextInput
              placeholderTextColor="#EF4136"
              placeholder="$00.00"
              value={hRate}
              onChangeText={(val) => setHRate(val)}
              style={{ color: "#EF4136", fontFamily: "RRe", marginRight: 15 }}
            />
          </TouchableOpacity>

          <View style={[styles.textInputContainer, { marginTop: 35 }]}>
            <View style={{ position: "absolute", left: 15 }}>
              <PhoneIcon />
            </View>
            <TextInput
              placeholder="Phone Number"
              placeholderTextColor="#707070"
              style={styles.textInput}
              onChangeText={(val) => setPhone(val)}
              keyboardType="number-pad"
            />
          </View>
          <View style={[styles.textInputContainer]}>
            <View style={{ position: "absolute", left: 15 }}>
              <AddressIcon />
            </View>
            <TextInput
              placeholder="Address"
              placeholderTextColor="#707070"
              style={styles.textInput}
              onChangeText={(val) => setAddress(val)}
            />
          </View>
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              marginTop: 20,
            }}
          >
            <TouchableOpacity
              style={[styles.radioBtn]}
              onPress={() => setVerifiedProviderCheck(!verifiedProviderCheck)}
            >
              {verifiedProviderCheck && (
                <AntDesign name="check" size={15} color="black" />
              )}
            </TouchableOpacity>
            <Text
              style={{
                color: "#707070",
                fontSize: 12,
                fontFamily: "RRe",
                marginLeft: 13,
              }}
            >
              Register as verified Service Proivider
            </Text>
          </View>

          <TouchableOpacity
            onPress={() => pickImage("BNR")}
            style={[
              styles.textInputContainer,
              {
                backgroundColor: "#FFFFFF",
                justifyContent: "space-between",
                paddingRight: 15,
              },
            ]}
          >
            <Text style={styles.textInput}>Business name Registration</Text>
            {businessNameImg ? (
              <Text style={{ color: "#222", fontSize: 15 }}>1</Text>
            ) : (
              <PaperClipIcon />
            )}
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => pickImage("AGF")}
            style={[
              styles.textInputContainer,
              {
                backgroundColor: "#FFFFFF",
                justifyContent: "space-between",
                paddingRight: 15,
              },
            ]}
          >
            <Text style={styles.textInput}>Attach Guarantee Form</Text>
            {guranteeForm ? (
              <Text style={{ color: "#222", fontSize: 15 }}>1</Text>
            ) : (
              <PaperClipIcon />
            )}
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => pickImage("AA")}
            style={[
              styles.textInputContainer,
              {
                backgroundColor: "#FFFFFF",
                justifyContent: "space-between",
                paddingRight: 15,
              },
            ]}
          >
            <Text style={styles.textInput}>Attach Address</Text>
            {addressImg ? (
              <Text style={{ color: "#222", fontSize: 15 }}>1</Text>
            ) : (
              <PaperClipIcon />
            )}
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => pickImage("PC")}
            style={[
              styles.textInputContainer,
              {
                backgroundColor: "#FFFFFF",
                justifyContent: "space-between",
                paddingRight: 15,
              },
            ]}
          >
            <Text style={styles.textInput}>Police Clearance</Text>
            {policeClearanceImg ? (
              <Text style={{ color: "#222", fontSize: 15 }}>1</Text>
            ) : (
              <PaperClipIcon />
            )}
          </TouchableOpacity>

          <TouchableOpacity
            onPress={onSubmit}
            style={{ marginTop: 40, width: "100%", height: 40 }}
          >
            <LinearGradient
              style={{
                flex: 1,
                justifyContent: "center",
                alignItems: "center",
                borderRadius: 13,
              }}
              colors={[AppColors.linear1, AppColors.linear2]}
            >
              <Text
                style={{ color: "#FFFFFF", fontSize: 14, fontFamily: "RBo" }}
              >
                C O M P L E T E A C C O U N T
              </Text>
            </LinearGradient>
          </TouchableOpacity>
        </View>
      </ScrollView>
      {/* <View style={{flex:1}}> */}
      {/* <View> */}
      {showSubCategory && (
        <View style={styles.pickerPopUpWrapper}>
          <View style={styles.pickerPopUp}>
            <ScrollView>
              {subCategories[skillCategory]?.items?.map((item, index) => {
                return (
                  <TouchableOpacity
                    onPress={() => {
                      setSubCategory(item);
                      setShowSubCategory(false);
                    }}
                    style={styles.pickerPopUpItem}
                    key={index}
                  >
                    <Text>{item}</Text>
                  </TouchableOpacity>
                );
              })}
            </ScrollView>
          </View>
        </View>
      )}
      {showCategory && (
        <View style={styles.pickerPopUpWrapper}>
          <View style={styles.pickerPopUp}>
            <ScrollView>
              {categoriesArray.map((item, index) => {
                return (
                  <TouchableOpacity
                    onPress={() => {
                      setSkillCategory(index);
                      setShowCategory(false);
                    }}
                    style={styles.pickerPopUpItem}
                    key={index}
                  >
                    <Text>{item.name}</Text>
                  </TouchableOpacity>
                );
              })}
            </ScrollView>
          </View>
        </View>
      )}
      {/* </View> */}
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  textInputContainer: {
    flexDirection: "row",
    backgroundColor: "#E4E4E4",
    borderRadius: 13,
    height: 40,
    width: "100%",
    paddingLeft: 15,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 20,
  },
  textInput: {
    fontFamily: "RRe",
    fontSize: 14,
    color: "#707070",
  },
  radioBtn: {
    width: 20,
    height: 20,
    borderRadius: 5,
    borderWidth: 2,
    borderColor: "#707070",
    backgroundColor: "#FFFFFF",
    alignItems: "center",
    justifyContent: "center",
  },
  pickerPopUp: {
    width: "90%",
    backgroundColor: "#fff",
    borderRadius: 10,
    position: "absolute",
    paddingVertical: hp("2%"),
    minHeight: "30%",
    overflow: "scroll",
  },
  pickerPopUpWrapper: {
    width: wp("100%"),
    height: hp("100%"),
    position: "absolute",
    alignItems: "center",
    justifyContent: "center",
  },
  pickerPopUpItem: {
    width: "100%",
    height: hp("5%"),
    borderBottomWidth: 0.5,
    justifyContent: "center",
    paddingHorizontal: "5%",
    borderColor: "orange",
  },
});

const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  signup_res: state.main.signup_res,
});
export default connect(mapStateToProps, { signup })(HandymanRegister1);

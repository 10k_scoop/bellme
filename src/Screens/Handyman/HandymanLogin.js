import { LinearGradient } from "expo-linear-gradient";
import { StatusBar } from "expo-status-bar";
import React, { useState } from "react";
import {
  SafeAreaView,
  View,
  Text,
  TextInput,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
  ActivityIndicator,
} from "react-native";
import { connect } from "react-redux";
import AppColors from "../../Components/AppColors";
import {
  ArrowLeft,
  FbIcon,
  ForgetPasswordSvg,
  GoogleIcon,
  LogoSvg,
  MsgIcon,
  ORSvg,
  PasswordIcon,
  RegisterSvg,
} from "../../Components/SvgComponets";
import { login } from "../../state-management/actions/auth/AuthActions";

const HandymanLogin = (props) => {
  const [email, setEmail] = useState("");
  const [pass, setPass] = useState("");
  const [loading, setLoading] = useState(false);

  const onLogin = () => {
    setLoading(true);
    if (email != "" && pass != "") {
      let data = {
        email: email,
        pass: pass,
      };
      props.login(data, setLoading);
    } else {
      setLoading(false);
      alert("Fill all details");
    }
  };

  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: "#F5F5F5" }}>
      <StatusBar hidden={true} />
      <View
        style={{
          marginTop: Platform.OS == "android" ? "15%" : "10%",
          width: "90%",
          alignItems: "center",
        }}
      >
        <View style={{ flexDirection: "row" }}>
          <TouchableOpacity
            onPress={() => {
              props.navigation.goBack();
            }}
            style={{ marginRight: 10 }}
          >
            <ArrowLeft />
          </TouchableOpacity>
          <LogoSvg />
        </View>
      </View>
      <Text
        style={{
          alignSelf: "center",
          fontFamily: "RRe",
          fontSize: 14,
          color: "#707070",
          marginTop: 20,
        }}
      >
        Sign in to your SERVICE PROVIDER account
      </Text>
      <ScrollView contentContainerStyle={{ paddingBottom: 80 }}>
        <View
          style={{
            width: "85%",
            alignSelf: "center",
            alignItems: "center",
            marginTop: 10,
          }}
        >
          <View style={styles.textInputContainer}>
            <View style={{ position: "absolute", left: 15 }}>
              <MsgIcon />
            </View>
            <TextInput
              placeholder="Email Address"
              placeholderTextColor="#707070"
              style={styles.textInput}
              onChangeText={(val) => setEmail(val)}
            />
          </View>
          <View style={styles.textInputContainer}>
            <View style={{ position: "absolute", left: 15 }}>
              <PasswordIcon />
            </View>
            <TextInput
              placeholder="Password"
              placeholderTextColor="#707070"
              style={styles.textInput}
              onChangeText={(val) => setPass(val)}
              secureTextEntry={true}
            />
          </View>

          <TouchableOpacity
            onPress={() =>
              props.navigation.navigate("HandyManAuth", {
                screen: "ForgotPassword",
              })
            }
            style={{ alignSelf: "center", marginTop: 40 }}
          >
            <ForgetPasswordSvg />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => (loading ? undefined : onLogin())}
            style={{ marginTop: 40, width: "100%", height: 40 }}
          >
            <LinearGradient
              style={{
                flex: 1,
                justifyContent: "center",
                alignItems: "center",
                borderRadius: 13,
              }}
              colors={[AppColors.linear1, AppColors.linear2]}
            >
              {loading ? (
                <ActivityIndicator size="small" color="white" />
              ) : (
                <Text
                  style={{ color: "#FFFFFF", fontSize: 14, fontFamily: "RBo" }}
                >
                  L O G I N
                </Text>
              )}
            </LinearGradient>
          </TouchableOpacity>
          <Text
            style={{
              fontFamily: "RRe",
              fontSize: 14,
              color: "#707070",
              marginTop: 200,
            }}
          >
            Don't have an account yet?
          </Text>
          <TouchableOpacity
            onPress={() => props.navigation.navigate("HandymanRegister")}
            style={{ marginTop: 15 }}
          >
            <RegisterSvg />
          </TouchableOpacity>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  textInputContainer: {
    flexDirection: "row",
    backgroundColor: "#E4E4E4",
    borderRadius: 13,
    height: 40,
    width: "100%",
    paddingLeft: 15,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 35,
  },
  textInput: {
    fontFamily: "RRe",
    fontSize: 14,
    color: "#707070",
    width: "80%",
    textAlign: "center",
  },
});

const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  login_details: state.main.login_details,
});
export default connect(mapStateToProps, { login })(HandymanLogin);

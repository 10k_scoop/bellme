import React, { useEffect, useState } from "react";
import {
  SafeAreaView,
  Text,
  View,
  TouchableOpacity,
  StyleSheet,
  ActivityIndicator,
  ScrollView,
} from "react-native";
import { NotficationIcon } from "../../Components/SvgComponets";
import {
  appliedJobs,
  getAllNotifications,
} from "../../state-management/actions/features/Features";
import * as firebase from "firebase";
import { connect } from "react-redux";
import { RFValue } from "react-native-responsive-fontsize";
import { Entypo } from "@expo/vector-icons";
const HNotification = (props) => {
  const [notificationState, setNotificationState] = useState(true);
  const [projectState, setProjectState] = useState(false);
  const [jobs, setJobs] = useState([]);
  const [notifications, setNotifications] = useState([]);
  const [loading, setLoading] = useState(true);
  const user = firebase.auth().currentUser;
  let route = props?.route.params?.route;

  useEffect(() => {
    props.appliedJobs(setLoading);
    props.getAllNotifications(setLoading);
  }, []);

  useEffect(() => {
    if (props.applied_jobs != null) {
      setJobs(props?.applied_jobs);
      setProjectState(route == "quote" ? true : false);
      setNotificationState(route != "quote" ? true : false);
    }
  }, [props]);

  useEffect(() => {
    if (props.get_all_notifications != null) {
      let arr = [];
      props?.get_all_notifications?.map((item, index) => {
        if (item?.data?.data?.to == user.email) {
          arr.push(item);
        }
        setNotifications(arr);
      });
    }
  }, [props]);

  const NotificationView = (props) => (
    <TouchableOpacity
      // onPress={() =>
      //   //props.navigation.navigate("HHomeStack", { screen: "HLiveLocation" })
      //   props.navigation.navigate("HHomeStack", {
      //     screen: "HJobStatus",
      //     params: { data:{jobDetails:{id:props?.data?.data?.data?.jobId}} },
      //   })
      // }
      style={styles.notificationBox}
    >
      <View
        style={{
          backgroundColor: "#FBB040",
          width: 5,
          height: "60%",
          borderRadius: 20,
        }}
      ></View>
      <View style={{ marginLeft: 10 }}>
        <Text style={{ color: "#FBB040", fontFamily: "RBo", fontSize: 14 }}>
          {props?.type == "applied"
            ? "Congrats !"
            : props?.type == "completed"
            ? "Congrats !"
            : props?.type == "hired"
            ? "Congrats ! you are hired"
            : "Alert"}
        </Text>
        <Text style={{ fontFamily: "RRe", fontSize: 11 }}>
          {props?.data?.data?.data?.message}
        </Text>
      </View>
    </TouchableOpacity>
  );

  if (loading) {
    return (
      <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
        <ActivityIndicator color="black" size="large" />
      </View>
    );
  }

  const ProjectsView = (data) => (
    <TouchableOpacity
      onPress={() =>
        //props.navigation.navigate("HHomeStack", { screen: "HFeedback" })
        props.navigation.navigate("HHomeStack", {
          screen: "HJobStatus",
          params: { data: data?.data, id: data?.id },
        })
      }
      style={styles.notificationBox}
    >
      <View
        style={{
          backgroundColor:
            data?.data?.jobDetails?.status == "accepted" ||
            data?.data?.jobDetails?.status == "Complete"
              ? "green"
              : "#FBB040",
          width: 5,
          height: "60%",
          borderRadius: 20,
        }}
      ></View>
      <View style={{ marginLeft: 10, width: "100%" }}>
        <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
          <Text
            style={{
              color:
                data?.data?.jobDetails?.status == "accepted" ||
                data?.data?.jobDetails?.status == "Complete"
                  ? "green"
                  : "#FBB040",
              fontFamily: "RBo",
              fontSize: 14,
            }}
          >
            {data?.data?.jobDetails?.category}
          </Text>
          <Text
            style={{
              color: "#112D4E",
              fontFamily: "RRe",
              fontSize: 10,
              textAlign: "right",
              width: "70%",
            }}
          >
            {/* {data?.time == 0 ? "Today" : data?.time + " days ago"} */}
          </Text>
        </View>
        <Text style={{ fontFamily: "RRe", fontSize: 11, overflow: "visible" }}>
          {data?.data?.Hmandetails?.message}
        </Text>
        <View
          style={[
            styles.statusTextWrapper,
            {
              backgroundColor:
                data?.data?.jobDetails?.status == "accepted" ||
                data?.data?.jobDetails?.status == "Complete"
                  ? "green"
                  : "#FBB040",
            },
          ]}
        >
          <Text style={styles.statusText}>
            {data?.data?.jobDetails?.status}
          </Text>
        </View>
      </View>
    </TouchableOpacity>
  );

  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: "#F5F5F5" }}>
      <Text
        style={{
          fontSize: 20,
          fontFamily: "RBo",
          color: "#EF4136",
          alignSelf: "center",
          marginTop: Platform.OS == "android" ? "10%" : 0,
        }}
      >
        Notifications
      </Text>
      <View style={{ marginTop: 30, width: "80%", alignSelf: "center" }}>
        <View
          style={{
            width: "100%",
            paddingHorizontal: 3,
            height: 40,
            flexDirection: "row",
            backgroundColor: "#E4E4E4",
            borderRadius: 13,
          }}
        >
          <TouchableOpacity
            onPress={() => {
              setNotificationState(true);
              setProjectState(false);
            }}
            style={notificationState ? styles.activeTab : styles.inActiveTab}
          >
            <Text
              style={
                notificationState ? styles.activeText : styles.inActiveText
              }
            >
              Notifications
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              setProjectState(true);
              setNotificationState(false);
            }}
            style={projectState ? styles.activeTab : styles.inActiveTab}
          >
            <Text
              style={projectState ? styles.activeText : styles.inActiveText}
            >
              Projects
            </Text>
          </TouchableOpacity>
        </View>

        <ScrollView>
          {notificationState ? (
            <View style={{ marginTop: 20, flexDirection: "column-reverse" }}>
              {notifications?.length == 0 && (
                <View style={styles.errorMsg}>
                  <Entypo name="emoji-sad" size={RFValue(30)} color="black" />
                  <Text
                    style={{
                      marginVertical: 15,
                      fontSize: RFValue(17),
                      fontWeight: "600",
                      textAlign: "center",
                      width: "100%",
                    }}
                  >
                    No recent notifications
                  </Text>
                </View>
              )}
              {notifications?.map((item, index) => {
                return (
                  <NotificationView
                    navigation={props.navigation}
                    key={index}
                    data={item}
                    type={item?.data?.data?.type}
                  />
                );
              })}
            </View>
          ) : null}
          {projectState ? (
            <View style={{ marginTop: 20, flexDirection: "column-reverse" }}>
              {jobs?.length == 0 && (
                <View style={styles.errorMsg}>
                  <Entypo name="emoji-sad" size={RFValue(30)} color="black" />
                  <Text
                    style={{
                      marginVertical: 15,
                      fontSize: RFValue(17),
                      fontWeight: "600",
                      textAlign: "center",
                      width: "100%",
                    }}
                  >
                    No recent projects
                  </Text>
                </View>
              )}
              {jobs?.map((item, index) => {
                let time = Date(item?.data?.data?.Hmandetails?.appliedAt);
                // To calculate the time difference of two dates
                var Difference_In_Time = new Date() - new Date(time).getTime();
                // To calculate the no. of days between two dates
                var Difference_In_Days =
                  Difference_In_Time / (1000 * 3600 * 24);
                console.log(item?.jobDetails);
                return (
                  <ProjectsView
                    key={index}
                    data={item?.data?.data}
                    id={item?.id}
                    time={Math.floor(Difference_In_Days).toString()}
                  />
                );
              })}
            </View>
          ) : null}
        </ScrollView>
      </View>
    </SafeAreaView>
  );
};
const styles = StyleSheet.create({
  activeTab: {
    backgroundColor: "#FFFFFF",
    borderRadius: 11,
    width: "50%",
    height: 36,
    alignSelf: "center",
    justifyContent: "center",
    alignItems: "center",
  },
  errorMsg: {
    width: "100%",
    paddingVertical: 15,
    borderRadius: 20,
    paddingHorizontal: 10,
    alignItems: "center",
    marginTop: 10,
  },
  inActiveTab: {
    // backgroundColor:'#FFFFFF',
    // borderRadius:11,
    width: "50%",
    // height:36,
    alignSelf: "center",
    justifyContent: "center",
    alignItems: "center",
  },
  activeText: {
    color: "#EF4136",
    fontFamily: "RBo",
    fontSize: 14,
  },
  inActiveText: {
    color: "#707070",
    fontFamily: "RRe",
    fontSize: 14,
  },
  notificationBox: {
    width: "100%",
    paddingVertical: 15,
    borderRadius: 20,
    backgroundColor: "white",
    flexDirection: "row",
    paddingHorizontal: 10,
    alignItems: "center",
    marginTop: 10,
    paddingRight: 20,
  },
  statusTextWrapper: {
    width: "30%",
    alignItems: "center",
    display: "flex",
    paddingHorizontal: 10,
    backgroundColor: "#FBB040",
    borderRadius: 100,
    position: "absolute",
    right: 5,
    top: 0,
  },
  statusText: {
    fontFamily: "RRe",
    fontSize: 11,
    overflow: "visible",
    color: "#fff",
  },
});
const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  applied_jobs: state.main.applied_jobs,
  get_all_notifications: state.main.get_all_notifications,
});
export default connect(mapStateToProps, { appliedJobs, getAllNotifications })(
  HNotification
);

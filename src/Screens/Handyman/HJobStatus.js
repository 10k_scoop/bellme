import { LinearGradient } from "expo-linear-gradient";
import React, { useEffect, useState } from "react";
import {
  Dimensions,
  Image,
  SafeAreaView,
  ScrollView,
  Text,
  TouchableOpacity,
  ActivityIndicator,
  View,
} from "react-native";
import { connect } from "react-redux";
import AppColors from "../../Components/AppColors";
import { ArrowLeft, StarIcon } from "../../Components/SvgComponets";
import { viewJob } from "../../state-management/actions/features/Features";
import * as firebase from "firebase";
import { completeJobByProvider } from "../../state-management/actions/features/Features";
const HJobStatus = (props) => {
  const [loading, setLoading] = useState(true);
  const [data, setData] = useState(true);
  let jobData = props.route.params?.data;
  let applicationID = props.route.params?.id;
  const user = firebase.auth().currentUser;

  useEffect(() => {
    props?.viewJob(jobData?.jobDetails?.id, setLoading);
  }, []);

  useEffect(() => {
    if (props?.view_job != "") {
      setData(props?.view_job);
    }
    if (props?.errors != "") {
      console.log(props?.errors);
    }
  }, [props]);

  const onComplete = async () => {
    let clientNotification = {
      from: jobData?.Hmandetails?.email,
      to: jobData?.clientDetails?.email,
      message: "Job marked as complete by " + jobData?.Hmandetails?.name,
      jobId: jobData?.jobDetails?.id,
      type: "completed",
      handyManId: jobData?.Hmandetails?.id,
      clientId: jobData?.clientDetails?.id,
      status: "applied",
      providerEarning: props?.get_user_details?.earnings
        ? eval(props?.get_user_details?.earnings) + eval(data?.budget)
        : data?.budget,
    };
    jobData.jobDetails.status = "Complete";
    jobData.Hmandetails.message = "Job has been completed ";
    setLoading(true);
    await props?.completeJobByProvider(
      jobData,
      clientNotification,
      applicationID,
      setLoading
    );
    props?.viewJob(jobData?.jobDetails?.id, setLoading);
  };
  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: "#F5F5F5" }}>
      <View
        style={{
          flex: 1,
          width: "80%",
          alignSelf: "center",
          marginTop: Platform.OS == "android" ? "10%" : 0,
        }}
      >
        <View
          style={{
            width: "100%",
            flexDirection: "row",
            justifyContent: "space-between",
          }}
        >
          <TouchableOpacity
            onPress={() => {
              props.navigation.goBack();
            }}
          >
            <ArrowLeft />
          </TouchableOpacity>
          <Text style={{ color: "#EF4136", fontFamily: "RBo", fontSize: 20 }}>
            Job Status
          </Text>
          <Text></Text>
        </View>

        {loading ? (
          <View style={{ flex: 1, justifyContent: "center" }}>
            <ActivityIndicator color="orange" size="large" />
          </View>
        ) : (
          <>
            <Image
              style={{
                alignSelf: "center",
                marginTop: 30,
                width: 100,
                height: 100,
                borderRadius: 50,
              }}
              source={{ uri: data?.authorProfile }}
            />
            <Text
              style={{
                alignSelf: "center",
                marginTop: 5,
                fontSize: 20,
                fontFamily: "RBo",
              }}
            >
              {data?.category}
            </Text>
            <ScrollView
              contentContainerStyle={{ flex: 1 }}
              // style={{backgroundColor:'red',}}
            >
              <View
                style={{
                  width: "100%",
                  height: 40,
                  backgroundColor: "white",
                  borderRadius: 13,
                  flexDirection: "row",
                  marginTop: 15,
                  alignItems: "center",
                  justifyContent: "space-between",
                  paddingHorizontal: 20,
                }}
              >
                <Text
                  style={{ fontSize: 14, fontFamily: "RRe", color: "#707070" }}
                >
                  Sub-Category
                </Text>
                <Text
                  style={{ fontSize: 14, fontFamily: "RRe", color: "#FBB040" }}
                >
                  {data?.subCategory}
                </Text>
              </View>

              <View
                style={{
                  width: "100%",
                  height: 40,
                  backgroundColor: "white",
                  borderRadius: 13,
                  flexDirection: "row",
                  marginTop: 15,
                  alignItems: "center",
                  justifyContent: "space-between",
                  paddingHorizontal: 20,
                }}
              >
                <Text
                  style={{ fontSize: 14, fontFamily: "RRe", color: "#707070" }}
                >
                  Status
                </Text>
                <Text
                  style={{ fontSize: 14, fontFamily: "RRe", color: "#FBB040" }}
                >
                  {data?.hiredId == user?.uid && data?.status == "onGoing"
                    ? "process"
                    : data?.hiredId == user?.uid &&
                      data?.status == "half-completed"
                    ? "Marked as complete by client"
                    : data?.hiredId == user?.uid && data?.status == "completed"
                    ? "Completed"
                    : data?.status}
                </Text>
              </View>

              <Text
                style={{
                  fontSize: 14,
                  fontFamily: "RBo",
                  color: "#112D4E",
                  marginTop: 25,
                }}
              >
                Quote
              </Text>
              <View
                style={{
                  width: "100%",
                  paddingTop: 10,
                  paddingBottom: 60,
                  backgroundColor: "#FFFFFF",
                  marginTop: 10,
                  borderRadius: 13,
                  paddingHorizontal: 20,
                }}
              >
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "space-between",
                  }}
                >
                  <Text
                    style={{
                      color: "#707070",
                      fontSize: 14,
                      fontFamily: "RRe",
                    }}
                  >
                    Estimate Budget
                  </Text>
                  <Text
                    style={{
                      color: "#EF4136",
                      fontFamily: "RBo",
                      fontSize: 14,
                    }}
                  >
                    {data?.budget}
                  </Text>
                </View>
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "space-between",
                    marginTop: 15,
                  }}
                >
                  <Text
                    style={{
                      color: "#707070",
                      fontSize: 14,
                      fontFamily: "RRe",
                    }}
                  >
                    Time to Complete
                  </Text>
                  <Text
                    style={{
                      color: "#EF4136",
                      fontFamily: "RBo",
                      fontSize: 14,
                    }}
                  >
                    {data?.duration}
                  </Text>
                </View>
                <Text
                  style={{
                    color: "#707070",
                    fontSize: 14,
                    fontFamily: "RBo",
                    marginTop: 15,
                  }}
                >
                  Remarks
                </Text>
                <Text
                  style={{
                    color: "#707070",
                    fontSize: 10,
                    fontFamily: "RRe",
                    marginTop: 7,
                    lineHeight: 13,
                  }}
                >
                  {data?.desc}
                </Text>
              </View>
              {data?.hiredId == user?.uid && data?.hiredSomeone && (
                <>
                  <TouchableOpacity
                    style={{ width: "100%", height: 40, marginTop: 20 }}
                    onPress={() =>
                      props.navigation.navigate("HChat2", { data: jobData })
                    }
                  >
                    <LinearGradient
                      style={{
                        flex: 1,
                        borderRadius: 13,
                        alignItems: "center",
                        justifyContent: "center",
                      }}
                      colors={[AppColors.linear1, AppColors.linear2]}
                    >
                      <Text
                        style={{
                          color: "white",
                          fontFamily: "RBo",
                          fontSize: 14,
                        }}
                      >
                        Chat now
                      </Text>
                    </LinearGradient>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={{ width: "100%", height: 40, marginTop: 20 }}
                    onPress={() =>
                      props.navigation.navigate("CheckLocation", {
                        data: {Hmandetails:{handyManLocations:data?.clientsLocations}},
                      })
                    }
                  >
                    <LinearGradient
                      style={{
                        flex: 1,
                        borderRadius: 13,
                        alignItems: "center",
                        justifyContent: "center",
                      }}
                      colors={["#112D4E", "#112D4E"]}
                    >
                      <Text
                        style={{
                          color: "white",
                          fontFamily: "RBo",
                          fontSize: 14,
                        }}
                      >
                        Check Location
                      </Text>
                    </LinearGradient>
                  </TouchableOpacity>
                </>
              )}
              {data?.hiredId == user?.uid && data?.status == "half-completed" && (
                <View
                  style={{
                    flexDirection: "row",
                    alignItems: "flex-end",
                    justifyContent: "space-between",
                    flex: 1,
                    paddingBottom: 40,
                  }}
                >
                  <TouchableOpacity
                    style={{ width: "45%", height: 40, marginTop: 20 }}
                    onPress={() =>
                      props.navigation.navigate("HChat2", { data: jobData })
                    }
                  >
                    <LinearGradient
                      style={{
                        flex: 1,
                        borderRadius: 13,
                        alignItems: "center",
                        justifyContent: "center",
                      }}
                      colors={["red", AppColors.linear2]}
                    >
                      <Text
                        style={{
                          color: "white",
                          fontFamily: "RBo",
                          fontSize: 14,
                        }}
                      >
                        Reject
                      </Text>
                    </LinearGradient>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={{ width: "45%", height: 40, marginTop: 20 }}
                    onPress={async () => onComplete()}
                  >
                    <LinearGradient
                      style={{
                        flex: 1,
                        borderRadius: 13,
                        alignItems: "center",
                        justifyContent: "center",
                      }}
                      colors={["lightgreen", "green"]}
                    >
                      <Text
                        style={{
                          color: "white",
                          fontFamily: "RBo",
                          fontSize: 14,
                        }}
                      >
                        Complete
                      </Text>
                    </LinearGradient>
                  </TouchableOpacity>
                </View>
              )}
            </ScrollView>
          </>
        )}
      </View>
    </SafeAreaView>
  );
};

const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  get_user_details: state.main.get_user_details,
  view_job: state.main.view_job,
});
export default connect(mapStateToProps, { viewJob, completeJobByProvider })(
  HJobStatus
);

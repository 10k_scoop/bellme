import MaskedView from '@react-native-community/masked-view';
import { LinearGradient } from 'expo-linear-gradient';
import React from 'react'
import { Dimensions, Image, SafeAreaView, ScrollView, Text, TextInput, TouchableOpacity, View } from 'react-native';
import AppColors from '../../Components/AppColors';
import { ArrowLeft, DollarIcon, StarBlack, StarIcon } from '../../Components/SvgComponets';

const HFeedback = (props) => {
    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: '#F5F5F5', }}>

            <View style={{ flex: 1, width: "80%", alignSelf: 'center', marginTop: Platform.OS == "android" ? "10%" : 0 }}>
                <View style={{ width: "100%", flexDirection: 'row', justifyContent: 'space-between' }}>
                    <TouchableOpacity
                        onPress={() => {
                            props.navigation.goBack();
                        }}
                    >
                        <ArrowLeft />
                    </TouchableOpacity>
                    <Text style={{ color: '#EF4136', fontFamily: 'RBo', fontSize: 20 }}>Job Completed</Text>
                    <Text></Text>
                </View>

                <Image
                    style={{ alignSelf: 'center', marginTop: 30, width: 100, height: 100, borderRadius: 50, }}
                    source={require('../../assets/profileIcon2.png')}
                />

                <Text style={{ alignSelf: 'center', marginTop: 5, fontSize: 20, fontFamily: 'RBo' }}>Selena Adems</Text>

                <ScrollView
                    showsVerticalScrollIndicator={false}
                    contentContainerStyle={{ paddingBottom: 100 }}
                // style={{backgroundColor:'red',}}
                >
                    <View style={{ width: "100%", height: 40, backgroundColor: 'white', borderRadius: 13, flexDirection: 'row', marginTop: 15, alignItems: 'center', justifyContent: 'space-between', paddingHorizontal: 20 }}>
                        <Text style={{ fontSize: 14, fontFamily: 'RRe', color: "#707070" }}>Home</Text>
                        <Text style={{ fontSize: 14, fontFamily: 'RRe', color: '#FBB040' }}>123, Jason Burg 23, London</Text>
                    </View>



                    <MaskedView
                        style={{ width: "100%", height: 30, marginTop: 25 }}
                        maskElement={<Text style={{ fontSize: 20, fontFamily: 'RBo', alignSelf: 'center' }}>Rate Your Experience</Text>}
                    >
                        <LinearGradient
                            colors={[AppColors.linear1, AppColors.linear2]}
                            start={{ x: 0, y: 1 }}
                            end={{ x: 1, y: 1 }}
                            style={{ flex: 1 }}
                        />
                    </MaskedView>
                    <Text style={{ color: '#707070', fontSize: 12, fontFamily: 'RRe', textAlign: 'center', lineHeight: 16, marginTop: 5 }}>Selena has marked your job completed, please rate your experience with her</Text>

                    <View style={{ backgroundColor: 'white', paddingBottom: 15, paddingHorizontal: 15, borderRadius: 13, marginTop: 20 }}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 15 }}>
                            <Text style={{ fontFamily: 'RRe', fontSize: 14, color: "#707070" }}>Estimate Cost</Text>
                            <Text style={{ fontFamily: 'RBo', fontSize: 14, color: '#EF4136' }}>$200</Text>
                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 15 }}>
                            <Text style={{ fontFamily: 'RRe', fontSize: 14, color: "#707070" }}>Behavior</Text>
                            <Text style={{ fontFamily: 'RBo', fontSize: 14, color: '#EF4136' }}>3 days</Text>
                        </View>
                        <Text style={{ fontSize: 14, marginTop: 15, color: '#707070', fontFamily: 'RBo' }}>Remarks</Text>
                        <Text style={{ color: '#707070', fontFamily: 'RRe', fontSize: 10, lineHeight: 13 }}>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et</Text>

                    </View>
                    <TextInput
                        style={{ width: "100%", paddingBottom: 70, paddingLeft: 10, paddingTop: 10, backgroundColor: '#E4E4E4', borderRadius: 10, marginTop: 20, color: '#707070', fontFamily: 'RRe', fontSize: 14 }}
                        placeholder="Write a review"
                        placeholderTextColor="#707070"
                        textAlignVertical="top"
                        multiline={true}
                    />
                    <View style={{ flexDirection: 'row', width: '100%', justifyContent: 'space-between', marginTop: 15 }}>
                        <Text style={{ fontFamily: 'RRe', fontSize: 14, color: "#707070" }}>Overall Experience</Text>
                        <View style={{ flexDirection: 'row' }}>
                            <StarBlack />
                            <StarBlack />
                            <StarBlack />
                            <StarBlack />
                            <StarBlack />
                        </View>
                    </View>

                    <TouchableOpacity
                        onPress={() => {
                            props.navigation.navigate('HMBottomTabNavigator', { screen: "HHome" })
                        }}
                        style={{ marginTop: 25, width: "100%", height: 40, }}>
                        <LinearGradient
                            style={{ flex: 1, borderRadius: 13, alignItems: 'center', justifyContent: 'center', }}
                            colors={[AppColors.linear1, AppColors.linear2]}
                        >
                            <Text style={{ color: 'white', fontFamily: 'RBo', fontSize: 14 }}>S U B M I T   F E E D B A C K</Text>
                        </LinearGradient>
                    </TouchableOpacity>


                </ScrollView>


            </View>
        </SafeAreaView>
    )
}

export default HFeedback

import {
  APPLIEDJOB,
  APPLY_TO_JOB,
  GET_All_NOTIFICATIONS,
  GET_CHAT,
  GET_CHAT_RELATIONS,
  GET_ERRORS,
  GET_JOBS,
  GET_PROVIDERS,
  GET_QUOTE_DETAILS,
  GET_USER_DETAILS,
  MY_JOBS,
  POST_JOB,
  VIEW_JOB,
} from "../../types/types";
import * as firebase from "firebase";

export const getJobs = (setLoading) => async (dispatch) => {
  var db = firebase.firestore();
  try {
    db.collection("jobs")
      .orderBy("date", "desc")
      .onSnapshot((querySnapshot) => {
        var jobs = [];
        querySnapshot.forEach((doc) => {
          let arr = doc.data();
          arr.id = doc.id;
          jobs.push(arr);
        });
        dispatch({ type: GET_JOBS, payload: jobs });
        setLoading(false);
      });
  } catch (e) {
    console.log(e.message);
    dispatch({ type: GET_ERRORS, payload: e.message });
    setLoading(false);
  }
};

export const viewJob = (id, setLoading) => async (dispatch) => {
  var db = firebase.firestore();
  try {
    var docRef = db.collection("jobs").doc(id);

    docRef
      .get()
      .then((doc) => {
        if (doc.exists) {
          dispatch({ type: VIEW_JOB, payload: doc.data() });
        }
        setLoading(false);
      })
      .catch((error) => {
        console.log("Error getting document:", error);
      });
  } catch (e) {
    dispatch({ type: GET_ERRORS, payload: e.message });
    setLoading(false);
  }
};

export const postJob = (data, setLoading, setModalVisible) => async (
  dispatch
) => {
  var db = firebase.firestore();
  try {
    db.collection("jobs")
      .add(data)
      .then((docRef) => {
        dispatch({ type: POST_JOB, payload: docRef });
        setLoading(false);
        setModalVisible(true);
        //alert("Congrats Your job is live now");
        console.log(docRef);
      })
      .catch((error) => {
        dispatch({ type: GET_ERRORS, payload: e.message });
        setLoading(false);
      });
  } catch (e) {
    dispatch({ type: GET_ERRORS, payload: e.message });
    setLoading(false);
    console.log("empty");
  }
};

export const myJobs = (email, setLoading) => async (dispatch) => {
  var db = firebase.firestore();
  try {
    db.collection("jobs")
      .where("authorEmail", "==", email)
      .orderBy("date", "desc")
      .onSnapshot((querySnapshot) => {
        var jobs = [];
        querySnapshot.forEach((doc) => {
          jobs.push({ data: doc.data(), id: doc.id });
        });
        dispatch({ type: MY_JOBS, payload: jobs });
        setLoading(false);
      });
  } catch (e) {
    dispatch({ type: GET_ERRORS, payload: e.message });
    setLoading(false);
  }
};

export const providerJobs = (email, setLoading) => async (dispatch) => {
  var db = firebase.firestore();
  console.log(email);

  try {
    db.collection("jobs")
      .where("hiredEmail", "==", email)
      .where("status", "==", "completed")
      .onSnapshot((querySnapshot) => {
        var jobs = [];
        querySnapshot.forEach((doc) => {
          jobs.push({ data: doc.data(), id: doc.id });
        });
        dispatch({ type: MY_JOBS, payload: jobs });
        setLoading(false);
      });
  } catch (e) {
    dispatch({ type: GET_ERRORS, payload: e.message });
    setLoading(false);
    console.log(e.message);
  }
};

export const appliedJobs = (setLoading) => async (dispatch) => {
  var db = firebase.firestore();

  try {
    db.collection("activeJobs")
      .orderBy("createdAt", "desc")
      .get()
      .then((querySnapshot) => {
        var jobs = [];
        querySnapshot.forEach((doc) => {
          jobs.push({ id: doc.id, data: doc.data() });
        });
        dispatch({ type: APPLIEDJOB, payload: jobs });
        setLoading(false);
      })
      .catch((e) => {
        setLoading(false);
        dispatch({ type: GET_ERRORS, payload: e.message });
        console.log(e.message);
      });
  } catch (e) {
    dispatch({ type: GET_ERRORS, payload: e.message });
    setLoading(false);
    console.log(e.message);
  }
};

export const getUserData = (id, setLoading) => async (dispatch) => {
  var db = firebase.firestore();
  var docRef = db.collection("users").doc(id);

  docRef
    .get()
    .then((doc) => {
      if (doc.exists) {
        dispatch({ type: GET_USER_DETAILS, payload: doc.data() });
        setLoading(false);
      } else {
        // doc.data() will be undefined in this case
        console.log("No such document!");
      }
    })
    .catch((error) => {
      console.log(error.message);
      dispatch({ type: GET_ERRORS, payload: error });
      setLoading(false);
    });
};

export const getAnyUserData = (id, setLoading, setAData) => async (
  dispatch
) => {
  var db = firebase.firestore();
  var docRef = db.collection("users").doc(id);

  docRef
    .get()
    .then((doc) => {
      if (doc.exists) {
        setAData(doc.data());
        setLoading(false);
      } else {
        // doc.data() will be undefined in this case
        console.log("No such document!");
      }
    })
    .catch((error) => {
      console.log(error.message);
      dispatch({ type: GET_ERRORS, payload: error });
      setLoading(false);
    });
};

export const applyToJob = (data, setLoading, setModalVisible) => async (
  dispatch
) => {
  try {
    var db = firebase.firestore();
    let providerNotification = {
      to: data?.data?.Hmandetails?.email,
      from: data?.data?.clientDetails?.email,
      message:
        "Your application has been sent for " +
        data?.data?.jobDetails?.subCategory +
        " job",
      jobId: data?.data?.jobDetails?.id,
      type: "applied",
      handyManId: data?.data?.Hmandetails?.id,
      clientId: data?.data?.clientDetails?.id,
      status: "new",
      date: new Date(),
    };
    let clientNotification = {
      from: data?.data?.Hmandetails?.email,
      to: data?.data?.clientDetails?.email,
      message:
        "You just have recieved a quote from" + data?.data?.Hmandetails?.name,
      jobId: data?.data?.jobDetails?.id,
      type: "applied",
      handyManId: data?.data?.Hmandetails?.id,
      clientId: data?.data?.clientDetails?.id,
      status: "new",
      date: new Date(),
    };

    db.collection("activeJobs")
      .add(data)
      .then((docRef) => {
        db.collection("notifications")
          .add({ data: providerNotification, applicationId: docRef.id })
          .then((res) => {
            db.collection("notifications")
              .add({ data: clientNotification, applicationId: docRef.id })
              .then((res) => {
                db.collection("jobs")
                  .doc(data?.data?.jobDetails?.id)
                  .update({
                    applicants: data?.data?.jobDetails?.applicants,
                  })
                  .then((res) => {
                    dispatch({ type: APPLY_TO_JOB, payload: "success" });
                    setLoading(false);
                    setModalVisible(true);
                  })
                  .catch((error) => {
                    dispatch({ type: GET_ERRORS, payload: e.message });
                    setLoading(false);
                  });
              })
              .catch((error) => {
                dispatch({ type: GET_ERRORS, payload: e.message });
                setLoading(false);
              });
          })
          .catch((error) => {
            dispatch({ type: GET_ERRORS, payload: e.message });
            setLoading(false);
          });
      })
      .catch((error) => {
        dispatch({ type: GET_ERRORS, payload: error });
        console.log("Error adding document: ", error);
        setLoading(false);
      });
  } catch (e) {
    console.log(e.message);
    dispatch({ type: GET_ERRORS, payload: e.message });
    setLoading(false);
  }
};

export const getAllNotifications = (setLoading) => async (dispatch) => {
  var db = firebase.firestore();

  try {
    db.collection("notifications")
      .get()
      .then((querySnapshot) => {
        var noti = [];
        querySnapshot.forEach((doc) => {
          noti.push({ id: doc.id, data: doc.data() });
        });
        dispatch({ type: GET_All_NOTIFICATIONS, payload: noti });
        setLoading(false);
      })
      .catch((e) => {
        setLoading(false);
        dispatch({ type: GET_ERRORS, payload: e.message });
        console.log(e.message);
      });
  } catch (e) {
    dispatch({ type: GET_ERRORS, payload: e.message });
    setLoading(false);
    console.log(e.message);
  }
};

export const getQuoteDetails = (id, setLoading) => async (dispatch) => {
  var db = firebase.firestore();
  var docRef = db.collection("activeJobs").doc(id);

  docRef
    .get()
    .then((doc) => {
      if (doc.exists) {
        dispatch({
          type: GET_QUOTE_DETAILS,
          payload: { data: doc.data(), id: doc.id },
        });
        setLoading(false);
      } else {
        // doc.data() will be undefined in this case
        console.log("No such document!");
      }
    })
    .catch((error) => {
      console.log(error.message);
      dispatch({ type: GET_ERRORS, payload: error });
      setLoading(false);
    });
};

export const getSingleQuoteDetails = (id, setLoading) => async (dispatch) => {
  var db = firebase.firestore();
  var docRef = db.collection("activeJobs");

  docRef
    .get()
    .then((querySnapshot) => {
      querySnapshot.forEach((doc) => {
        if (doc.data()?.data?.jobDetails?.id == id) {
          dispatch({
            type: GET_QUOTE_DETAILS,
            payload: { data: doc.data(), id: doc.id },
          });
          setLoading(false);
        } else {
          console.log(id);
        }
      });
    })
    .catch((error) => {
      console.log(error.message);
      dispatch({ type: GET_ERRORS, payload: error });
      setLoading(false);
    });
};

export const acceptJob = (data, id, setLoading) => async (dispatch) => {
  try {
    var db = firebase.firestore();
    var docRef = db.collection("activeJobs").doc(id);

    let providerNotification = {
      to: data?.Hmandetails?.email,
      from: data?.clientDetails?.email,
      message:
        "Congrats! you got a new job, Click to check details. " +
        data?.jobDetails?.subCategory +
        " job",
      jobId: data?.jobDetails?.id,
      type: "hired",
      handyManId: data?.Hmandetails?.id,
      clientId: data?.clientDetails?.id,
      status: "new",
      date: new Date(),
    };

    docRef
      .update({ data: data })
      .then((doc) => {
        db.collection("notifications")
          .add({ data: providerNotification, applicationId: id })
          .then((res) => {
            db.collection("jobs")
              .doc(data?.jobDetails?.id)
              .update({
                hiredSomeone: true,
                status: "onGoing",
                hiredId: data?.Hmandetails?.id,
                hiredEmail: data?.Hmandetails?.email,
              })
              .then((res) => {
                db.collection("users")
                  .doc(data?.Hmandetails?.id)
                  .update({
                    hired: true,
                  })
                  .then((res) => {
                    alert(
                      "Congrats! Job has been started contact the handyman for support"
                    );
                    setLoading(false);
                  })
                  .catch((error) => {
                    dispatch({ type: GET_ERRORS, payload: e.message });
                    setLoading(false);
                  });
              })
              .catch((error) => {
                dispatch({ type: GET_ERRORS, payload: e.message });
                setLoading(false);
              });
          })
          .catch((error) => {
            dispatch({ type: GET_ERRORS, payload: e.message });
            setLoading(false);
          });
      })
      .catch((error) => {
        console.log(error.message);
        dispatch({ type: GET_ERRORS, payload: error });
        setLoading(false);
      });
  } catch (e) {
    console.log(e.message);
  }
};

export const sendMsg = (data, setLoading) => async (dispatch) => {
  var db = firebase.firestore();
  try {
    db.collection("messages")
      .add(data)
      .then((docRef) => {
        setLoading(false);
      })
      .catch((error) => {
        dispatch({ type: GET_ERRORS, payload: e.message });
        setLoading(false);
      });
  } catch (e) {
    dispatch({ type: GET_ERRORS, payload: e.message });
    setLoading(false);
    console.log(e.message);
  }
};

export const deleteAJob = (id, setLoading, navigation) => async (dispatch) => {
  var db = firebase.firestore();
  try {
    db.collection("jobs")
      .doc(id)
      .update({
        status: "deactivated",
      })
      .then(() => {
        setLoading(false);
        alert("Job has been deleted");
        navigation.navigate("Home");
      })
      .catch((error) => {
        dispatch({ type: GET_ERRORS, payload: e.message });
        setLoading(false);
      });
  } catch (e) {
    dispatch({ type: GET_ERRORS, payload: e.message });
    setLoading(false);
    console.log(e.message);
  }
};

export const makeChatRelation = (ids, data, user, setLoading) => async (
  dispatch
) => {
  var db = firebase.firestore();

  let chatData = {
    users: [data?.clientDetails?.email, data?.Hmandetails?.email],
    sender: user?.email,
    reciever:
      user?.email == data?.clientDetails?.email
        ? data?.Hmandetails?.email
        : data?.clientDetails?.email,
    encodeId: ids[0],
    lastText: "hi hello",
    createdAt: new Date(),
    chatDetails: data,
  };
  try {
    db.collection("chatRelation")
      .where("encodeId", "in", ids)
      .get()
      .then((querySnapshot) => {
        if (querySnapshot.size == 0) {
          db.collection("chatRelation")
            .add(chatData)
            .then((docRef) => {
              console.log("relation Added");
              setLoading(false);
            })
            .catch((e) => {
              dispatch({ type: GET_ERRORS, payload: e.message });
              setLoading(false);
            });
        } else {
          console.log("relation already exists");
        }
      })
      .catch((e) => {
        setLoading(false);
        dispatch({ type: GET_ERRORS, payload: e.message });
        console.log(e.message);
      });
  } catch (e) {
    dispatch({ type: GET_ERRORS, payload: e.message });
    setLoading(false);
    console.log(e.message);
  }
};
export const renewChat = (ids, setLoading) => async (dispatch) => {
  try {
    var db = firebase.firestore();

    db.collection("chatRelation")
      .where("encodeId", "in", ids)
      .get()
      .then((querySnapshot) => {
        querySnapshot.forEach((doc) => {
          db.collection("chatRelation")
            .doc(doc?.id)
            .update({ deletedFor: [] })
            .then(() => {
              setLoading(false);
            })
            .catch((e) => {
              setLoading(false);
              dispatch({ type: GET_ERRORS, payload: e.message });
              console.log(e.message);
            });
        });
      })
      .catch((e) => {
        setLoading(false);
        dispatch({ type: GET_ERRORS, payload: e.message });
        console.log(e.message);
      });
  } catch (e) {
    setLoading(false);
    dispatch({ type: GET_ERRORS, payload: e.message });
    console.log(e.message);
  }
};

export const getChat = (data, setLoading) => async (dispatch) => {
  var db = firebase.firestore();
  console.log(data);
  try {
    db.collection("messages")
      .where("encodeId", "in", data)
      .orderBy("createdAt")
      .onSnapshot((querySnapshot) => {
        var chat = [];
        querySnapshot.forEach((doc) => {
          chat.push(doc.data());
        });
        dispatch({ type: GET_CHAT, payload: chat });
        setLoading(false);
      });
  } catch (e) {
    dispatch({ type: GET_ERRORS, payload: e.message });
    setLoading(false);
    console.log(e.message);
  }
};
export const getChatRelations = (user, setLoading) => async (dispatch) => {
  var db = firebase.firestore();
  console.log(user);
  try {
    db.collection("chatRelation")
      .where("users", "array-contains", user)
      .onSnapshot((querySnapshot) => {
        var chat = [];
        querySnapshot.forEach((doc) => {
          var chat = [];
          querySnapshot.forEach((doc) => {
            chat.push({ data: doc.data(), id: doc.id });
          });
          dispatch({ type: GET_CHAT_RELATIONS, payload: chat });
          setLoading(false);
        });
        dispatch({ type: GET_CHAT, payload: chat });
        setLoading(false);
      });
  } catch (e) {
    dispatch({ type: GET_ERRORS, payload: e.message });
    setLoading(false);
    console.log(e.message);
  }
};

export const completeJobByClient = (data, setLoading) => async (dispatch) => {
  var db = firebase.firestore();
  try {
    db.collection("jobs")
      .doc(data?.jobId)
      .update({
        status: "half-completed",
      })
      .then((res) => {
        db.collection("notifications")
          .add({ data: data, applicationId: null })
          .then((res) => {
            db.collection("users")
              .doc(data?.handyManId)
              .update({
                rating: data?.rating,
              })
              .then(() => {
                setLoading(false);
                alert("Submitted to handyman, for review request.");
              })
              .catch((error) => {
                dispatch({ type: GET_ERRORS, payload: e.message });
                setLoading(false);
              });
          })
          .catch((error) => {
            dispatch({ type: GET_ERRORS, payload: e.message });
            setLoading(false);
          });
      })
      .catch((error) => {
        dispatch({ type: GET_ERRORS, payload: error.message });
        setLoading(false);
      });
  } catch (e) {
    dispatch({ type: GET_ERRORS, payload: e.message });
    setLoading(false);
    console.log(e.message);
  }
};

export const completeJobByProvider = (
  data,
  notiData,
  appId,
  setLoading
) => async (dispatch) => {
  console.log(notiData?.handyManId);
  var db = firebase.firestore();
  try {
    db.collection("jobs")
      .doc(notiData?.jobId)
      .update({
        status: "completed",
      })
      .then((res) => {
        db.collection("notifications")
          .add({ data: notiData, applicationId: null })
          .then((res) => {
            db.collection("activeJobs")
              .doc(appId)
              .update({ data: data })
              .then(() => {
                db.collection("users")
                  .doc(notiData?.handyManId)
                  .update({
                    earnings: notiData?.providerEarning,
                    hired: false,
                  })
                  .then(() => {
                    setLoading(false);
                    alert("Congrats ! Job has been completed.");
                  })
                  .catch((e) => {
                    dispatch({ type: GET_ERRORS, payload: e.message });
                    setLoading(false);
                  });
              })
              .catch((e) => {
                dispatch({ type: GET_ERRORS, payload: e.message });
                setLoading(false);
              });
          })
          .catch((e) => {
            dispatch({ type: GET_ERRORS, payload: e.message });
            setLoading(false);
          });
      })
      .catch((error) => {
        dispatch({ type: GET_ERRORS, payload: error.message });
        setLoading(false);
        alert("Something went wrong !");
      });
  } catch (e) {
    dispatch({ type: GET_ERRORS, payload: e.message });
    setLoading(false);
    console.log(e.message);
  }
};

export const profileUpdate = (
  data,
  id,
  setLoading,
  navigation,
  route
) => async (dispatch) => {
  var db = firebase.firestore();
  try {
    db.collection("users")
      .doc(id)
      .update(data)
      .then(() => {
        db.collection("users")
          .doc(id)
          .get()
          .then((doc) => {
            if (doc.exists) {
              dispatch({ type: GET_USER_DETAILS, payload: doc.data() });
              setLoading(false);
              alert("Updated");
              navigation.reset({
                index: 0,
                routes: [{ name: route }],
              });
            } else {
              // doc.data() will be undefined in this case
              console.log("No such document!");
            }
          })
          .catch((error) => {
            console.log(error.message);
            dispatch({ type: GET_ERRORS, payload: error });
            setLoading(false);
          });
      })
      .catch((e) => {
        dispatch({ type: GET_ERRORS, payload: e.message });
        setLoading(false);
      });
  } catch (e) {
    dispatch({ type: GET_ERRORS, payload: e.message });
    setLoading(false);
    console.log(e.message);
  }
};

export const changePassword = (data, id, setLoading, navigation) => async (
  dispatch
) => {
  var db = firebase.firestore();
  try {
  } catch (e) {
    dispatch({ type: GET_ERRORS, payload: e.message });
    setLoading(false);
    console.log(e.message);
  }
};

export const uploadPortfolio = (
  data,
  setLoading,
  setPortfolioLoading
) => async (dispatch) => {
  var db = firebase.firestore();
  const response = await fetch(data.image);
  const blob = await response.blob();
  var imgName = data?.email + data.id;
  let imageRef = firebase
    .storage()
    .ref("images/portfolios/" + imgName + ".jpg");

  imageRef
    .put(blob)
    .then((snapshot) => {
      imageRef
        .getDownloadURL()
        .then((url) => {
          db.collection("users")
            .doc(data?.id)
            .update({ portfolio: url })
            .then(() => {
              setLoading(false);
              setPortfolioLoading(false);
              alert("Portfolio Updated");
            })
            .catch((e) => {
              dispatch({ type: GET_ERRORS, payload: e.message });
              setLoading(false);
              setPortfolioLoading(false);
              console.log(e.message);
            });
        })
        .catch((e) => {
          dispatch({ type: GET_ERRORS, payload: e.message });
          setLoading(false);
          setPortfolioLoading(false);
          console.log(e.message);
        });
    })
    .catch((e) => {
      dispatch({ type: GET_ERRORS, payload: e.message });
      setLoading(false);
      setPortfolioLoading(false);
      console.log(e.message);
    });

  try {
  } catch (e) {
    dispatch({ type: GET_ERRORS, payload: e.message });
    setLoading(false);
    setPortfolioLoading(false);
    console.log(e.message);
  }
};

export const deleteChat = (id, setLoading, email, ids, arrEmail) => async (
  dispatch
) => {
  try {
    console.log(id);
    var db = firebase.firestore();
    var chatRef = db.collection("chatRelation").doc(id);
    var messagesRef = db.collection("messages");

    chatRef
      .update({
        deletedFor: arrEmail,
      })
      .then(() => {
        db.collection("messages")
          .where("encodeId", "in", ids)
          .get()
          .then((querySnapshot) => {
            querySnapshot.forEach((doc) => {
              messagesRef
                .doc(doc?.id)
                .update({
                  deletedFor: arrEmail,
                })
                .then(() => {
                  db.collection("chatRelation")
                    .where("users", "array-contains", email)
                    .get()
                    .then((querySnapshot) => {
                      var chat = [];
                      querySnapshot.forEach((doc) => {
                        chat.push({ data: doc.data(), id: doc.id });
                      });
                      dispatch({ type: GET_CHAT_RELATIONS, payload: chat });
                      setLoading(false);
                    })
                    .catch((e) => {
                      dispatch({ type: GET_ERRORS, payload: e.message });
                      setLoading(false);
                      console.log(e.message);
                    });
                })
                .catch((error) => {
                  dispatch({ type: GET_ERRORS, payload: e.message });
                  setLoading(false);
                  console.log(e.message);
                });
            });
          })
          .catch((error) => {
            dispatch({ type: GET_ERRORS, payload: e.message });
            setLoading(false);
            console.log(e.message);
          });
      })
      .catch((error) => {
        // The document probably doesn't exist.
        console.error("Error updating document: ", error);
      });
  } catch (e) {
    dispatch({ type: GET_ERRORS, payload: e.message });
    setLoading(false);
    console.log(e.message);
  }
};

export const getProviders = (setLoading) => async (dispatch) => {
  var db = firebase.firestore();
  try {
    db.collection("users")
      .where("role", "==", "provider")
      .get()
      .then((querySnapshot) => {
        var users = [];
        querySnapshot.forEach((doc) => {
          users.push({ data: doc.data(), id: doc.id });
        });
        dispatch({ type: GET_PROVIDERS, payload: users });
        setLoading(false);
      })
      .catch((e) => {
        dispatch({ type: GET_ERRORS, payload: e.message });
        setLoading(false);
        console.log(e.message);
      });
  } catch (e) {
    dispatch({ type: GET_ERRORS, payload: e.message });
    setLoading(false);
    console.log(e.message);
  }
};

export const updateProviderVerification = (
  data,
  id,
  setLoading,
  navigation
) => async (dispatch) => {
  var db = firebase.firestore();
  try {
    var providerRef = db.collection("users").doc(id);
    providerRef
      .update(data)
      .then(() => {
        db.collection("users")
          .where("role", "==", "provider")
          .get()
          .then((querySnapshot) => {
            var users = [];
            querySnapshot.forEach((doc) => {
              users.push({ data: doc.data(), id: doc.id });
            });
            dispatch({ type: GET_PROVIDERS, payload: users });
            setLoading(false);
            alert("Updated");
            navigation.goBack();
          })
          .catch((e) => {
            dispatch({ type: GET_ERRORS, payload: e.message });
            setLoading(false);
            console.log(e.message);
          });
      })
      .catch((e) => {
        dispatch({ type: GET_ERRORS, payload: e.message });
        setLoading(false);
        console.log(e.message);
      });
  } catch (e) {
    dispatch({ type: GET_ERRORS, payload: e.message });
    setLoading(false);
    console.log(e.message);
  }
};

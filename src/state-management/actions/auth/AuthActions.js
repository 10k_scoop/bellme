import {
  DO_LOGIN,
  GET_ERRORS,
  DO_LOGOUT,
  GET_USER_DATA,
  DO_SIGNUP,
  GET_FACEBOOK_LOGIN,
} from "../../types/types";
import * as firebase from "firebase";
import * as Facebook from "expo-facebook";
import * as Google from "expo-google-app-auth";
import * as AppAuth from "expo-app-auth";
import * as Google2 from "expo-auth-session/providers/google";
export const logout = () => async (dispatch) => {
  try {
    firebase
      .auth()
      .signOut()
      .then(() => {
        // Sign-out successful.
        dispatch({ type: DO_LOGOUT, payload: "Logged Out" });
        console.log("user logged out");
      })
      .catch((error) => {
        // An error happened.
        console.log(error.message);
      });
  } catch (e) {
    dispatch({ type: GET_ERRORS, payload: e });
  }
};

export const signup = (data, setLoading) => async (dispatch) => {
  var db = firebase.firestore();
  try {
    if (data?.role != "client" && data?.verifiedProviderCheck) {
      const businessImgRes = await fetch(data?.businessNameImg);
      const guranteeFormRes = await fetch(data?.guranteeForm);
      const addressImgRes = await fetch(data?.addressImg);
      const policeClearanceImgRes = await fetch(data?.policeClearanceImg);
      const blob1 = await businessImgRes.blob();
      const blob2 = await guranteeFormRes.blob();
      const blob3 = await addressImgRes.blob();
      const blob4 = await policeClearanceImgRes.blob();

      var imgName1 = data?.email + "businessImgRes";
      var imgName2 = data?.email + "guranteeFormRes";
      var imgName3 = data?.email + "addressImgRes";
      var imgName4 = data?.email + "policeClearanceImgRes";
      let imageRef1 = firebase
        .storage()
        .ref("images/handyManDocument/" + imgName1 + ".jpg");

      let imageRef2 = firebase
        .storage()
        .ref("images/handyManDocument/" + imgName2 + ".jpg");

      let imageRef3 = firebase
        .storage()
        .ref("images/handyManDocument/" + imgName3 + ".jpg");

      let imageRef4 = firebase
        .storage()
        .ref("images/handyManDocument/" + imgName4 + ".jpg");

      await imageRef1
        .put(blob1)
        .then((snapshot) => {
          imageRef1
            .getDownloadURL()
            .then((url1) => {
              // 2 blob
              imageRef2
                .put(blob2)
                .then((snapshot) => {
                  imageRef2
                    .getDownloadURL()
                    .then((url2) => {
                      imageRef3
                        .put(blob3)
                        .then((snapshot) => {
                          imageRef3
                            .getDownloadURL()
                            .then((url3) => {
                              imageRef4
                                .put(blob4)
                                .then((snapshot) => {
                                  imageRef4
                                    .getDownloadURL()
                                    .then((url4) => {
                                      firebase
                                        .auth()
                                        .createUserWithEmailAndPassword(
                                          data.email,
                                          data.pass
                                        )
                                        .then(async (userCredential) => {
                                          const user = userCredential.user;
                                          data["businessNameImg"] = url1;
                                          data["guranteeForm"] = url2;
                                          data["addressImg"] = url3;
                                          data["policeClearanceImg"] = url4;
                                          db.collection("users")
                                            .doc(user.uid)
                                            .set(data)
                                            .then(function(res) {
                                              dispatch({
                                                type: DO_SIGNUP,
                                                payload: user,
                                              });
                                              setLoading(false);
                                              alert("Congrats! ");
                                            })
                                            .catch((e) => {
                                              dispatch({
                                                type: GET_ERRORS,
                                                payload: e.message,
                                              });
                                              setLoading(false);
                                              alert(e.message);
                                            });
                                        })
                                        .catch((error) => {
                                          const errorCode = error.code;
                                          const errorMessage = error.message;
                                          console.log(errorMessage);
                                          dispatch({
                                            type: GET_ERRORS,
                                            payload: errorMessage,
                                          });
                                          setLoading(false);
                                          alert(errorMessage);
                                          // ..
                                        });
                                    })
                                    .catch((e) => {
                                      dispatch({
                                        type: GET_ERRORS,
                                        payload: e.message,
                                      });
                                      console.log(e.message);
                                    });
                                })
                                .catch((e) => {
                                  dispatch({
                                    type: GET_ERRORS,
                                    payload: e.message,
                                  });
                                  console.log(e.message);
                                });
                            })
                            .catch((e) => {
                              dispatch({
                                type: GET_ERRORS,
                                payload: e.message,
                              });
                              console.log(e.message);
                            });
                        })
                        .catch((e) => {
                          dispatch({
                            type: GET_ERRORS,
                            payload: e.message,
                          });
                          console.log(e.message);
                        });
                    })
                    .catch((e) => {
                      dispatch({ type: GET_ERRORS, payload: e.message });
                      setLoading(false);
                      console.log(e.message);
                    });
                })
                .catch((e) => {
                  dispatch({ type: GET_ERRORS, payload: e.message });
                  setLoading(false);
                  console.log(e.message);
                });
              // 2 blob
            })
            .catch((e) => {
              dispatch({ type: GET_ERRORS, payload: e.message });
              setLoading(false);
              console.log(e.message);
            });
        })
        .catch((e) => {
          dispatch({ type: GET_ERRORS, payload: e.message });
          setLoading(false);
          console.log(e.message);
        });
    } else if (data?.role == "client" && data?.profileImg?.length > 1) {
      const profileRes = await fetch(data?.profileImg);
      const blob = await profileRes.blob();
      var imgName = data?.email + data.id + "profileRes";
      let imageRef = firebase
        .storage()
        .ref("images/profile/" + imgName + ".jpg");
        imageRef
        .put(blob)
        .then((snapshot) => {
          imageRef
            .getDownloadURL()
            .then((url1) => {
              firebase
                .auth()
                .createUserWithEmailAndPassword(data.email, data.pass)
                .then(async (userCredential) => {
                  const user = userCredential.user;
                  data["profileImg"] = url1;
                  db.collection("users")
                    .doc(user.uid)
                    .set(data)
                    .then(function(res) {
                      dispatch({ type: DO_SIGNUP, payload: user });
                      setLoading(false);
                      alert("Congrats! ");
                    })
                    .catch((e) => {
                      dispatch({ type: GET_ERRORS, payload: e.message });
                      setLoading(false);
                      alert(e.message);
                    });
                })
                .catch((error) => {
                  const errorCode = error.code;
                  const errorMessage = error.message;
                  console.log(errorMessage);
                  dispatch({ type: GET_ERRORS, payload: errorMessage });
                  setLoading(false);
                  alert(errorMessage);
                  // ..
                });
            })
            .catch((e) => {
              dispatch({ type: GET_ERRORS, payload: e.message });
              setLoading(false);
              alert(e.message);
            });
        })
        .catch((e) => {
          dispatch({ type: GET_ERRORS, payload: e.message });
          setLoading(false);
          alert(e.message);
        });
    } else {
      firebase
        .auth()
        .createUserWithEmailAndPassword(data.email, data.pass)
        .then(async (userCredential) => {
          const user = userCredential.user;
          db.collection("users")
            .doc(user.uid)
            .set(data)
            .then(function(res) {
              dispatch({ type: DO_SIGNUP, payload: user });
              setLoading(false);
              alert("Congrats! ");
            })
            .catch((e) => {
              dispatch({ type: GET_ERRORS, payload: e.message });
              setLoading(false);
              alert(e.message);
            });
        })
        .catch((error) => {
          const errorCode = error.code;
          const errorMessage = error.message;
          console.log(errorMessage);
          dispatch({ type: GET_ERRORS, payload: errorMessage });
          setLoading(false);
          alert(errorMessage);
          // ..
        });
    } //if end
  } catch (e) {
    dispatch({ type: GET_ERRORS, payload: e });
    console.log(e.message);
    setLoading(false);
    alert(e.message);
  }
};

export const login = (data, setLoading) => async (dispatch) => {
  try {
    firebase
      .auth()
      .signInWithEmailAndPassword(data.email, data.pass)
      .then((userCredential) => {
        // Signed in
        var user = userCredential.user;
        dispatch({ type: DO_LOGIN, payload: user });
        setLoading(false);
        // ...
      })
      .catch((error) => {
        var errorCode = error.code;
        var errorMessage = error.message;
        alert(errorMessage);
        setLoading(false);
      });
  } catch (e) {
    dispatch({ type: GET_ERRORS, payload: e });
    console.log(e.message);
    setLoading(false);
    alert(e.message);
  }
};

export const getUserData = (id, setLoading) => async (dispatch) => {
  var db = firebase.firestore();
  var docRef = db.collection("users").doc(id);

  docRef
    .get()
    .then((doc) => {
      if (doc.exists) {
        dispatch({ type: GET_USER_DATA, payload: doc.data() });
        setLoading(false);
      } else {
        // doc.data() will be undefined in this case
        console.log("No such document!");
      }
    })
    .catch((error) => {
      dispatch({ type: GET_ERRORS, payload: error });
      setLoading(false);
    });
};

export const facebookLogin = (setFacebookLoading) => async (dispatch) => {
  var db = firebase.firestore();
  try {
    await Facebook.initializeAsync({
      appId: "900062407307318",
      appName: "BellMe",
    });
    const { type, token } = await Facebook.logInWithReadPermissionsAsync({
      permissions: ["public_profile", "email"],
    });
    if (type === "success") {
      // Build Firebase credential with the Facebook access token.
      const credential = firebase.auth.FacebookAuthProvider.credential(token);
      // Sign in with credential from the Facebook user.
      firebase
        .auth()
        .signInWithCredential(credential)
        .then((res) => {
          const user = res.user;
          let data = user.providerData[0];
          db.collection("users")
            .doc(user.uid)
            .set({
              name: data.displayName,
              email: data.email,
              username: "",
              phone: "",
              address: "",
              profileImg: data.photoURL,
              role: "client",
            })
            .then(function(res) {
              dispatch({ type: GET_FACEBOOK_LOGIN, payload: res });
              setFacebookLoading(false);
            })
            .catch((e) => {
              dispatch({ type: GET_ERRORS, payload: e.message });
              setFacebookLoading(false);
              alert(e.message);
            });
        })
        .catch((error) => {
          // Handle Errors here.
          dispatch({ type: GET_ERRORS, payload: error.message });
          setFacebookLoading(false);
        });
    } else {
      setFacebookLoading(false);
    }
  } catch ({ message }) {
    console.log(message);
    dispatch({ type: GET_ERRORS, payload: `Facebook Login Error: ${message}` });
    setFacebookLoading(false);
  }
};

export const loginWithGoogle = () => async (dispatch) => {
  var db = firebase.firestore();
  try {
    const { type, accessToken, user, idToken } = await Google.logInAsync({
      androidClientId:
        "184120454139-qg1k6k2m7j0of9tlb7mtpc3k1lq68atq.apps.googleusercontent.com",
      iosClientId:
        "184120454139-ucd3d2sq9gsg8g013sm00hisjmdb0cqs.apps.googleusercontent.com",
      iosStandaloneAppClientId:
        "184120454139-i2klli6mjicssmmuei55ndiooo2je90c.apps.googleusercontent.com",
      androidStandaloneAppClientId:
        "184120454139-qg1k6k2m7j0of9tlb7mtpc3k1lq68atq.apps.googleusercontent.com",
      scopes: ["profile", "email"],
      redirectUrl: `${AppAuth.OAuthRedirect}:/oauth2redirect/google`,
    });
    if (type === "success") {
      const credential = firebase.auth.GoogleAuthProvider.credential(
        idToken,
        accessToken
      );
      firebase
        .auth()
        .signInWithCredential(credential)
        .then((res) => {
          const user = res.user;
          let data = user.providerData[0];
          db.collection("users")
            .doc(user.uid)
            .set({
              name: data.displayName,
              email: data.email,
              username: "",
              phone: "",
              address: "",
              profileImg: data.photoURL,
              role: "client",
            })
            .then(function(res) {
              dispatch({ type: DO_LOGIN, payload: res });
            })
            .catch((e) => {
              a;
              dispatch({ type: GET_ERRORS, payload: e.message });
              alert(e.message);
            });
        })
        .catch((e) => {
          console.log(e.message);
        });
    } else {
      console.log("error sucess");
    }
  } catch (e) {
    console.log(e.message);
  }
};

export const GET_ERRORS = "GET_ERRORS";
export const DO_LOGIN = "DO_LOGIN";
export const DO_LOGOUT = "DO_LOGOUT";
export const DO_SIGNUP = "DO_SIGNUP";
export const GET_USER_DATA = "GET_USER_DATA";
export const GET_FACEBOOK_LOGIN = "GET_FACEBOOK_LOGIN";
export const GET_JOBS = "GET_JOBS";
export const POST_JOB = "POST_JOB";
export const VIEW_JOB = "VIEW_JOB";
export const MY_JOBS = "MY_JOBS";
export const APPLY_TO_JOB = "APPLY_TO_JOB";
export const UPDATEJOB = "UPDATEJOB";
export const APPLIEDJOB = "APPLIEDJOB";
export const GET_USER_DETAILS = "GET_USER_DETAILS";
export const GET_All_NOTIFICATIONS = "GET_All_NOTIFICATIONS";
export const GET_QUOTE_DETAILS = "GET_QUOTE_DETAILS";
export const GET_CHAT = "GET_CHAT";
export const GET_CHAT_RELATIONS = "GET_CHAT_RELATIONS";
export const GET_PROVIDERS = "GET_PROVIDERS";

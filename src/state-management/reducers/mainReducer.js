import {
  DO_LOGIN,
  DO_LOGOUT,
  GET_USER_DATA,
  DO_SIGNUP,
  GET_FACEBOOK_LOGIN,
  POST_JOB,
  VIEW_JOB,
  MY_JOBS,
  APPLIEDJOB,
  UPDATEJOB,
  APPLY_TO_JOB,
  GET_JOBS,
  GET_USER_DETAILS,
  GET_All_NOTIFICATIONS,
  GET_QUOTE_DETAILS,
  GET_CHAT,
  GET_CHAT_RELATIONS,
  GET_PROVIDERS,
} from "../types/types";
const initialState = {
  login_details: null,
  logout: null,
  get_user_data: null,
  signup_res: null,
  facebook_login_details: null,
  apply_to_job: null,
  update_job: null,
  applied_jobs: null,
  post_job: null,
  view_job: null,
  get_jobs: null,
  get_user_details: null,
  my_jobs: null,
  get_all_notifications: null,
  get_quote_details: null,
  get_chat: null,
  get_chat_relations: null,
  get_providers: null,
};
const mainReducer = (state = initialState, action) => {
  switch (action.type) {
    case DO_LOGIN:
      return {
        ...state,
        login_details: action.payload,
      };
    case GET_FACEBOOK_LOGIN:
      return {
        ...state,
        facebook_login_details: action.payload,
      };
    case DO_LOGOUT:
      return {
        ...state,
        logout: action.payload,
      };

    case GET_USER_DATA:
      return {
        ...state,
        get_user_data: action.payload,
      };

    case DO_SIGNUP:
      return {
        ...state,
        signup_res: action.payload,
      };
    case GET_JOBS:
      return {
        ...state,
        get_jobs: action.payload,
      };
    case POST_JOB:
      return {
        ...state,
        post_job: action.payload,
      };
    case VIEW_JOB:
      return {
        ...state,
        view_job: action.payload,
      };
    case MY_JOBS:
      return {
        ...state,
        my_jobs: action.payload,
      };
    case APPLY_TO_JOB:
      return {
        ...state,
        apply_to_job: action.payload,
      };
    case UPDATEJOB:
      return {
        ...state,
        update_job: action.payload,
      };
    case APPLIEDJOB:
      return {
        ...state,
        applied_jobs: action.payload,
      };
    case GET_USER_DETAILS:
      return {
        ...state,
        get_user_details: action.payload,
      };
    case GET_All_NOTIFICATIONS:
      return {
        ...state,
        get_all_notifications: action.payload,
      };
    case GET_QUOTE_DETAILS:
      return {
        ...state,
        get_quote_details: action.payload,
      };
    case GET_CHAT:
      return {
        ...state,
        get_chat: action.payload,
      };
    case GET_CHAT_RELATIONS:
      return {
        ...state,
        get_chat_relations: action.payload,
      };
    case GET_PROVIDERS:
      return {
        ...state,
        get_providers: action.payload,
      };

    default:
      return state;
  }
};
export default mainReducer;
